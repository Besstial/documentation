# [Documentation](https://besstial.gitlab.io/documentation)

## Mkdocs

Pour installer et exécuter la documentation en local, il faut réaliser les actions suivantes : 

```bash
 bash install.sh
```

```bash
 mkdocs serve
```

## Obsidian

Pour utiliser Obsidian, vous devez télécharger l'AppImage et réaliser les actions suivantes :

```bash
 sudo add-apt-repository universe
 sudo apt install libfuse2
```

```bash
 chmod u+x Obsidian-<version>.AppImage
```

```bash
 ./Obsidian-<version>.AppImage
```

## Links

- [MKDocs](https://www.mkdocs.org/getting-started/)
- [Obsidian](https://obsidian.md/download)
- [Gitlab](https://about.gitlab.com/blog/2022/03/15/publishing-obsidian-notes-with-gitlab-pages/)
