# Git

## Git and the Command Line

Git is a technology that can be used to track changes to a project and to help multiple people to collaborate on a project. At a basic level, a project version controlled by Git consists of a folder with files in it, and Git tracks the changes that are made to the files in the project. This allows you to save different versions of the work you’re doing, which is why we call Git a version control system. 

Git configurations are settings that allow you to customize how Git works. List the variables in the global Git configuration file and their values: 

```bash
git config --global --list
```
To set these variables in your global Git configuration file: 

```bash
git config --global user.name <name>
git config --global user.email <email>
git config --global init.defaultBranch <branch_name>
```

## Local Repositories

A repository is how we refer to a project version controlled by Git. There are two types of repositories:

- A local repository is a repository that is stored on a computer.
- A remote repository is a repository that is hosted on a hosting service.

A <u>local repository</u> is represented by a hidden directory called .git that exists within a project directory. It contains all the data on the changes that have been made to the files in a project. 

Initialize a Git repository and set the name for the initial branch:

```bash
git init -b <branch_name>
```

There are four important areas to be aware of when you are working with Git:

- Working directory
- Staging area
- Commit history
- Local repository

The <u>working directory</u> contains the files and directories in the project directory that represent one version of a project. It is sort of like a workbench. It is where you add, edit, and delete files and directories.

The <u>staging area</u> is similar to a rough draft space. It is where you can add and remove files, when you are preparing what you want to include in the next saved version of your project (your next commit). The staging area is represented by a binary file in the .git directory called index.

A commit in Git is basically one version of a project. You can think of it as a snapshot of a project, or a standalone version of a project that contains references to all the files that are part of that commit. Every commit has a commit hash (sometimes called a commit ID).

The <u>commit history</u> is where you can think of your commits existing. It is represented by the objects directory inside the .git directory.

When you create a file, even though the file is in the working directory, it is not part of your repository. It has not been added to the staging area and it has not yet been included in a commit in the commit history. It is an untracked file. An untracked file is a file in the working directory that Git is not version controlling. Once you add a file to the staging area and include it in a commit, the file becomes a tracked file. 

## Making a Commit

Making a commit is a two-step process:

1. Add all the files you want to include in the next commit to the staging area. 
2. Make a commit with a commit message. 

Show the state of the working directory and the staging area:

```bash
git status
```

![untrack_file](./attachments/git/untrack_file.png)

Add all the files in the working directory that have been edited or changed to the staging area:

```bash
git add -A
```

The command does not move a file from the working directory to the staging area. It copies the file from the working directory into the staging area. 

![staging_area](./attachments/git/staging_area.png)

It’s important to note that commit is both a verb and a noun. In Git, the verb to commit means to save something, and the noun (a commit) means a version of our project. So, to make a commit means to save a version of a project. 

Create a new commit with a commit message:

```bash
git commit -m “<message>”
```

![commit_file](./attachments/git/commit_file.png)

Show a list of commits in reverse chronological order:

```bash
git log
```

Show a list of commits in reverse chronological order for all branches in a local repository: 

```bash
git log --all
```

## Branches

There are two main reasons to use branches: 

- To work on the same project in different ways.
- To help multiple people work on the same project at the same time.

A Git project can have multiple branches. Each of these branches is a standalone version of the project. Branches in Git are movable pointers to commits.

You can navigated into the .git directory, the refs directory, and then the heads directory. The term “refs” stands for “references.” The heads directory stores a file for each local branch in your local repository. You can think of that file as storing the “head” of that branch; in other words, the latest commit on that branch. 

Tracked files in the working directory can be in one of two states. Unmodified files and modified files. The file must have been saved in the text editor. If you have not saved those changes in your text editor, then Git will view it as an unmodified file. The `git status` command actually shows a list of all modified files and tells you whether or not they have been added to the staging area. It does not, however, list unmodified files.

List local branches:

```bash
git branch
``` 

Create a branch: 

```bash
git branch <new_branch_name>
```

A new branch will initially point to the commit that you were on when you made the branch.

### What is HEAD?

HEAD is simply a pointer that tells you which branch you are on. There are times where you can be on a commit that is not pointed to by a branch. Git calls this “detached HEAD state”. The HEAD file on the .git directory contains ref: refs/heads/main, which is a reference to the main file that represents the main branch. This indicates you’re currently on the main branch.

HEAD (in capital letters) should not be confused with the heads directory that can be found in .git > refs > heads. The heads directory stores a file for every local branch in your local repository, while HEAD indicates which branch you are on by referencing one of the files inside the heads directory.

Making a branch in Git does not mean that you automatically switch onto that branch. You must explicitly instruct Git that you want to switch onto a branch.

Switch branches: 

```bash
git switch <branch_name>
```

```bash
git checkout <branch_name>
```

The only purpose of the `git switch` command is to switch branches, while the `git checkout` command can do more things. The `git switch` (or `git checkout`) command does three things when used to switch branches:

1. It changes the HEAD pointer to point to the branch you are switching onto.
2. It populates the staging area with a snapshot of the commit you are switching onto. 
3. It copies the contents of the staging area into the working directory

<div class="center-images">
    <img src="../attachments/git/red_main.png" alt="red_main">
    <img src="../attachments/git/orange_main.png" alt="orange_main">
    <img src="../attachments/git/git_switch.png" alt="git_switch">
    <img src="../attachments/git/yellow_commit.png" alt="yellow_commit">
</div>

## Merging

We are going to learn about integrating changes from one branch into another. In Git, there are two ways to do this: merging and rebasing.

Merging in Git is one way you can integrate the changes made in one branch into another branch. In any merge, there is one branch that you are merging, called the **source branch**, and one branch that you’re merging into, called the **target branch**. The source branch is the branch that contains the changes that will be integrated into the target branch. The target branch is the branch that receives the changes and is therefore the only one that is altered in this operation.

There are two types of merges: 

- Fast-forward merges
- Three-way merges

The factor that determines which of these types of merges will take place when you merge the source branch into the target branch is whether the development histories of the two branches have diverged. A branch’s development history can be traced by following the parent links of commits.

When it is possible to reach the target branch by following the parent links that make up the commit history of the source branch, Git performs the fast-forward merge. During a fast-forward merge, Git takes the pointer of the target branch and moves it to the commit of the source branch. 

<div class="center-images">
    <img src="../attachments/git/fastforward_merge_example_1.png" alt="fastforward_merge_example_1">
    <img src="../attachments/git/fastforward_merge_example_2.png" alt="fastforward_merge_example_2">
    <img src="../attachments/git/fastforward_merge_example_3.png" alt="fastforward_merge_example_3">
</div>

A three-way merge is a type of merge that occurs when the development histories of the branches involved in the merge have diverged. Development histories have diverged when it is not possible to reach the target branch by following the commit history of the source branch. In this case when you merge the source branch into the target branch, Git performs a three-way merge, creating a merge commit to tie the two development histories together; it then moves the pointer of the target branch to the merge commit.

<div class="center-images">
    <img src="../attachments/git/threeway_merge_example_1.png" alt="threeway_merge_example_1">
    <img src="../attachments/git/threeway_merge_example_2.png" alt="threeway_merge_example_2">
    <img src="../attachments/git/threeway_merge_example_3.png" alt="threeway_merge_example_3">
</div>

Commit M points back to both commit J and commit L. The reason that this kind of merge is called a three-way merge is because in order to carry out the merge, Git will take a look at the two commits that the branches involved in the merge are pointing to. Three-way merges are a more complex type of merge where you may experience merge conflicts. These arise when you merge two branches where different changes have been made to the same parts of the same file(s), or if in one branch a file was deleted that was edited in the other branch. 

There are two steps involved in doing a merge: 

1. Switch onto the branch that you want to merge into (the target branch).
2. Use the git merge command and pass in the name of the branch you’re merging (the source branch). 

Integrate changes from one branch into another branch: 

```bash
git switch <target_branch>
git merge <source_branch>
```

If Git detects that switching branches will cause you to lose uncommitted changes in your working directory, then it will stop you from switching branches and present you with an error message. However, this happens only if the files that contain uncommitted changes have conflicting changes in the branch you are switching onto.

Merging a branch does not delete the branch. You must explicitly delete a branch if you no longer want to use it. `git checkout` command may be used to switch branches as well as to carry out other actions. One of the other things you can do with the `git checkout` command is check out commits. What if I want to look at an older version of your project but there is currently no branch pointing to that commit ? So I can’t switch onto it by switching onto a branch. Instead, you can choose to check out that commit by using the `git checkout` command and passing in the commit hash of the commit. 

Check out a commit: 

```bash
git checkout <commit_hash>
```

The main difference between checkout and switch is that the HEAD pointer will point directly to a commit instead of pointing to a branch. This means that you will be in something that Git calls a detached HEAD state. This allows you to look at any commit—or, in other words, any version of your project—in your entire repository.

![detached_head](./attachments/git/detached_head.png)

Create a new branch and switch onto it: 

```bash
git switch -c <new_branch_name>
```

```bash
git checkout -b <new_branch_name>
```

## Hosting Services and Authentication

Local repositories are found on a computer, while remote repositories are hosted on a 
hosting service in the cloud. To transfer data between a local repository and a remote repository on a hosting service, you must connect and authenticate using either SSH or HTTPS.

The HTTPS protocol uses a username and some sort of password (or authentication credential) to allow you to securely connect to remote repositories. In the past, all the hosting services allowed you to use the password you use to log in to your account on the hosting service (which we will refer to as the account password) for HTTPS authentication as well. However, GitHub and Bitbucket no longer allow this; they require you to create another authentication credential. In GitHub, the authentication credential is called a personal access token.

The SSH protocol uses a public and private SSH key pair to allow you to securely connect to remote repositories. The three main steps to setting up SSH access are:

1. Create an SSH key pair on your computer.
2. Add the private SSH key to the SSH agent. 
3. Add the public SSH key to the hosting service account.

## Creating and Pushing to a Remote Repository

You can start working on a project using Git from either a local or a remote repository. 

Upload data to a remote repository: 

```bash
git push 
```

Local repositories and remote repositories act separately. When it comes to working with them, it’s important to understand that no interaction between them happens automatically. In other words, no updates from the local repository to the remote repository will happen automatically, and conversely, no updates from the remote repository to the local repository will happen automatically. 
Why Do We Use Remote Repositories?

- Easily backup your project somewhere other than your computer.
- Access a Git project from multiple computers.
- Collaborate with others on Git projects.

There are three steps to this process: 

1. Create the remote repository on the hosting service. 
2. Add a connection to the remote repository in the local repository. 
3. Upload (or push) data from the local repository to the remote repository. 

A local repository can communicate with a remote repository when the local repository has a connection to the remote repository stored within it. This connection will have a name, which we refer to as the remote repository shortname or just shortname. A local repository can have connections to multiple remote repositories, although this isn’t very common. 

Add a connection to a remote repository named  < shortname > at < URL >: 

```bash
git remote add <shortname> <URL>
```

You may choose to use either your SSH URL or your HTTPS URL, depending on which protocol you have chosen to use.

When you clone a remote repository to create a local repository, Git automatically adds a connection to the remote repository with the default shortname origin. In the official Git documentation, a connection to a remote repository stored in a local repository is simply referred to as a remote. 

List the remote repository connections in the local repository with shortnames and URLs: 

```bash
git remote -v
```

When you push a local branch to a remote repository, you will create a remote branch. A remote branch is a branch in a remote repository. Remote branches do not automatically update when you make more commits on local branches. You have to explicitly push commits from a local branch to a remote branch. 

Every remote branch (that a local repository knows about) also has a remote-tracking branch. This is a reference in a local repository to the commit a remote branch pointed at the last time any network communication happened with the remote repository. You can set up a tracking relationship between a local branch and a remote branch by defining which remote branch a local branch should track. This is referred to as the upstream branch. 

If the local branch has an upstream branch defined for it, you can use `git push` with no arguments, and Git will automatically push the work to that branch. However, if no upstream branch is defined for the local branch you’re working on, you’ll need to specify which remote branch to push to when you enter the git push command. 

Upload content from < branch_name > to the < shortname > remote repository:

```bash
git push <shortname> <branch_name>
```

After you execute the git push command, two things will happen: 

1. A remote branch will be created in your remote repository. 
2. A remote-tracking branch will be created in your local repository. 

<div class="center-images">
    <img src="../attachments/git/remote_repository_1.png" alt="remote_repository_1">
    <img src="../attachments/git/remote_repository_2.png" alt="remote_repository_2">
    <img src="../attachments/git/remote_repository_3.png" alt="remote_repository_3">
</div>

List local branches and remote-tracking branches: 

```bash
git branch --all
```

Notice that when you push a specific branch to a remote repository, only the data from that branch is uploaded to the remote repository.

## Cloning and Fetching

Clone a remote repository:

```bash
git clone <URL> <directory_name>
```

If you don’t pass a project directory name, then the local repository will be assigned the remote repository project name. The `git clone` command does the following:

1. Create a project directory inside the current directory. 
2. Create (initialize) the local repository.
3. Download all the data from the remote repository. 
4. Add a connection to the remote repository that was cloned; by default it will have the shortname origin in the new local repository.

### What is origin/HEAD?

When you clone a repository, Git needs to know which branch it should be on when cloning is done. The origin/HEAD pointer determines which branch this is. 

When you clone a repository the `git clone` command will create remote-tracking branches for all the branches currently present in the remote repository that is being cloned, but the only local branch that is created is the branch that origin/HEAD points to. To work on the feature branch, they must switch onto it. Then Git will create a local feature branch based on where the remote-tracking branch was pointing. 

The main reason to delete branches is to keep a Git project organized and uncluttered. When you delete a branch with commits that are not part of any other branch, you don’t delete the commits that are part of that branch. The commits still exist in your commit history. However, they are no longer easy to reach, because there is no simple branch reference to them and they are not part of the development history of any existing branch.

Delete a remote branch and the associated remote-tracking branch: 

```bash
git push <shortname> -d <branch_name>
```

Delete a local branch: 

```bash
git branch -d <branch_name>
```

List the local branches and their upstream branches, if they have any: 

```bash
git branch -vv
```

The `git fetch` command downloads all the necessary commits to update all the remote-tracking branches in the local repository to reflect the state of the remote branches in the remote repository that is specified.

Download data from the < shortname > remote repository:

```bash
git fetch <shortname>
```

The `git fetch` command affects only remote-tracking branches. It does not affect local branches. Thus, nothing in your working directory will change when you fetch data from a remote repository. Once you have fetched the changes from a remote repository and updated the remote-tracking branches in a local repository, you’re ready to update a local branch. You are going to merge the origin/main remote-tracking branch into the local main branch

Remove remote-tracking branches that correspond to deleted remote branches and download data from the remote repository:

```bash
git fetch -p
```

## Three-Way Merges

Define an upstream branch for the current local branch: 

```bash
git branch -u <shortname>/<branch_name>
```

Just because a file with a particular name is staged for commit doesn’t mean it is automatically updated with any other changes you make to it. You need to explicitly add each updated version of a file to the staging area to include the latest changes in your next commit. 

Incorporating changes from a remote repository is a two-step process:

1. First, you fetch the changes from the remote repository.
2. Second, you integrate the changes into the local branch in the local repository.

At step 2, instead of a fast-forward merge because the development histories of the two branches involved in the merge have diverged, we will end up carrying out a three-way merge.

To view the parent commits of a commit: 

```bash
git cat-file -p <commit_hash> 
```

Up until now, when you wanted to update your local repository with changes from the remote repository you did it in two steps: first you fetched the data from the remote repository (using `git fetch`), and then you merged the data (using `git merge`) into the local branch. Pulling data allows you to do both in one go.

Fetch and integrate changes from the < shortname > remote repository for the specified < branch_name >: 

```bash
git pull <shortname> <branch_name>
```

There’s one more thing you need to know about the git pull command. As you’ve learned, in Git there are two ways to integrate changes: merging and rebasing. Which method the git pull command uses will depend on whether the development histories of the branches have diverged and, if so, on the option you choose when entering the command:

- If the development histories of the local branch and remote branch in a git pull have not diverged, then by default a fast-forward merge will occur.
- If the development histories of the local branch and the remote branch in a git pull have diverged, then you must tell Git whether you want to integrate the changes by merging or rebasing (otherwise, you’ll get an error). To tell Git to integrate the changes by merging, you must pass in the --no-rebase option. To tell Git to integrate the changes by rebasing, you must pass in the --rebase option. 

![git_pull](./attachments/git/git_pull.png)

## Merge Conflicts

When merge conflicts happen, you will see a set of special markers in each of the files involved that indicate where the conflicts occur. These markers, called conflict markers, consist of seven left angle brackets (<<<<<<<), seven equals signs (=======), and seven right angle brackets (>>>>>>>), as well as references to the branches involved in the merge.

![merge_conflicts](./attachments/git/merge_conflicts.png)

There are two steps to resolving merge conflicts: 

1. Decide what to keep, edit the content, and remove the conflict markers. 
2. Add the file(s) you have edited to the staging area and commit your changes. 

Stop the merge process and go back to the state before the merge: 

```bash
git merge --abort
```

## Rebasing

Some teams and individuals prefer to maintain a linear project history because they find it is more organized and simpler. You can use the process of rebasing to avoid three-way merges and merge commits and maintain a linear project history. Rebasing takes all the work you have done in the commits on one branch and reapplies the work on another branch, creating entirely new commits. This can make it appear as though you have created a branch from an entirely different commit than the original commit you created it from. Given that rebasing creates entirely new commits, this means it changes the commit history. 

<div class="center-images">
    <img src="../attachments/git/rebase_before.png" alt="rebase_before">
    <img src="../attachments/git/rebase_after.png" alt="rebase_after">
</div>

Reapply commits on top of another branch: 

```bash
git rebase <branch_name>
```

Restore a file to another version of the file in the staging area: 

```bash
git restore --staged <filename>
```

To initiate the rebase process, you use the `git rebase` command. Git will then carry out the five stages of the process itself; the only time you have to actively get involved is if there are any merge conflicts.

1. Find the common ancestor
2. Store information about the branches involved in the rebase
3. Reset HEAD. Git will reset HEAD to point to the same commit as the branch you are rebasing onto.
4. Apply and commit the changes
5. Switch onto the rebased branch. Git will make the branch you rebased point to the last commit it reapplies, and it will check out that branch so that HEAD points to it. 

<div class="center-images">
    <img src="../attachments/git/rebase_stage_1_2.png" alt="rebase_stage_1_2">
    <img src="../attachments/git/rebase_stage_3_4.png" alt="rebase_stage_3_4">
    <img src="../attachments/git/rebase_stage_5.png" alt="rebase_stage_5">
</div>

When resolving merge conflicts in a three-way merge, all the merge conflicts are presented to you at the same time; once you’ve resolved all the conflicts and added all the updated files to the staging area, you make the final merge commit. By contrast, in the process of rebasing, as Git applies the changes from each commit one by one, it will pause the process if it encounters merge conflicts in any reapplied commit. This means that you may have to resolve merge conflicts several times when rebasing, depending on how many commits contain merge conflicts. 

Continue with the rebase process after having resolved merge conflicts: 

```bash
git rebase --continue
```

Stop the rebase process and go back to the state before the rebase: 

```bash
git rebase --abort
```

**<u>The Golden Rule of Rebasing.</u>**  You may safely rebase a branch if: 

- You have a local branch that has never been pushed to the remote repository. 
- You have a local branch that you’ve pushed to a remote repository that you’re 100% sure nobody has based work on or contributed to. 

## Pull Requests (Merge Requests)

A pull request (also referred to as a merge request) is a feature offered by a hosting service that allows you to share work you have done on a branch with your collaborators, potentially gather feedback on that work, and finally integrate that work into the project remotely on the hosting service. Although pull requests are not a feature of Git but of the hosting services that host projects using Git, they are so useful in day-to-day work with Git.

By default, merging remotely is different from merging locally. The default setting for most hosting services is that a remote merge with a pull request happens with a merge option called non-fast-forward. With this option, even if the development histories of the source branch and the target branch have not diverged, a merge commit will still be made.


## Utils

The command to quit Vim when `git log` command is used:

```bash
q
```

The command to enter to save and quit Vim: 

```bash
:^w^q
```
