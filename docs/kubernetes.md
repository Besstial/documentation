# Kubernetes

Kubernetes is an open source orchestrator for deploying containerized applications. It was originally developed by Google, inspired by a decade of experience deploying  scalable, reliable systems in containers via application-oriented APIs.

There are many reasons people come to use containers and container APIs like Kubernetes, but we believe they can all be traced back to one of these benefits:
- Development velocity
- Scaling (of both software and teams)
- Abstracting your infrastructure
- Efficiency
- Cloud native ecosystem

#### Velocity
Velocity is measured not in terms of the raw number of features you can ship per hour or day, but rather in terms of the number of things you can ship  while maintaining a highly available service.

In this way, containers and Kubernetes can provide the tools that you need to move quickly, while staying available. The core concepts that enable this are:
- Immutability
- Declarative configuration
- Online self-healing systems
- Shared reusable libraries and tools

These ideas all interrelate to radically improve the speed with which you can reliably deploy new software.

#### The Value of Immutability
Containers and Kubernetes encourage developers to build distributed systems that adhere to the principles of immutable infrastructure. With immutable infrastructure,  once an artifact is created in the system, it does not change via user modifications.

#### Declarative Configuration
Immutability extends beyond containers running in your cluster to the way you describe your application to Kubernetes. Everything in Kubernetes is a declarative configuration object that represents the desired state of the system. It is the job of Kubernetes to ensure that the actual state of the world matches this desired state.

Much like mutable versus immutable infrastructure, declarative configuration is an alternative to imperative configuration, where the state of the world is defined by the execution of a series of instructions rather than a declaration of the desired state of the world. While imperative commands define actions, declarative configurations define state.

The combination of declarative state stored in a version control system and the ability of Kubernetes to make reality match this declarative state makes rollback of a change trivially easy. It is simply restating the previous declarative state of the system. This is usually impossible with imperative systems, because although the imperative instructions describe how to get you from point A to point B, they rarely include the reverse instructions that can get you back.

#### Self-Healing Systems
Kubernetes is an online, self-healing system. When it receives a desired state configuration, it does not simply take a set of actions to make the current state match the desired state a single time. It continuously takes actions to ensure that the current state matches the desired state. This means that not only will Kubernetes initialize your system, but it will guard it against any failures or perturbations that might destabilize the system and affect reliability.

### Scaling Your Service and Your Teams
As your product grows, it’s inevitable that you will need to scale both your software and the teams that develop it. Fortunately, Kubernetes can help with both of these goals. Kubernetes achieves scalability by favoring decoupled architectures.

#### Decoupling
In a decoupled architecture, each component is separated from other components by defined APIs and service load balancers. APIs and load balancers isolate each piece of the system from the others. APIs provide a buffer between implementer and consumer, and load balancers provide a buffer between running instances of each service.

Decoupling components via load balancers makes it easy to scale the programs that make up your service, because increasing the size (and therefore the capacity) of the program can be done without adjusting or reconfiguring any of the other layers of your service.

Decoupling servers via APIs makes it easier to scale the development teams because each team can focus on a single, smaller microservice with a comprehensible surface area.

#### Easy Scaling for Applications and Clusters
Sometimes you actually need to scale up the cluster itself. Again, Kubernetes makes this task easier. 

Kubernetes provides numerous abstractions and APIs that make it easier to build these decoupled microservice architectures:
- Pods, or groups of containers, can group together container images developed by different teams into a single deployable unit.
- Kubernetes services provide load balancing, naming, and discovery to isolate one microservice from another.
- Namespaces provide isolation and access control, so that each microservice can control the degree to which other services interact with it.
- Ingress objects provide an easy-to-use frontend that can combine multiple micro‐services into a single externalized API surface area.

Finally, decoupling the application container image and machine means that different microservices can colocate on the same machine without interfering with one another, reducing the overhead and cost of microservice architectures. 

## Creating and Running Containers

### Container Images
A container image is a binary package that encapsulates all of the files necessary to run a program inside of an OS container.

#### Container Layering
The image isn’t a single file but rather a specification for a manifest file that points to other files. The manifest and associated files are often treated by users as a unit. Container images are constructed with a series of filesystem layers, where each layer inherits and modifies the layers that came before it.

```
.
└── container A: a base operating system only, such as Debian 
    └── container B: build upon #A, by adding Ruby v2.1.10 
    └── container C: build upon #A, by adding Golang v1.6
```

Container images are typically combined with a container configuration file, which provides instructions on how to set up the container environment and execute an application entry point. The container configuration often includes information on how to set up networking, namespace isolation, resource constraints (cgroups), and what syscall restrictions should be placed on a running container instance. The container root filesystem and configuration file are typically bundled using the Docker image format.

Another pitfall revolves around image caching and building. Remember that each layer is an independent delta from the layer below it. Every time you change a layer, it changes every layer that comes after it. Changing the preceding layers means that they need to be rebuilt, repushed, and repulled to deploy your image to development.

#### Image Security
Secrets and images should never be mixed.

#### Multistage Image Builds
One of the most common ways to accidentally build large images is to do the actual program compilation as part of the construction of the application container image. Compiling code as part of the image build feels natural, and it is the easiest way to build a container image from your program. The trouble with doing this is that it leaves all of the unnecessary development tools, which are usually quite large, lying around inside your image and slowing down your deployments.

To resolve this problem, Docker introduced multistage builds. With multistage builds, rather than producing a single image, a Docker file can actually produce multiple images. Each image is considered a stage. Artifacts can be copied from preceding stages to the current stage.

### The Container Runtime Interface
Kubernetes provides an API for describing an application deployment, but relies on a container runtime to set up an application container using the container-specific APIs native to the target OS. On a Linux system that means configuring cgroups and namespaces. The interface to this container runtime is defined by the Container Runtime Interface (CRI) standard. The CRI API is implemented by a number of different programs, including the containerd-cri built by Docker and the cri-o implementation contributed by Red Hat. When you install the Docker tooling, the containerd runtime is also installed and used by the Docker daemon.

#### Limiting Resource Usage
Docker enables applications to use fewer resources by exposing the underlying cgroup technology provided by the Linux kernel. These capabilities are likewise used by Kubernetes to limit the resources each Pod uses.
- Limiting memory resources: use the `--memory` and `--memory-swap`
- Limiting CPU resources: use the `--cpu-shares`

## Deploying a Kubernetes Cluster
```bash
kubectl version
kubectl get componentstatuses
kubectl get nodes
kubectl describe nodes kube1
```
You can see here the components that make up the Kubernetes cluster. The controller-manager is responsible for running various controllers that regulate behavior in the cluster; for example, ensuring that all of the replicas of a service are available and healthy. The scheduler is responsible for placing different Pods onto different nodes in the cluster. Finally, the etcd server is the storage for the cluster where all of the API objects are stored.

In Kubernetes, nodes are separated into control-plane nodes that contain containers like the API server, scheduler, etc., which manage the cluster, and worker nodes where your containers will run. Kubernetes won’t generally schedule work onto control-plane nodes to ensure that user workloads don’t harm the overall operation of the cluster.

### Cluster Components

#### Kubernetes Proxy
The Kubernetes proxy is responsible for routing network traffic to load-balanced services in the Kubernetes cluster. To do its job, the proxy must be present on every node in the cluster. Kubernetes has an API object named DaemonSet, which you will learn about in Chapter 11, that is used in many clusters to accomplish this. If your cluster runs the Kubernetes proxy with a DaemonSet, you can see the proxies by running:
```bash
kubectl get daemonSets --namespace=kube-system kube-proxy
```

#### Kubernetes DNS
Kubernetes also runs a DNS server, which provides naming and discovery for the services that are defined in the cluster. This DNS server also runs as a replicated service on the cluster.

## Common kubectl Commands

### Namespaces
Kubernetes uses namespaces to organize objects in the cluster. You can think of each namespace as a folder that holds a set of objects. By default, the kubectl command-line tool interacts with the default namespace. If you want to use a different namespace, you can pass kubectl the `--namespace` flag. The shorthand -n to list all Pods in your cluster—you can pass the `--all-namespaces` flag.

### Contexts
If you want to change the default namespace more permanently, you can use a context.

### Viewing Kubernetes API Objects
Everything contained in Kubernetes is represented by a RESTful resource. Throughout this book, we refer to these resources as Kubernetes objects. Each Kubernetes object exists at a unique HTTP path; for example, https://your-k8s.com/api/v1/namespaces/default/pods/my-pod leads to the representation of a Pod in the default namespace named my-pod. The kubectl command makes HTTP requests to these URLs to access the Kubernetes objects that reside at these paths.

### Creating, Updating, and Destroying Kubernetes Objects
Objects in the Kubernetes API are represented as JSON or YAML files. These files are either returned by the server in response to a query or posted to the server as part of an API request. You can use these YAML or JSON files to create, update, or delete objects on the Kubernetes server.

You can use kubectl to create this object in Kubernetes by running:
```bash
kubectl apply -f obj.yaml
```

Similarly, after you make changes to the object, you can use the apply command again to update the object:
```bash
kubectl apply -f obj.yaml
```

If you want to see what the apply command will do without actually making the changes, you can use the `--dry-run` flag to print the objects to the terminal without actually sending them to the server.

The apply command also records the history of previous configurations in an annotation within the object. You can manipulate these records with the `edit-last-applied`, `set-last-applied`, and `view-last-applied` commands.

When you want to delete an object, you can simply run:
```bash
kubectl delete -f obj.yaml
```

### Debugging Commands

kubectl also makes a number of commands available for debugging your containers. You can use the following to see the logs for a running container. If you have multiple containers in your Pod, you can choose the container to view using the `-c` flag. By default, kubectl logs lists the current logs and exits. If you instead want to continuously stream the logs back to the terminal without exiting, you can add the `-f` (follow) command-line flag.
```bash
kubectl logs <pod-name>
```

You can also use the exec command to execute a command in a running container:
```bash
kubectl exec -it <pod-name> -- bash
```
This will provide you with an interactive shell inside the running container so that you can perform more debugging.

If you don’t have bash or some other terminal available within your container, you can always attach to the running process:
```bash
kubectl attach -it <pod-name>
```

You can also copy files to and from a container using the `cp` command:
```bash
kubectl cp <pod-name>:</path/to/remote/file> </path/to/local/file>
```

If you want to access your Pod via the network, you can use the port-forward command to forward network traffic from the local machine to the Pod. This enables you to securely tunnel network traffic through to containers that might not be exposed anywhere on the public network.
```bash
kubectl port-forward <pod-name> 8080:80
```

If you want to view Kubernetes events, you can use the kubectl get events command to see a list of the latest 10 events on all objects in a given namespace:
```bash
kubectl get events
```

if you are interested in how your cluster is using resources, you can use the top command to see the list of resources in use by either nodes or Pods. 
```bash
kubectl top nodes
kubectl top pods
```

### Cluster Management

The kubectl tool can also be used to manage the cluster itself. The most common action that people take to manage their cluster is to cordon and drain a particular node. When you cordon a node, you prevent future Pods from being scheduled onto that machine. When you drain a node, you remove any Pods that are currently running on that machine. A good example use case for these commands would be removing a physical machine for repairs or upgrades. In that scenario, you can use `kubectl cordon` followed by `kubectl drain` to safely remove the machine from the cluster. Once the machine is repaired, you can use `kubectl uncordon` to re-enable Pods scheduling onto the node. There is no undrain command; Pods will naturally get scheduled onto the empty node as they are created. For something quick affecting a node (e.g., a machine reboot), it is generally unnecessary to cordon or drain; it’s only necessary if the machine will be out of service long enough that you want the Pods to move to a different machine.

## Pods

A Pod is a collection of application containers and volumes running in the same execution environment. Pods, not containers, are the smallest deployable artifact in a Kubernetes cluster. This means all of the containers in a Pod always land on the same machine. Each container within a Pod runs in its own cgroup, but they share a number of Linux namespaces.

Applications running in the same Pod share the same IP address and port space(network namespace), have the same hostname (UTS namespace), and can communicate using native interprocess communication channels over System V IPC or POSIX message queues (IPC namespace). However, applications in different Pods are isolated from each other; they have different IP addresses, hostnames, and more. Containers in different Pods running on the same node might as well be on different servers.

In general, the right question to ask yourself when designing Pods is “Will these containers work correctly if they land on different machines?” If the answer is no, a Pod is the correct grouping for the containers. If the answer is yes, using multiple Pods is probably the correct solution. 

### The Pod Manifest

Pods are described in a Pod manifest, which is just a text-file representation of the Kubernetes API object. You can write Pod manifests using YAML or JSON, but YAML is generally preferred because it is slightly more human-editable and supports comments. 

The Kubernetes API server accepts and processes Pod manifests before storing them in persistent storage (etcd). The scheduler also uses the Kubernetes API to find Pods that haven’t been scheduled to a node. It then places the Pods onto nodes depending on the resources and other constraints expressed in the Pod manifests. The scheduler can place multiple Pods on the same machine as long as there are sufficient resources. However, scheduling multiple replicas of the same application onto the same machine is worse for reliability, since the machine is a single failure domain. Consequently, the Kubernetes scheduler tries to ensure that Pods from the same application are distributed onto different machines for reliability in the presence of such failures. Once scheduled to a node, Pods don’t move and must be explicitly destroyed and rescheduled.

To find out more information about a Pod (or any Kubernetes object), you can use the kubectl describe command.

```bash
kubectl describe pods <pod-name>
```

When a Pod is deleted, it is not immediately killed. Instead, if you run kubectl get pods, you will see that the Pod is in the Terminating state. All Pods have a termination grace period. By default, this is 30 seconds. When a Pod is transitioned to Terminating, it no longer receives new requests. In a serving scenario, the grace period is important for reliability because it allows the Pod to finish any active requests that it may be in the middle of processing before it is terminated.

### Health Checks
When you run your application as a container in Kubernetes, it is automatically kept alive for you using a process health check. This health check simply ensures that the main process of your application is always running. If it isn’t, Kubernetes restarts it. However, in most cases, a simple process check is insufficient. For example, if your process has deadlocked and is unable to serve requests, a process health check will still believe that your application is healthy since its process is still running.To address this, Kubernetes introduced health checks for application liveness.

Liveness health checks run application-specific logic, like loading a web page, to verify that the application is not just still running, but is functioning properly. Since these liveness health checks are application-specific, you have to define them in your Pod manifest.

#### Liveness
While the default response to a failed liveness check is to restart the Pod, the actual behavior is governed by the Pod’s restartPolicy. There are three options for the restart policy: Always (the default), OnFailure (restart only on liveness failure or nonzero process exit code), or Never.

#### Readness
Of course, liveness isn’t the only kind of health check we want to perform. Kubernetes makes a distinction between liveness and readiness. Liveness determines if an application is running properly. Containers that fail liveness checks are restarted. Readiness describes when a container is ready to serve user requests. Containers that fail readiness checks are removed from service load balancers. 

#### Startup Probe
Startup probes have recently been introduced to Kubernetes as an alternative way of managing slow-starting containers. When a Pod is started, the startup probe is run before any other probing of the Pod is started.

#### Other Types of Health Checks
In addition to HTTP checks, Kubernetes also supports tcpSocket health checks that open a TCP socket; if the connection succeeds, the probe succeeds. Finally,Kubernetes allows exec probes. These execute a script or program in the context of the container. Following typical convention, if this script returns a zero exit code, the probe succeeds; otherwise, it fails. 

### Resource Management

#### Resource Requests: Minimum Required Resources
The Kubernetes scheduler will ensure that the sum of all requests of all Pods on a node does not exceed the capacity of the node. Therefore, a Pod is guaranteed to have at least the requested resources when running on the node. Importantly, “request” specifies a minimum. It does not specify a maximum cap on the resources a Pod may use. 

#### Capping Resource Usage with Limits
In addition to setting the resources required by a Pod, which establishes the minimum resources available to it, you can also set a maximum on a its resource usage via resource limits.

### Persisting Data with Volumes
When a Pod is deleted or a container restarts, any and all data in the container’s filesystem is also deleted. This is often a good thing, since you don’t want to leave around cruft that happened to be written by your stateless web application. In other cases, having access to persistent disk storage is an important part of a healthy application. Kubernetes models such persistent storage.

#### Using Volumes with Pods
To add a volume to a Pod manifest, there are two new stanzas to add to our configuration. The first is a new spec.volumes section. This array defines all of the volumes that may be accessed by containers in the Pod manifest. It’s important to note that not all containers are required to mount all volumes defined in the Pod. The second addition is the volumeMounts array in the container definition. This array defines the volumes that are mounted into a particular container and the path where each volume should be mounted. Note that two different containers in a Pod can mount the same volume at different mount paths.

#### Different Ways of Using Volumes with Pods
1. <u>Communication/synchronization:</u> To achieve this, the Pod uses an emptyDir volume. Such a volume is scoped to the Pod’s lifespan, but it can be shared between two containers
2. <u>Cache:</u> You want such a cache to survive a container restart due to a health-check failure, and thus emptyDir works well for the cache use case as well.
3. <u>Persistent data:</u> Sometimes you will use a volume for truly persistent data—data that is independent of the lifespan of a particular Pod, and should move between nodes in the cluster if a node fails or a Pod moves to a different machine. 
4. <u>Mounting the host filesystem:</u> Other applications don’t actually need a persistent volume, but they do need some access to the underlying host filesystem. Kubernetes supports the hostPath volume, which can mount arbitrary locations on the worker node into the container.

## Labels and Annotations

Labels are key/value pairs that can be attached to Kubernetes objects such as Pods
and ReplicaSets. They can be arbitrary and are useful for attaching identifying infor‐ 
mation to Kubernetes objects. Labels provide the foundation for grouping objects.
Annotations, on the other hand, provide a storage mechanism that resembles labels:
key/value pairs designed to hold nonidentifying information that tools and libraries 
can leverage. Unlike labels, annotations are not meant for querying, filtering, or 
otherwise differentiating Pods from each other.

### Labels

### Label Selectors

If we specify two selectors separated by a comma, only the objects that satisfy both
will be returned. This is a logical AND operation:
```bash
kubectl get pods --selector="app=bandicoot,ver=2"
```

Operator|Description
---|--:    
key=value|key is set to value
key!=value|key is not set to value
key in (value1, value2)|key is one of value1 or value2
key notin (value1, value2)|key is not one of value1 or value2
key|key is set
!key|key is not set

### Labels in the Kubernetes Architecture
In addition to enabling users to organize their infrastructure, labels play a critical role
in linking various related Kubernetes objects. Kubernetes is a purposefully decoupled 
system. There is no hierarchy and all components operate independently. However, in 
many cases, objects need to relate to one another, and these relationships are defined 
by labels and label selectors.

For example, ReplicaSets, which create and maintain multiple replicas of a Pod, find
the Pods that they are managing via a selector. Likewise, a service load balancer finds 
the Pods to which it should bring traffic via a selector query. When a Pod is created, 
it can use a node selector to identify a particular set of nodes onto which it can be 
scheduled. When people want to restrict network traffic in their cluster, they use 
Network Policy in conjunction with specific labels to identify Pods that should or 
should not be allowed to communicate with each other.

### Annotations

Annotations provide object-scoped key/value metadata storage used by automation
tooling and client libraries. They can also be used to hold configuration data for 
external tools such as third-party schedulers and monitoring tools.

## Service Discovery

While the dynamic nature of Kubernetes makes it easy to run a lot of things, it creates
problems when it comes to finding those things. Most of the traditional network 
infrastructure wasn’t built for the level of dynamism that Kubernetes presents.

### What Is Service Discovery?
The general name for this class of problems and solutions is service discovery. Service-
discovery tools help solve the problem of finding which processes are listening at 
which addresses for which services. A good service-discovery system will enable users 
to resolve this information quickly and reliably. A good system is also low-latency; 
clients are updated soon after the information associated with a service changes. 
Finally, a good service-discovery system can store a richer definition of what that 
service is. For example, perhaps there are multiple ports associated with the service.

The Domain Name System (DNS) is the traditional system of service discovery on
the internet. DNS is designed for relatively stable name resolution with wide and 
efficient caching. It is a great system for the internet but falls short in the dynamic 
world of Kubernetes.

### The Service Object

Real service discovery in Kubernetes starts with a Service object. A Service object is
a way to create a named label selector. Just as the kubectl run command is an easy way to create a Kubernetes deployment,
we can use kubectl expose to create a service. 

### Service DNS
Because the cluster IP is virtual, it is stable, and it is appropriate to give it a DNS
address. Kubernetes provides a DNS service exposed to Pods running in the cluster. This
Kubernetes DNS service was installed as a system component when the cluster was 
first created. The DNS service is, itself, managed by Kubernetes and is a great example 
of Kubernetes building on Kubernetes. The Kubernetes DNS service provides DNS 
names for cluster IPs.

### Readiness Checks
One nice thing the Service object does is track which of your Pods 
are ready via a readiness check. 

### Looking Beyond the Cluster
So far, everything we’ve covered in this chapter has been about exposing services
inside of a cluster. Oftentimes, the IPs for Pods are only reachable from within the 
cluster. At some point, we have to allow new traffic in!

The most portable way to do this is to use a feature called NodePorts, which enhance 
a service even further. In addition to a cluster IP, the system picks a port (or the user 
can specify one), and every node in the cluster then forwards traffic to that port to 
the service.

With this feature, if you can reach any node in the cluster, you can contact a service.
You can use the NodePort without knowing where any of the Pods for that service are 
running. 

If your cluster is in the cloud someplace, you can 
use SSH tunneling with something like this:
```bash
ssh <node> -L 8080:localhost:32711
```
Now if you point your browser to http://localhost:8080, you will be connected to that
service. 

### Load Balancer Integration
The examples that we have seen so far use external load balancers; that is, load
balancers that are connected to the public internet. While this is great for exposing 
services to the world, you’ll often want to expose your application within only your 
private network. To achieve this, use an internal load balancer.  it is 
done in a somewhat ad hoc manner via object annotations.  Here are the settings for some popular clouds:
Google Cloud Platform: cloud.google.com/load-balancer-type: "Internal"

### Endpoints
Some applications (and the system itself) want to be able to use services without
using a cluster IP. This is done with another type of object called an Endpoints object. 

### kube-proxy and Cluster IPs
Cluster IPs are stable virtual IPs that load balance traffic across all of the endpoints
in a service. This magic is performed by a component running on every node in the 
cluster called the kube-proxy.

![kube_proxy](./attachments/kubernetes/kube_proxy.png)

## HTTP Load Balancing with Ingress

But the Service object operates at Layer 4 (according to the OSI model).1 This means
that it only forwards TCP and UDP connections and doesn’t look inside of those con‐ 
nections. Because of this, hosting many applications on a cluster uses many different 
exposed services. In the case where these services are type: NodePort, you’ll have to 
have clients connect to a unique port per service. In the case where these services are 
type: LoadBalancer, you’ll be allocating (often expensive or scarce) cloud resources 
for each service. But for HTTP (Layer 7)-based services, we can do better.

When solving a similar problem in non-Kubernetes situations, users often turn to the
idea of “virtual hosting.” This is a mechanism to host many HTTP sites on a single IP 
address. Typically, the user uses a load balancer or reverse proxy to accept incoming 
connections on HTTP (80) and HTTPS (443) ports. That program then parses the 
HTTP connection and, based on the Host header and the URL path that is requested, 
proxies the HTTP call to some other program. In this way, that load balancer or 
reverse proxy directs traffic for decoding and directing incoming connections to the 
right “upstream” server.

Kubernetes calls its HTTP-based load-balancing system Ingress. Ingress is a
Kubernetes-native way to implement the “virtual hosting” pattern we just discussed. 
One of the more complex aspects of the pattern is that the user has to manage the
load balancer configuration file. In a dynamic environment and as the set of virtual 
hosts expands, this can be very complex. The Kubernetes Ingress system works to 
simplify this by (a) standardizing that configuration, (b) moving it to a standard 
Kubernetes object, and (c) merging multiple Ingress objects into a single config for 
the load balancer.

![kube_ingress](./attachments/kubernetes/kube_ingress.png)

There is no 
“standard” Ingress controller that is built into Kubernetes, so the user must install one 
of many optional implementations.

### Configuring DNS

To make Ingress work well, you need to configure DNS entries to the external address
for your load balancer. You can map multiple hostnames to a single external endpoint 
and the Ingress controller will direct incoming requests to the appropriate upstream 
service based on that hostname.

The ExternalDNS project is a cluster add-on that you can use to manage DNS
records for you. ExternalDNS monitors your Kubernetes cluster and synchronizes 
IP addresses for Kubernetes Service resources with an external DNS provider. 

### Using Ingress
- simple-ingress 
- host-ingress 
- path-ingress

### Running Multiple Ingress Controllers
There are multiple Ingress controller implementations, and you may want to run
multiple Ingress controllers on a single cluster. To solve this case, the IngressClass 
resource exists so that an Ingress resource can request a particular implementation. 

### Multiple Ingress Objects
If you specify multiple Ingress objects, the Ingress controllers should read them
all and try to merge them into a coherent configuration. However, if you specify 
duplicate and conflicting configurations, the behavior is undefined. It is likely that 
different Ingress controllers will behave differently. Even a single implementation 
may do different things depending on nonobvious factors.

### Ingress and Namespaces
Ingress interacts with namespaces in some nonobvious ways. First, due to an abun‐
dance of security caution, an Ingress object can refer to only an upstream service 
in the same namespace. This means that you can’t use an Ingress object to point a 
subpath to a service in another namespace.
However, multiple Ingress objects in different namespaces can specify subpaths for
the same host. These Ingress objects are then merged to come up with the final config 
for the Ingress controller.
This cross-namespace behavior means that coordinating Ingress globally across the
cluster is necessary. If not coordinated carefully, an Ingress object in one namespace 
could cause problems (and undefined behavior) in other namespaces.
Typically there are no restrictions built into the Ingress controller around which
namespaces are allowed to specify which hostnames and paths. Advanced users may 
try to enforce a policy for this using a custom admission controller. There are also 
evolutions of Ingress described in “The Future of Ingress” on page 101 that address 
this problem.

### Serving TLS
When serving websites, it is becoming increasingly necessary to do so securely using
TLS and HTTPS. Ingress supports this (as do most Ingress controllers).
First, users need to specify a Secret with their TLS certificate and keys

```yaml
apiVersion: v1
kind: Secret
metadata:
  creationTimestamp: null
  name: tls-secret-name
type: kubernetes.io/tls
data:
  tls.crt: <base64 encoded certificate>
  tls.key: <base64 encoded private key>
```

Once you have the certificate uploaded, you can reference it in an Ingress object. This
specifies a list of certificates along with the hostnames that those certificates should be 
used for 

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress 
metadata: 
  name: tls-ingress 
spec:
  tls:
  - hosts:
    - alpaca.example.com 
    secretName: tls-secret-name 
rules: ...
```

Uploading and managing TLS secrets can be difficult. In addition, certificates can
often come at a significant cost. To help solve this problem, there is a nonprofit called 
“Let's Encrypt” running a free Certificate Authority that is API-driven. Since it is 
API-driven, it is possible to set up a Kubernetes cluster that automatically fetches 
and installs TLS certificates for you. It can be tricky to set up, but when working, it’s 
very simple to use. The missing piece is an open source project called cert-manager 
created by Jetstack, a UK startup, onboarded to the CNCF. The cert-manager.io 
website or GitHub repository has details on how to install cert-manager and get 
started.

### Alternate Ingress Implementations
The most popular generic Ingress controller is probably the open source NGINX
Ingress controller. 

Traefik is a reverse proxy implemented in Go that also can function as an Ingress
controller. It has a set of features and dashboards that are very developer-friendly.

### The Future of Ingress
The future of HTTP load balancing for Kubernetes looks to be the Gateway API,
which is in the midst of development by the Kubernetes special interest group (SIG) 
dedicated to networking. The Gateway API project is intended to develop a more 
modern API for routing in Kubernetes. Though it is more focused on HTTP balanc‐ 
ing, Gateway also includes resources for controlling Layer 4 (TCP) balancing. 

## ReplicaSets
More often than not, you want multiple replicas of a 
container running at a particular time for a variety of reasons:
- Redundancy: Failure toleration by running multiple instances.
- Scale: Higher request-processing capacity by running multiple instances.
- Sharding: Different replicas can handle different parts of a computation in parallel.

Logically, a user managing a replicated set of Pods considers them as a 
single entity to be defined and managed—and that’s precisely what a ReplicaSet is. 
A ReplicaSet acts as a cluster-wide Pod manager, ensuring that the right types and 
numbers of Pods are running at all times. 

Pods managed by ReplicaSets are 
automatically rescheduled under certain failure conditions, such as node failures and 
network partitions.

### Reconciliation Loops
The central concept behind a reconciliation loop is the notion of desired state versus
observed or current state. Desired state is the state you want. With a ReplicaSet, it is 
the desired number of replicas and the definition of the Pod to replicate.

The reconciliation loop is constantly running, observing the current state of the
world and taking action to try to make the observed state match the desired state. 

### Relating Pods and ReplicaSets
Though ReplicaSets create and 
manage Pods, they do not own the Pods they create. ReplicaSets use label queries to 
identify the set of Pods they should be managing. They then use the exact same Pod 
API that you used directly in Chapter 5 to create the Pods that they are managing. 

### Adopting Existing Containers
at some point, you may want to expand your singleton container into a replicated
service and create and manage an array of similar containers. If ReplicaSets owned 
the Pods they created, then the only way to start replicating your Pod would be to 
delete it and relaunch it via a ReplicaSet. This might be disruptive, as there would 
be a moment when there would be no copies of your container running. However, 
because ReplicaSets are decoupled from the Pods they manage, you can simply create 
a ReplicaSet that will “adopt” the existing Pod and scale out additional copies of those 
containers. In this way, you can seamlessly move from a single imperative Pod to a 
replicated set of Pods managed by a ReplicaSet.

### Designing with ReplicaSets
ReplicaSets are designed to represent a single, scalable microservice inside your
architecture. Their key characteristic is that every Pod the ReplicaSet controller 
creates is entirely homogeneous. 

```yaml
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  labels:
    app: kuard
    version: "2"
  name: kuard
spec:
  replicas: 1
  selector:
    matchLabels:
      app: kuard
      version: "2"
  template:
    metadata:
      labels:
        app: kuard
        version: "2"
  spec:
    containers:
      - name: kuard
        image: "gcr.io/kuar-demo/kuard-amd64:green"
```

### Scaling ReplicaSets
#### Imperative Scaling with kubectl scale
```bash
kubectl scale replicasets kuard --replicas=4
```
need to ensure that any imperative changes are
immediately followed by a declarative change in source control.
#### Declaratively Scaling with kubectl apply
```yaml
...
spec:
  replicas: 3
...
```

### Autoscaling a ReplicaSet
Kubernetes makes a distinction between hori‐ 
zontal scaling, which involves creating additional replicas of a Pod, and vertical 
scaling, which involves increasing the resources required for a particular Pod (such 
as increasing the CPU required for the Pod).

To scale a ReplicaSet, you can run a command like the following:
```bash
kubectl autoscale rs kuard --min=2 --max=5 --cpu-percent=80
```

### Deleting ReplicaSets
If you don't want to delete the Pods that the ReplicaSet is managing, you can set the 
--cascade flag to false to ensure only the ReplicaSet object is deleted and not the 
Pods.
```bash
kubectl delete rs kuard --cascade=false
```

## Deployments
both Pods and ReplicaSets are expected to be tied to specific container images that don’t 
change.

The Deployment object exists to manage the release of new versions. Deployments
represent deployed applications in a way that transcends any particular version. Addi‐ 
tionally, Deployments enable you to easily move from one version of your code to the 
next. This “rollout” process is specifiable and careful. It waits for a user-configurable 
amount of time between upgrading individual Pods. It also uses health checks to 
ensure that the new version of the application is operating correctly and stops the 
deployment if too many failures occur.

Using Deployments, you can simply and reliably roll out new software versions
without downtime or errors. The actual mechanics of the software rollout performed 
by a Deployment are controlled by a Deployment controller that runs in the Kuber‐ 
netes cluster itself. This means you can let a Deployment proceed unattended and 
it will still operate correctly and safely. This makes it easy to integrate Deployments 
with numerous continuous delivery tools and services. 

Remember, Kubernetes is an online, self-healing system. The top-level Deployment
object is managing this ReplicaSet. When you adjust the number of replicas to one, it 
no longer matches the desired state of the Deployment, which has replicas set to 2. 
The Deployment controller notices this and takes action to ensure the observed state 
matches the desired state, in this case readjusting the number of replicas back to two.

If you ever want to manage that ReplicaSet directly, you need to delete the Deploy‐
ment. (Remember to set --cascade to false, or else it will delete the ReplicaSet and 
Pods as well!)

### Creating deployments
The Deployment spec has a very similar structure to the ReplicaSet spec. There is a 
Pod template, which contains a number of containers that are created for each replica 
managed by the Deployment. In addition to the Pod specification, there is also a 
strategy object:
...
  strategy:
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate
...
The strategy object dictates the different ways in which a rollout of new software
can proceed. There are two strategies supported by Deployments: Recreate and 
RollingUpdate. 

If you have a current Deployment 
in progress, you can use `kubectl rollout status` to obtain the current status of that 
rollout.

### Updating Deployments
If you are in the middle of a rollout and you want to temporarily pause it (e.g., if you
start seeing weird behavior in your system that you want to investigate), you can use 
the pause command:
```bash
kubectl rollout pause deployments kuard
```

If, after investigation, you believe the rollout can safely proceed, you can use the
resume command to start up where you left off:
```bash
kubectl rollout resume deployments kuard
```

### Rollout History
Kubernetes Deployments maintain a history of rollouts, which can be useful both for
understanding the previous state of the Deployment and for rolling back to a specific 
version.
You can see the Deployment history by running:
```bash
kubectl rollout history deployment kuard
```
Let's say there is an issue with the latest release and you want to roll back while you
investigate. You can simply undo the last rollout:
```bash
kubectl rollout undo deployments kuard
```
An alternative (and perhaps preferable) way to undo a rollout is to
revert your YAML file and kubectl apply the previous version. In 
this way, your “change tracked configuration” more closely tracks 
what is really running in your cluster.

### Deployment Strategies
When it comes time to change the version of the software implementing your service,
a Kubernetes deployment supports two different rollout strategies, Recreate and 
RollingUpdate. Let's look at each in turn.

#### Recreate Strategy
The Recreate strategy is the simpler of the two. It simply updates the ReplicaSet it
manages to use the new image and terminates all of the Pods associated with the 
Deployment. The ReplicaSet notices that it no longer has any replicas and re-creates 
all Pods using the new image. Once the Pods are re-created, they are running the new 
version.

While this strategy is fast and simple, it will result in workload downtime. Because of
this, the Recreate strategy should be used only for test Deployments where a service 
downtime is acceptable.
#### RollingUpdate Strategy
The RollingUpdate strategy is the generally preferable strategy for any user-facing
service. While it is slower than Recreate, it is also significantly more sophisticated 
and robust. Using RollingUpdate, you can roll out a new version of your service 
while it is still receiving user traffic, without any downtime.

As you might infer from the name, the RollingUpdate strategy works by updating a
few Pods at a time, moving incrementally until all of the Pods are running the new 
version of your software.

##### Managing multiple versions of your service
Importantly, this means that for a while, both the new and the old version of your
service will be receiving requests and serving traffic. This has important implications 
for how you build your software. Namely, it is critically important that each version 
of your software, and each of its clients, is capable of talking interchangeably with 
both a slightly older and a slightly newer version of your software.

##### Configuring a rolling update
The maxUnavailable parameter sets the maximum number of Pods that can be
unavailable during a rolling update.

However, there are situations where you don’t want to fall below 100% capacity, 
but you are willing to temporarily use additional resources to perform a rollout. In 
these situations, you can set the maxUnavailable parameter to 0, and instead control 
the rollout using the maxSurge parameter. 

The maxSurge parameter controls how many extra resources can be created to achieve
a rollout.

Sometimes, however, simply noticing that a Pod has become ready doesn’t give you 
sufficient confidence that the Pod is actually behaving correctly. Some error condi‐ 
tions don't occur immediately. In most real-world scenarios, you want to wait a period of time to 
have high confidence that the new version is operating correctly before you move on 
to updating the next Pod.

Setting minReadySeconds to 60 indicates that the Deployment must wait for 60
seconds after seeing a Pod become healthy before moving on to updating the next 
Pod.

In addition to waiting for a Pod to become healthy, you also want to set a timeout that
limits how long the system will wait. Suppose, for example, the new version of your 
service has a bug and immediately deadlocks. It will never become ready, and in the 
absence of a timeout, the Deployment controller will stall your rollout forever.

In order to set the timeout period, you will use the Deployment parameter progress 
DeadlineSeconds

## DaemonSets

A DaemonSet ensures that a copy of a Pod is running across a set of nodes in a
Kubernetes cluster. DaemonSets are used to deploy system daemons such as log col‐ 
lectors and monitoring agents, which typically must run on every node. DaemonSets 
share similar functionality with ReplicaSets; both create Pods that are expected to be 
long-running services and ensure that the desired state and the observed state of the 
cluster match.

DaemonSets should be used when a single 
copy of your application must run on all or a subset of the nodes in the cluster.

### DaemonSet Scheduler
By default, a DaemonSet will create a copy of a Pod on every node unless a node
selector is used, which will limit eligible nodes to those with a matching set of labels. 
DaemonSets determine which node a Pod will run on at Pod creation time by specify‐ 
ing the nodeName field in the Pod spec. As a result, Pods created by DaemonSets are 
ignored by the Kubernetes scheduler.

Like ReplicaSets, DaemonSets are managed by a reconciliation control loop that
measures the desired state (a Pod is present on all nodes) with the observed state 

DaemonSets require a unique name across all DaemonSets in a given Kubernetes
namespace. Each DaemonSet must include a Pod template spec, which will be used 
to create Pods as needed.

### Limiting DaemonSets to Specific Nodes
#### Adding Labels to Nodes
The first step in limiting DaemonSets to specific nodes is to add the desired set of
labels to a subset of nodes. This can be achieved using the kubectl label command.
The following command adds the ssd=true label to a single node:
```bash
kubectl label nodes k0-default-pool-35609c18-z7tb ssd=true
```

Using a label selector, we can filter nodes based on labels. To list only the nodes
that have the `ssd` label set to `true`, use the `kubectl get nodes` command with the 
`--selector` flag.

### Updating a DaemonSet
With the release of Kubernetes 1.6, DaemonSets gained an equivalent to the 
Deployment object that manages a ReplicaSet rollout inside the cluster.
DaemonSets can be rolled out using the same RollingUpdate strategy that
Deployments use. You can configure the update strategy using the spec.update 
Strategy.type field, which should have the value RollingUpdate. 

## Jobs
A Job creates Pods that run until successful termination (for instance, exit with 0).
In contrast, a regular Pod will continually restart regardless of its exit code. Jobs are 
useful for things you only want to do once, such as database migrations or batch jobs. 

### The Job Object
The Job object is responsible for creating and managing Pods defined in a template in
the job specification. These Pods generally run until successful completion. The Job 
object coordinates running a number of Pods in parallel.

If the Pod fails before a successful termination, the job controller will create a new
Pod based on the Pod template in the job specification. Given that Pods have to be 
scheduled, there is a chance that your job will not execute if the scheduler does not 
find the required resources. Also, due to the nature of distributed systems, there is 
a small chance that duplicate Pods will be created for a specific task during certain 
failure scenarios.

### Job Patterns
Type|Use case|Behavior|completions|parallelism
---|---|---|---|---
One shot|Database migrations|A single Pod running once until successful termination|1|1
Parallel fixed completions|Multiple Pods processing a set of work in parallel|One or more Pods running one or more times until reaching a fixed completion count|1+|1+
Work queue: parallel jobs|Multiple Pods processing from a centralized work queue|One or more Pods running once until successful termination|1|2+

### CronJobs
Sometimes you want to schedule a job to be run at a certain interval. To achieve this, you can declare a CronJob in Kubernetes, which is responsible for creating a new Job object at a particular interval. Note the `spec.schedule` field, which contains the interval for the CronJob in standard cron format.

## ConfigMaps and Secrets

ConfigMaps are used to
provide configuration information for workloads. This can be either fine-grained 
information like a string or a composite value in the form of a file. Secrets are similar 
to ConfigMaps but focus on making sensitive information available to the workload. 
They can be used for things like credentials or TLS certificates.

### ConfigMaps
One way to think of a ConfigMap is as a Kubernetes object that defines a small
filesystem. Another way is as a set of variables that can be used when defining the 
environment or command line for your containers. The key thing to note is that 
the ConfigMap is combined with the Pod right before it is run. This means that the 
container image and the Pod definition can be reused by many workloads just by 
changing the ConfigMap that is used.

### Using a ConfigMap
There are three main ways to use a ConfigMap:
- Filesystem: You can mount a ConfigMap into a Pod. A file is created for each entry based on 
the key name. The contents of that file are set to the value.
- Environment variable: A ConfigMap can be used to dynamically set the value of an environment 
variable.
- Command-line argument: Kubernetes supports dynamically creating the command line for a container 
based on ConfigMap values.

### Secrets
Secrets enable container images to be created without bundling sensitive data. This
allows containers to remain portable across environments. Secrets are exposed to 
Pods via explicit declaration in Pod manifests and the Kubernetes API. In this way, 
the Kubernetes Secrets API provides an application-centric mechanism for exposing 
sensitive configuration information to applications in a way that’s easy to audit and 
leverages native OS isolation primitives.

By default, Kubernetes Secrets are stored in plain text in the etcd 
storage for the cluster. Depending on your requirements, this may 
not be sufficient security for you. In particular, anyone who has 
cluster administration rights in your cluster will be able to read all 
of the Secrets in the cluster.

In recent versions of Kubernetes, support has been added for
encrypting the Secrets with a user-supplied key, generally integra‐ 
ted into a cloud key store. 

### Consuming Secrets
Instead of accessing Secrets through the API server, we can use a Secrets volume.
Secret data can be exposed to Pods using the Secrets volume type. Secrets volumes are 
managed by the kubelet and are created at Pod creation time. Secrets are stored on 
tmpfs volumes (aka RAM disks), and as such are not written to disk on nodes.

### Naming Constraints
they must conform to the regular expression `^[.]?[a-zAZ0-9]([.]?[a-zA-Z0-9]+[-_a-zA-Z0-9]?)*$`.

Valid key name|Invalid key name
---|---
.auth_token|Token..properties
Key.pem|auth file.json
config_file|_password.txt

ConfigMap data values are simple UTF-8 text specified directly in the manifest. 
Secret data values hold arbitrary data encoded using base64. The use of base64 
encoding makes it possible to store binary data. This does, however, make it more 
difficult to manage Secrets that are stored in YAML files as the base64-encoded value 
must be put in the YAML. Note that the maximum size for a ConfigMap or Secret is 
1 MB.

## Role-Based Access Control for Kubernetes (RBAC)
Role-based access control provides a mechanism for restricting both access to and
actions on Kubernetes APIs to ensure that only authorized users have access. RBAC 
is a critical component to both harden access to the Kubernetes cluster where you are 
deploying your application and (possibly more importantly) prevent unexpected acci‐ 
dents where one person in the wrong namespace mistakenly takes down production 
when they think they are destroying their test cluster.

While RBAC can be quite useful in limiting access to the Kuber‐ 
netes API, it’s important to remember that anyone who can run 
arbitrary code inside the Kubernetes cluster can effectively obtain 
root privileges on the entire cluster. 

### Identity in Kubernetes
Every request to Kubernetes is associated with some identity. Even a request with no
identity is associated with the system:unauthenticated group. Kubernetes makes a 
distinction between user identities and service account identities. Service accounts 
are created and managed by Kubernetes itself and are generally associated with com‐ 
ponents running inside the cluster. User accounts are all other accounts associated 
with actual users of the cluster, and often include automation like continuous delivery 
services that run outside the cluster.
Kubernetes uses a generic interface for authentication providers. Each of the provid‐
ers supplies a username and, optionally, the set of groups to which the user belongs. 
Kubernetes supports a number of authentication providers, including:
- HTTP Basic Authentication (largely deprecated)
- x509 client certificates
- Static token files on the host
- Cloud authentication providers, such as Azure Active Directory and AWS Iden‐
tity and Access Management (IAM)
- Authentication webhooks
While most managed Kubernetes installations configure authentication for you, if 
you are deploying your own authentication, you will need to configure flags on the 
Kubernetes API server appropriately.

### Understanding Roles and Role Bindings
Identity is just the beginning of authorization in Kubernetes. Once Kubernetes knows
the identity of the request, it needs to determine if the request is authorized for that 
user. To achieve this, it uses roles and role bindings.

A role is a set of abstract capabilities. For example, the appdev role might represent
the ability to create Pods and Services. A role binding is an assignment of a role to one 
or more identities. Thus, binding the appdev role to the user identity alice indicates 
that Alice has the ability to create Pods and Services.

### Roles and Role Bindings in Kubernetes
In Kubernetes, two pairs of related resources represent roles and role bindings. One
pair is scoped to a namespace (Role and RoleBinding), while the other pair is scoped 
to the cluster (ClusterRole and ClusterRoleBinding).

Sometimes you need to create a role that applies to the entire cluster, or you want to
limit access to cluster-level resources. To achieve this, you use the ClusterRole and 
ClusterRoleBinding resources. They are largely identical to their namespaced peers, 
but are cluster-scoped.

#### Verbs for Kubernetes roles
Roles are defined in terms of both a resource (e.g., Pods) and a verb that describes
an action that can be performed on that resource. 

Verb|HTTP method|Description
---|---|---
create|POST|Create a new resource.
delete|DELETE|Delete an existing resource.
get|GET|Get a resource.
list|GET|List a collection of resources.
patch|PATCH|Modify an existing resource via a partial change.
update|PUT|Modify an existing resource via a complete object.
watch|GET|Watch for streaming updates to a resource.
proxy|GET|Connect to resource via a streaming WebSocket proxy.

#### Using built-in roles
Designing your own roles can be complicated and time-consuming. Kubernetes has
a large number of built-in cluster roles for well-known system identities (e.g., a 
scheduler) that require a known set of capabilities. You can view these by running:
```bash
kubectl get clusterroles
```
While most of these built-in roles are for system utilities, four are designed for
generic end users:
- The cluster-admin role provides complete access to the entire cluster.
- The admin role provides complete access to a complete namespace. 
- The edit role allows an end user to modify resources in a namespace. 
- The view role allows for read-only access to a namespace.

Most clusters already have numerous ClusterRole bindings set up, and you can view
these bindings with kubectl get clusterrolebindings.

### Techniques for Managing RBAC
#### Testing Authorization with can-i
```bash
kubectl auth can-i create pods
```

#### Managing RBAC in Source Control
The kubectl command-line tool provides a reconcile command that operates
somewhat like kubectl apply and will reconcile a set of roles and role bindings with the current state of the cluster. You can run:
```bash
kubectl auth reconcile -f <rbac-config-file.yaml>
```
If you want to see changes before they are made, you can add the --dry-run flag to
the command to output, but not apply, the changes.

### Advanced Topics
#### Aggregating ClusterRoles
Sometimes you want to be able to define roles that are combinations of other roles. Kubernetes RBAC 
supports the usage of an aggregation rule to combine multiple roles in a new role. 
This new role combines all of the capabilities of all of the aggregate roles, and any 
changes to any of the constituent subroles will automatically be propogated back into 
the aggregate role.

As with other aggregations or groupings in Kubernetes, the ClusterRoles to be aggre‐
gated are specified using label selectors. In this particular case, the aggregationRule 
field in the ClusterRole resource contains a clusterRoleSelector field, which in 
turn is a label selector. All ClusterRole resources that match this selector are dynami‐ 
cally aggregated into the rules array in the aggregate ClusterRole resource.

A best practice for managing ClusterRole resources is to create a number of fine-
grained cluster roles and then aggregate them to form higher-level or broader cluster 
roles. This is how the built-in cluster roles are defined. 

#### Using Groups for Bindings
When managing a large number of people in different organizations with similar
access to the cluster, it’s generally a best practice to use groups to manage the roles 
that define access, rather than individually adding bindings to specific identities. 
To bind a group to a ClusterRole, use a Group kind for the subject in the binding.

## Service Meshes

## Integrating Storage Solutions and Kubernetes

