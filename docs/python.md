# Python

Problem: Given an integer n, return true if it is possible to represent n as the sum of distinct powers of three. Otherwise, return false.

Example 1:
Input: n = 12
Output: true
Explanation: 12 = 3^1 + 3^2

Example 2:
Input: n = 91
Output: true
Explanation: 91 = 3^0 + 3^2 + 3^4

Example 3:
Input: n = 21
Output: false

```python
class Solution:

    def __init__(self):
        self.x = 1000

    def checkPowersOfThree(self, n: int) -> bool:
        return self.doRecursive(n=n)

    def doRecursive(self, n: int) -> bool:
        if self.x < 0:
            return True
        elif n//(3**self.x) > 1: 
            return False
        elif n//(3**self.x) == 0:
            self.x-=1
            return self.doRecursive(n)
        elif n//(3**self.x) == 1:
            new_n = n%(3**self.x)
            self.x-=1
            return self.doRecursive(new_n)
```