# DBT Fundamentals

https://gitlab.com/mw51.mw/documentation/-/blob/main/docs/_old/dbt.md?ref_type=heads

## Models

## Sources

## Tests & Documentations

## Deployment

## Macros

## Sides

usage d'un adapter pour établir une connexion entre dbt et bigquery par exemple

Astronomer Cosmos is an open-source library that bridges Apache Airflow and dbt, allowing you to easily transform your dbt projects into Airflow DAGs and manage everything seamlessly. With Cosmos, you can write your data transformations using dbt and then schedule and orchestrate them with Airflow, making the entire process smooth and straightforward.

The Astro CLI is a command-line interface for data orchestration. It allows you to get started with Apache Airflow quickly and it can be used with all Astronomer products.  With the Astro CLI, you can run Airflow on your local machine.

https://astronomer.github.io/astronomer-cosmos/getting_started/astro.html

https://www.astronomer.io/docs/learn/connections/
https://www.astronomer.io/docs/learn/what-is-an-operator/ like SQLExecuteQueryOperator

## Organisation type d'un repository DBT.

```
jaffle_shop
├── README.md
├── analyses
├── seeds
│   └── employees.csv
├── dbt_project.yml
├── macros
│   └── cents_to_dollars.sql
├── models
│   ├── intermediate
│   │   └── finance
│   │       ├── _int_finance__models.yml
│   │       └── int_payments_pivoted_to_orders.sql
│   ├── marts
│   │   ├── finance
│   │   │   ├── _finance__models.yml
│   │   │   ├── orders.sql
│   │   │   └── payments.sql
│   │   └── marketing
│   │       ├── _marketing__models.yml
│   │       └── customers.sql
│   ├── staging
│   │   ├── jaffle_shop
│   │   │   ├── _jaffle_shop__docs.md
│   │   │   ├── _jaffle_shop__models.yml
│   │   │   ├── _jaffle_shop__sources.yml
│   │   │   ├── base
│   │   │   │   ├── base_jaffle_shop__customers.sql
│   │   │   │   └── base_jaffle_shop__deleted_customers.sql
│   │   │   ├── stg_jaffle_shop__customers.sql
│   │   │   └── stg_jaffle_shop__orders.sql
│   │   └── stripe
│   │       ├── _stripe__models.yml
│   │       ├── _stripe__sources.yml
│   │       └── stg_stripe__payments.sql
│   └── utilities
│       └── all_dates.sql
├── packages.yml
├── snapshots
└── tests
    └── assert_positive_value_for_total_amount.sql
```

All YAML files are configuration files for DBT. Because configs can be set in multiple places, they are also applied hierarchically. An individual resource might inherit or override configs set elsewhere.
Depending on the resource type, configurations can be defined in the dbt project and also in an installed package by:
1. Using a config() Jinja macro within a model, snapshot, or test SQL file
2. Using a config property in a .yml file in the models/, snapshots/, seeds/, analyses/, or tests/ directory.
3. From the dbt_project.yml file, under the corresponding resource key (models:, snapshots:, tests:, etc)
The most specific config always takes precedence. This generally follows the order above: an in-file `config()` block --> properties defined in a `.yml` file --> config defined in the project file.
Few configs like tags, pre-hook, post-hook and meta are additive/merged.

```
-- dbt_project.yml

models:
  jaffle_shop:
    staging:
      +materialized: view
    intermediate:
      +materialized: ephemeral
    marts:
      +materialized: table
      finance:
        +schema: finance
      marketing:
        +schema: marketing
```
Every dbt project needs a dbt_project.yml file — this is how dbt knows a directory is a dbt project. It also contains important information that tells dbt how to operate your project.

```
-- packages.yml

packages:
  - package: dbt-labs/snowplow
    version: 0.7.0

  - git: "https://github.com/dbt-labs/dbt-utils.git"
    revision: 0.9.2

  - tarball: https://codeload.github.com/dbt-labs/dbt-utils/tar.gz/0.9.6
    name: 'dbt_utils'

  - local: /opt/dbt/redshift

  - private: dbt-labs/awesome_repo
    revision: "0.9.5"
    provider: "github"
```

Add any package dependencies to packages.yml. Run `dbt deps` to install the packages. Packages get installed in the dbt_packages directory – by default this directory is ignored by git, to avoid duplicating the source code for the package. Where name: 'dbt_utils' specifies the subfolder of dbt_packages that's created for the package source code to be installed within. Use the private key in your packages.yml or dependencies.yml to clone package repos using your existing dbt Cloud Git integration without having to provision an access token or create a dbt Cloud environment variable.

```
projects:
  - name: jaffle_finance
```
Add any project dependencies to dependencies.yml. One of the limitations of project dependencies in dbt is that they do not support private packages because they do not support Jinja rendering or conditional configuration. Additionally, project dependencies are acyclic, which means a new model in an upstream project cannot import and use a public model from a downstream project.

You can create a .dbtignore file in the root of your dbt project to specify files that should be entirely ignored by dbt. The file behaves like a .gitignore file, using the same syntax. 

```
-- dbt_project.yml

clean-targets: [target, dbt_packages, logs]
```

Remove packages, compiled files and logs as part of `dbt clean` command.

By default, dbt will install packages in the `dbt_packages` directory, i.e. `packages-install-path: dbt_packages`

## Profile
The profile your dbt project should use to connect to your data warehouse.
- If you are developing in dbt Cloud: This configuration is not applicable
- If you are developing locally: This configuration is required, unless a command-line option (i.e. --profile) is supplied.

## Exemple d'architecture de travail entre DBT, Composer et BigQuery.

### BigAuery Configuration DBT project file

```
-- dbt_project.yml

models:
  <resource-path>:
    +materialized: materialized_view
    +on_configuration_change: apply | continue | fail
    +cluster_by: <field-name> | [<field-name>]
    +partition_by:
      - field: <field-name>
      - data_type: timestamp | date | datetime | int64
        # only if `data_type` is not 'int64'
      - granularity: hour | day | month | year
        # only if `data_type` is 'int64'
      - range:
        - start: <integer>
        - end: <integer>
        - interval: <integer>
    +enable_refresh: true | false
    +refresh_interval_minutes: <float>
    +max_staleness: <interval>
    +description: <string>
    +labels: {<label-name>: <label-value>}
    +hours_to_expiration: <integer>
    +kms_key_name: <path-to-key>
```
## Commands

```python

# compile dbt models to SQL
dbt compile

# Run specific models
dbt run --select cool_waffle

# Skip certain models
dbt run --exclude boring_jaffle

# Rebuild everything from scratch
dbt run --full-refresh

# Pass variables to models
dbt run --vars '{"my_var": "value"}'

# Speed up runs with multiple threads
dbt run --threads 4


dbt test

# Test specific models
dbt test --select critical_data

# Run schema tests only
dbt test --select "test_type:generic"


dbt source freshness


dbt docs generate
dbt docs serve

dbt run --debug

dbt snapshot

dbt build

dbt seed

dbt ls

# List the most important resources
dbt ls --select tag:important

dbt show --select cool_waffle

dbt retry

dbt run-operation crazy_macro

dbt clone --state path/to/artifacts
```

# Exemple Requete SQL

# BIGQUERY SQL

The `WHERE` clause is used to filter rows based on a specific condition. It is applied to individual records.

The `HAVING` clause is similar to the WHERE clause, but it is used to filter groups (GROUP BY) of records, rather than individual records.

The `QUALIFY` clause is a bit different from the WHERE and HAVING clauses. It filters the results of window (or analytic) functions. Your window function is required SELECT list or directly in the QUALIFY clause.

Could be use for deduplication of rows in a table. Python example :

```python
from jinja2 import Template
from typing import Optional, Union, List

QUERY_TEMPLATE = '''
SELECT
{%- if fields is none %}
    *
{%- else %}
  {%- for field in fields %}
    {{ field }} {{- ", " if not loop.last else "" }}
  {%- endfor %}
{%- endif %}
FROM `{{ table_reference }}`
{%- if primary_keys is none %}
QUALIFY ROW_NUMBER() OVER() = 1
{% else %}
QUALIFY ROW_NUMBER() OVER(
    PARTITION BY 
      {%- for primary_key in primary_keys %}
        {{ primary_key }} {{- ", " if not loop.last else "" }}
      {%- endfor %}    
    {%- if ordering_expressions is not none %}
    ORDER BY 
      {%- for ordering_expression in ordering_expressions %}
        {{ ordering_expression }} {{- ", " if not loop.last else "" }}
      {%- endfor %} 
    {%- endif %}
) = 1
{%- endif -%}
; 
'''


def get_deduplication_query(
        table_reference: str,
        fields: Optional[List] = None,
        primary_keys: Optional[Union[str, List]] = None,
        ordering_expressions: Optional[Union[str, List]] = None
    ) -> str:
    """Create a deduplication query for a table given the primary keys and
    the ordering expressions. 

    :param fields: Table reference to deduplicate.
    :param table_reference: Table reference to deduplicate.
        must have a `(<projec_id>)?.<dataset_id>.<table_name>` pattern.
    :param primary_keys: Primary key(s) name of the table if exist. 
        If not specified, all the fields are considered as a primary key.
    :param ordering_expressions: Field(s) to order on. Can be considered as an expression. 
        For instance, you can add " DESC" to the field to invert the order. (ex: "creation_date DESC")
        If not specified, rows are arbitrarily ordered.
    :return: Deduplication query for the table.
    """


    if primary_keys is None:
        # Is not necessary anymore as it will be considered as a primary key
        ordering_expressions = None

    # Create a list if only a string was given        
    to_list_if_str = lambda x: [x] if isinstance(x, str) else x

    primary_keys = to_list_if_str(primary_keys)
    ordering_expressions = to_list_if_str(ordering_expressions)

    # Render the Jinja Template
    params = {
        'table_reference': table_reference,
        'fields': fields,
        'primary_keys': primary_keys,
        'ordering_expressions': ordering_expressions,
    }

    query = Template(QUERY_TEMPLATE).render(**params)

    return query


if __name__ == '__main__':
    query = get_deduplication_query(
        table_reference='bigquery-public-data.austin_waste.waste_and_diversion',
        fields=[
            'load_id', 'report_date', 'load_type', 'load_time', 'load_weight', 'dropoff_site', 'route_type', 'route_number'
        ],
        primary_keys='load_id',
        ordering_expressions=['report_date DESC', 'load_time DESC'],
    )
    print(query)
```


