# Présentation

Bonjour, je m’appelle Adrien, je suis Data Engineer avec un peu plus de deux ans d’expérience. J’ai fait mes études à l’ENSAI, une école d’ingénieur qui forme les futurs statisticiens de l’INSEE mais aussi des data scientists comme moi. Je me suis spécialisé dans le Big Data avec le traitement de gros volumes de données. 

J’ai commencé ma carrière chez Datafab, une société de conseil pour laquelle j’ai effectué un stage de pré-embauche de 6 mois où j’ai travaillé sur un projet d’analyse de rentabilité immobilière. 

Ensuite, j’ai été placé en mission à la Société Générale pendant plus d’un 1 an et demi sur des sujets autour de la supervision des accès et de l’IAM. J’ai eu l’occasion d’optimiser des traitements Spark, de mettre en place un POC Kafka/Spark Streaming et d’automatiser des déploiements avec Terraform et Ansible. 

À la suite de ça, j’ai pu avec l’aide d’un autre collaborateur animer une formation autour des bases de Kafka, sa mise en place et son utilisation, pour une équipe d’une vingtaine de développeurs de Stelogy, une entreprise spécialisée dans les télécoms. 

Aujourd’hui, je travaille sur un projet e-commerce en interne, où je mets en place une infrastructure Kubernetes bare-metal. L’objectif est de capitaliser sur les connaissances, les savoirs faire et les bonnes pratiques des consultants autour du DevOps afin d’explorer de nouvelles approches de facturation en mode forfait. 

J’en ai aussi profité pour passer deux certifications, la première sur Terraform et la seconde sur GCP en obtenant la Professional Data Engineer Certification. 

À côté de tout ça, j’aime bien tester de nouvelles technos à travers des projets plus personnels. Par exemple, le dernier en date et celui où je me suis le plus impliqué est le projet RLCStatistics. C’est un site web de pari esport autour du jeu vidéo Rocket League. 

En même temps, je suis aussi vacataire pour mon école d’origine où je donne des cours d'informatique et de mathématiques à Rennes. 

Si vous le souhaitez, je peux développer plus en détail mes différentes expériences, ce que j’ai réalisé et les résultats obtenus.

J’ai accepté de m'entretenir avec vous car j’ai envie de découvrir un nouvel environnement et trouver une nouvelle opportunité chez un client. 

# Expériences

## Datafab

Lors de mon stage de pré-embauche chez Datafab, j’ai travaillé sur un projet d’analyse de rentabilité immobilière. L’objectif était d’identifier les zones les plus intéressantes pour un investissement en comparant les prix des biens mis en vente aux loyers des biens similaires dans 3 villes: Paris, Bordeaux et Angoulême. Pour cela, nous avons établi plusieurs KPI : rendement brut, net et super net.

Mon rôle consistait à mettre en place un pipeline de données, de la collecte à la mise à disposition, en passant par le traitement, ainsi qu’un support de visualisation pour faciliter la prise de décision.

Dans un premier temps, j’ai collecté les données issues de Leboncoin, SeLoger et DVF, qui étaient stockées sur AWS S3. Ensuite, j’ai conçu un pipeline de traitement des données avec Spark sur un cluster EMR. Les principales étapes de traitement incluent le nettoyage des données (suppression des annonces aberrantes ou frauduleuses), l’extraction de features à partir des descriptions et la détection des doublons. Pour orchestrer ces différentes étapes, j’ai utilisé Step Functions, ce qui nous a permis d’avoir un workflow structuré et automatisé.

Je me suis également impliqué sur la partie modélisation et prédiction des prix en fonction du type de bien et de sa localisation. J’ai intégré une dimension MLOps pour suivre l’évolution du marché en temps réel. Ce pipeline s'exécute automatiquement à chaque nouveau batch de donnée reçue, quasiment quotidiennement.

Enfin, j’ai développé une application web conteneurisée avec Streamlit et Docker. Elle intégrait des cartes interactives permettant aux employés de Datafab d’explorer visuellement les zones les plus rentables et de simuler des investissements.

Grâce à ce projet, j’ai pu livrer une application fonctionnelle en partant de zéro et mettre en avant les zones à fort potentiel. Cela m’a aussi permis de monter en compétences sur des technologies jusqu’alors théoriques, comme AWS, Terraform pour l’IaC et GitLab CI/CD et surtout Spark, en développant une vraie culture DevOps.

## Société Générale

J’ai été en mission chez la Société Générale, au sein d’une équipe en charge de la supervision des accès IAM. L’objectif était de réconcilier les profils et permissions des applications internes du groupe afin de vérifier la conformité des accès utilisateurs et de fournir une interface web facilitant le travail d’audit.

Mon rôle a été d’optimiser les traitements Spark et de répondre à de nouveaux besoins, notamment ceux de la BCE, en créant par exemple des KPI sur le nombre d’applications contrôlées.

J’ai analysé les traitements Spark existants et identifié les goulots d’étranglement qui ralentissaient les exécutions. Pour améliorer les performances, j’ai appliqué plusieurs optimisations :
- Réduction des écritures sur disque,
- La mise en cache de la data,
- Meilleure répartition des données pour limiter le shuffle,
- Analyse et gestion de la skewness des données.

Grâce à ces améliorations, le traitement Spark principal de réconciliation des accès, qui prenait initialement plus de 4 heures, a été ramené à seulement 20 minutes.

En parallèle, j’ai travaillé sur une uniformisation des formats de données en entrée, ce qui a nécessité une refonte complète des configurations et des jobs de déploiement. L’enjeu était de passer de 35 applications supervisées à plus de 800, ce qui demandait une scalabilité accrue. J’ai donc conçu un système automatisé permettant d’intégrer toute nouvelle application sans intervention manuelle, à condition qu’elle respecte un format de données défini. Pour assurer la qualité des données entrantes, j’ai développé des scripts de validation qui rejettaient et notifiaient les responsables d’application en cas d’erreur.

Enfin, j’ai réalisé un POC sur Kafka et Spark Streaming pour démontrer la faisabilité du traitement en continu au sein de l’équipe. Jusqu’à présent, tous nos calculs étaient effectués quotidiennement, une fois par jour, mais un nouveau cas nécessitaient une remontée d’alerte en temps réel pour une application critique. Le POC a été un succès et a été validé pour une mise en production.

## Stelogy

Après avoir travaillé sur un POC sur Kafka et Spark Streaming à la Société Générale, j’ai été sollicité pour accompagner un data architecte senior dans la mise en place d’une formation d’une semaine sur Kafka chez Stelogy, une entreprise de télécommunications.

Leur objectif était de prendre rapidement en main Kafka afin de développer un use case permettant l’actualisation en temps réel d’une base de données clients. Mon rôle était de les accompagner sur l’installation et l’exploitation d’un cluster Kafka, tout en leur montrant comment l’intégrer à leur système d’information existant.

Je me suis particulièrement concentré sur le développement du contenu de la formation, en leur fournissant un repository Git permettant d’instancier un cluster Kafka en local ou sur des VM avec Docker. J’ai également abordé la gestion des certificats de sécurité nécessaires au bon fonctionnement du cluster:
- PlainText, SSL et SASL pour sécuriser les communications entre les brokers et avec l’extérieur.

En complément, j’ai fourni des exemples concrets de consumers et producers en Java, en expliquant les paramètres essentiels du cluster, comme :
- L’envoi et la lecture de messages,
- La reprise d’un flux à partir d’un offset précis,
- Les permissions avec les ACLs.

Au final, nous avons réussi à monter une formation complète en un temps très court, et les retours des développeurs étaient positifs. Ils ont tous réussi à instancier un cluster Kafka en local puis à créer un cluster dans un environnement d’homologation et y envoyer et lire des messages entre eux.

C’était une expérience enrichissante, très différente d’une mission classique, qui m’a demandé beaucoup d’efforts dans un délai restreint, mais qui s’est révélée très formatrice pour moi aussi.

## Alteca

Actuellement, je travaille sur un projet e-commerce interne chez Alteca, où je mets en place une infrastructure Kubernetes en bare metal. L’objectif est double : renforcer notre expertise en DevOps et structurer une nouvelle offre forfaitaire pour de futurs clients.

Le cluster est déployé sur des VM Google Cloud, avec deux approches pour sa configuration :
1. Via des scripts bash d’initialisation exécutés par Terraform,
2. Avec Ansible et son inventaire.

Une fois le cluster opérationnel, je déploie nos charts Helm pour instancier les services nécessaires au projet, notamment :
- ArgoCD pour l’orchestration,
- Spark pour le compute,
- PostgreSQL comme base de données,
- Les services web, incluant l’API et l’UI.

Le projet est encore en développement, et nous avançons progressivement. L’architecture mise en place offre plusieurs avantages :
- Une documentation complète, illustrée par un cas d’usage concret,
- Une montée en compétence rapide pour d’autres consultants,
- Une infrastructure scalable et robuste, prête à être utilisée sur d’autres projets

# Projets

## RLCStatistics

Je suis passionné par Rocket League, je suis toutes les compétitions, les rosters, tout ce qui touche à l’esport sur ce jeu. Et comme j’aime bien tester de nouvelles choses, je me suis dit que j’allais créer un site web de paris esport sur les matchs de Rocket League, un peu à la manière des prédictions sur Twitch.

L’idée, c’était d’avoir un site fiable et surtout pas cher à maintenir, qui récupère automatiquement les données des matchs passés, qui puisse prédire les résultats des futurs matchs et qui propose une sorte de gamification où les utilisateurs peuvent parier pour accumuler des pièces virtuelles.

Pour la partie technique, j’ai hébergé l’application sur Google Cloud Platform, et j’ai vraiment cherché à optimiser les coûts au maximum. Par exemple, toutes les données sont stockées dans un bucket GCS, et au lieu d’utiliser une base de données NoSQL classique, je manipule directement les fichiers de mon bucket. Il a fallu optimiser les manipulations des blobs pour éviter la redondance et ne traiter que le strict nécessaire. Le backend est réalisé en Python et exécuté le script à l’aide d’une cloud function. Pour la partie frontend, l’API est fait avec FastAPI, et l’UI en React et TypeScript. J’ai conteneurisé tout ça avec Docker et j’ai déployé l’ensemble sur Cloud Run.

J’ai aussi mis en place une CI/CD pour que les mises à jour se fassent automatiquement, ce qui me fait gagner un temps considérable quand je veux déployer une nouvelle version de l’API ou de l’UI. J’ai également travaillé sur l’indexation des documents pour établir une recherche de matchs passés relativement peu coûteuse en temps.

Ce qui était intéressant dans ce projet, c’était toute la réflexion autour de la gestion des données et des coûts. J’ai réussi à faire tourner le site pour moins d’un euro par mois, en partie parce que, pour l’instant, il n’y a quasiment pas de trafic. Mais ce n’était pas forcément l’objectif, c’était avant tout un projet perso pour tester des choses et monter en compétences, notamment sur GCP et sur le développement full-stack. Et au final, c’est un projet dont je suis assez fier, même s’il reste encore plein de choses à améliorer.

Lien de mon Gitlab : [Besstial](https://gitlab.com/Besstial)