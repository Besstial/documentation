# GCP

to do

## Compute Engine

## Google Kubernetes Engine

## Cloud Functions

## Cloud Dataflow

## BigQuery

### SQL

The `WHERE` clause is used to filter rows based on a specific condition. It is applied to individual records.

The `HAVING` clause is similar to the WHERE clause, but it is used to filter groups (GROUP BY) of records, rather than individual records.

The `QUALIFY` clause is a bit different from the WHERE and HAVING clauses. It filters the results of window (or analytic) functions. Your window function is required SELECT list or directly in the QUALIFY clause.

## Cloud Dataproc

## Cloud Pub/Sub

## Cloud Storage

## Les services Google selon votre besoin
- Entrepôt de données : BigQuery 
- Messagerie : Pub/Sub 
- Service de requête : BigQuery 
- Ingestion de données par flux : Pub/Sub 
- Traitement de données par flux : Dataflow 
- Orchestration des workflows : Cloud Composer 
- Stockage de données de document : Firestore 
- Stockage de données en mémoire : Memorystore 
- FaaS : Cloud Functions 
- PaaS : App Engine 
- Migration de bases de données : SQL Database Migration Service 
- CDN : Cloud CDN 
- Domaines et DNS Cloud : Domains 
- Sécurité du réseau : Cloud VPN 
- CI/CD : Cloud Build

## Les DB NoSQL
- MongoDB
- Cassandra
- Redis
- Couchbase
- Apache HBase