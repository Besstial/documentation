# Deep Work

<u>Définition du travail en profondeur :</u> activités professionnelles menées dans un état de concentration absolue qui pousse vos capacités cognitives jusqu’à leurs limites. Ces efforts créent de la valeur, améliorant votre savoir-faire et sont difficiles à reproduire.
Les travailleurs du savoir perdent contact avec le travail en profondeur à cause des outils de réseau laissant sa place à son alternative.

<u>Définition du travail superficiel :</u> tâches logistiques non exigeantes sur le plan cognitif, souvent exécutées en étant distrait. Ces efforts ont tendance à ne pas créer beaucoup de valeur et sont faciles à reproduire.

Pour rester précieux dans notre économie, vous devez maîtriser l’art d’apprendre rapidement des choses complexes. Et cette tâche nécessite un travail en profondeur. À cause de la révolution des réseaux numériques, la nécessité du travail en profondeur est récente.

<u>L’hypothèse du travail en profondeur :</u> la capacité à travailler en profondeur se fait de plus en plus rare tout en devenant de plus en plus précieuse dans notre économie. En conséquence, ce sont les rares personnes à entretenir cette habileté, puis à l’inscrire au cœur de leur vie professionnelle, qui s’épanouiront et connaîtront la réussite. 

Le but du livre est de convaincre à l’utilisation du travail en profondeur.

## Le travail en profondeur est une expérience d’une grande valeur

Dans notre économie, de plus en plus d’individus sont désormais en concurrence avec les rockstars de leur secteur. Dans cette nouvelle économie, trois groupes auront un avantage particulier : ceux qui sont capables de bien travailler, et faisant preuve de créativité, ceux qui sont les meilleurs dans leur domaine et ceux qui ont accès au capital. Deux capacités essentielles pour réussir dans la nouvelle économie : 

- La capacité à maîtriser rapidement les choses difficiles.
- La capacité à produire un travail d’un niveau exceptionnel, en termes de qualité et de rapidité.

Les deux capacités essentielles que nous venons de décrire sont conditionnées par votre faculté de réaliser un travail en profondeur. Selon Sertillanges, pour apprendre, il faut une concentration intense. Pratique délibérée : 

- Votre attention est centrée sur une habileté bien précise que vous essayer d’améliorer ou sur une idée que vous tentez de maîtriser.
- Vous recevez un retour qui vous permettra de corriger votre approche, de façon que votre attention reste centrée sur l’élément source de la plus grande productivité.

En se concentrant intensément sur une habileté spécifique, vous forcez le circuit neuronal concerné à s’activer encore et encore, de manière isolée. En revanche, si vous essayez d’apprendre une nouvelle habileté complexe dans un état de concentration peu intense, vous activez trop de circuits simultanément et aléatoirement pour isoler les groupes de neurones que vous souhaitez renforcer. Exemple de Grant

Production d’un travail d’excellente qualité = (Temps passé) * (Intensité de la concentration)
Concept de restes d’attention : Lorsque vous passez d’une tâche A à une tâche B, votre attention ne suit pas immédiatement  - des restes d’attention demeurent centrés sur la tâche A. Exemple de checker ses mails 

Pour produire une performance au sommet, vous devez exécuter une seule tâche sur de longues périodes et sans être distrait. Autrement dit, le type de travail qui optimise votre performance est le travail en profondeur. Le cas des dirigeants : le travail en profondeur n’est pas nécessaire dans leur réussite, mais plutôt au cumul des années d’expérience à sentir le marché.

## Le travail en profondeur est une expérience rare

Open space, messagerie instantanée, ces tendances mettent en lumière un paradoxe. Le travail en profondeur n’a jamais été aussi primordial au sein de notre économie. Pourtant les entreprises limitent les opportunités en rendant plus importants d’autres principes comme le hasard heureux, la communication rapide. Elles diminuent  activement la capacité des personnes à atteindre une grande profondeur de concentration.

Il n’est pas si simple de savoir quelle est exactement la contribution d’un salarié individuel à la production d’ensemble. Nous ne devons donc pas nous attendre à pouvoir facilement déceler l’influence de comportements néfastes au travail en profondeur. C’est le trou noir en matière de critères d’évaluation, ce qui met à mal le travail en profondeur.

La solution de facilité : en entreprise, sans retour clair de l’impact de différents comportements sur le résultat financier, nous avons tendance à opter pour les comportements les plus faciles à afficher sur l’instant. La culture de la connectivité persiste car elle est facile. La première raison, elle permet de répondre à un besoin rapidement. Elle facilite la vie et permet d’éviter de planifier en amont son temps et de devoir mieux s’organiser, lorsque l’on a besoin d’aide par exemple. La seconde est d’adapter la journée en fonction des messages reçus au cours de la journée. Il est bien plus facile de suivre le mouvement en prenant le dernier fil de discussion. En l'absence de critères d’évaluation, la plupart des employés cèdent à la facilité.

S’affairer pour prouver sa productivité : en l’absence d’indicateurs clairs de la productivité et de la valeurs à leurs postes, de nombreux travailleurs du savoir se tournent vers un indicateur industriel de la productivité - faire beaucoup de choses de façon très voyante. Pour un professeur d’université, il est facile de l’évaluer grâce à l’indice de Hirsch, de même pour les artisans. Pour les travailleurs du savoir, ils souhaitent prouver que ce sont des membres productifs de l'équipe et qu’ils ont leur place, mais ne savent pas tout à fait quel est cet objectif. Ils tendent alors à montrer qu’ils en font beaucoup parce qu’ils n’ont pas de meilleures solutions pour prouver leur valeur. Les entreprises n’améliorent pas ce comportement, même pire l’incite. “Si vous n'êtes visiblement pas occupé, j’estime que vous n'êtes pas productif”.

Le culte de l’Internet : Internet est synonyme d’avenir révolutionnaire des entreprises et gouvernements. C’est la caractéristique de la technopolie d’aujourd’hui. Si tu ne l’utilises pas, tu es dépassé. Le travail en profondeur est particulièrement défavorisé au sein d’une technopolie car il est écarté au profit de comportements modernes source de plus de distraction.

Mauvais pour l’entreprise… Bon pour vous : Le travail en profondeur devrait être une priorité mais ce n’est pas le cas. Le fait que le travail en profondeur est difficile et le travail superficiel est lupus facile, qu’en l’absence d’objectifs clairement définis pour votre poste, l’affairement visible, caractéristique du travail superficiel, permet d’obéir à son instinct de conservation et que notre culture a fait naître la croyance selon laquelle si un comportement est liée à Internet, c’est qu’il est bon - quel que soit son impact sur notre faculté de produire des choses précieuses.

## Le travail en profondeur est une expérience riche

Le lien entre le travail profond et une vie épanouie est courant et très largement accepté dan l’univers des artisans. On sait que la satisfaction qu’un individu éprouve à manifester concrètement sa propre réalité dans le monde par le biais du travail manuel tend à produire chez cet individu une certaine tranquillité et une certaine sérénité. Mais quand nous nous tournons vers le travail du savoir, ce lien se brouille. Une partie du problème est la clarté. Les artisans s’attaquent à des défis professionnels faciles à définir, mais difficiles à relever - équilibre nécessaire lorsque l’on cherche un sens à donner à sa vie. Mais ce n’est pas parce que ce lien entre la profondeur de concentration et le sens est moins perceptible en matière de travail du savoir qu’il n’existe pas. 

L’argument neurologique de la profondeur de concentration : nous avons tendance à beaucoup mettre l’accent sur notre situation, en partant du principe que c’est ce qui nous arrive qui conditionne notre ressenti. Cependant, ce n’est pas le cas. Notre cerveau bâtit notre vision du monde à partir de ce sur quoi nous portons notre attention. Si vous vous concentrez sur le diagnostic d’un cancer, vous et votre vie devenez tristes et sombres, mais si vous vous concentrez sur votre Martini du soir, vous et votre existence devenez plus agréables - bien que la situation demeure la même dans les deux cas. “La personne que vous êtes, ce que vous pensez, ressentez et faites, ce que vous aimez, est la somme de ce sur quoi vous vous concentrez.” Ces simples choix peuvent permettre de réinitialiser vos émotions. De nombreux travailleurs du savoir passent la majeure partie de leur journée de travail à interagir en fonction de ces types de soucis superficiels. Même lorsqu’ils doivent accomplir quelque chose en faisant preuve d’une concentration plus intense, l’habitude consistant à vérifier souvent leur boîte mail fait que ces soucis accaparent leur attention. En laissant votre attention dériver vers l’univers attirant qu’est la superficialité, vous courez le risque de tomber dans un autre piège neurologique. Une journée de travail empreinte de superficialité est susceptible d’être épuisante et contrariante, même si la plupart de ces éléments superficiels accaparant votre attention semblent inoffensifs. En conclusion , accroître le temps passé dans un état de profondeur de concentration donne beaucoup de sens et apporte une grande satisfaction professionnelle.

L’argument psychologique de la profondeur de concentration : La plupart des gens partaient du principe que c’est l’état de détente qui rend heureux. Nous souhaitons moins travailler et passer plus de temps allongé dans un hamac. Mais avec la méthode d'échantillonnage des expériences révèlent que la plupart des gens ont tort.  Il est aujourd’hui plus facile d’apprécier le travail que le temps libre. À l’instar des activités source d’une expérience optimale, le travail est doté d’objectifs, de règles et de défis qui incitent la personne à s’investir, à se concentrer et à se laisser absorber. De l’autre côté, le temps libre n’est pas structuré et demande bien plus d’efforts pour que l’on en fasse quelque chose d’appréciable. Il semble que les êtres humains évoluent à leur meilleur niveau lorsqu’ils sont immergés profondément dans une activité stimulante et rendent leur expérience optimale.

L’argument philosophique de la profondeur de concentration : N’importe quelle activité - qu’elle soit physique ou cognitive - demandant beaucoup de compétence peut également générer un sentiment sacré. On doit le sens dévoilé par ces efforts au talent inhérent de notre métier et non aux résultats du travail fourni. Autrement dit, vous n’avez pas besoin d’un poste à part pour donner sens mais d’une approche singulière de votre travail.
