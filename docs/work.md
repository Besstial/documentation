# Développements : Processus et Bonnes Pratiques

## Réception et Suivi des Alertes

Lorsqu'une alerte de dysfonctionnement est signalée (via Slack ou par une autre équipe), plusieurs étapes doivent être suivies pour résoudre efficacement le problème :

1. **Création d'un Ticket :**
    - Utiliser un outil de gestion comme Jira, Trello ou GitHub Issues pour tracer le problème.
    - Inclure des détails précis : description du problème, contexte, priorité, et impact métier.

2. **Communication :**
    - Identifier clairement le demandeur et son rôle (utilisateur, client, manager).
    - Poser des questions spécifiques pour clarifier le contexte et les attentes.

3. **Analyse et Planification :**
    - Catégoriser le problème (critique, bloquant, mineur).
    - Évaluer si une solution temporaire (workaround) est nécessaire avant la résolution complète.

4. **Exécution :**
    - Documenter chaque étape pour assurer la reproductibilité.
    - Solliciter un pair ou une équipe en cas de blocages majeurs.

5. **Suivi et Retour d’Informations :**
    - Informer le demandeur des avancées régulières.
    - Inclure des estimations de délais réalistes.

6. **Adaptation du Discours :**
    - Utiliser un langage accessible pour les parties prenantes non techniques.
    - Documenter les leçons apprises dans un rapport post-mortem si nécessaire.


## Développement et Gestion Git

### Création et Gestion des Branches

- **Feature Branch :**
    - Créer une branche de fonctionnalité à partir de la branche `dev` ou `main`, selon l'organisation de l'arbre Git (Trunk-Based ou Gitflow).

- **Trunk-Based Development :**
    - Une seule branche principale (`main`).
    - Convient aux équipes qui déploient fréquemment.
    - Nécessite des tests automatisés robustes pour éviter les régressions.

- **Gitflow :**
    - Branche supplémentaire `develop` pour intégrer plusieurs fonctionnalités avant leur déploiement dans `main`.
    - Recommandé pour des projets avec des cycles de développement structurés.
    - Inclut des branches spécifiques pour les releases (`release`), les correctifs (`hotfix`) et les tests par les métiers (`recettes`).

### Processus de MEP (Mise en Production)

1. **Pull/Merge Request :**
    - Inclure une description claire du problème et de la solution proposée.
2. **Tests Automatisés :**
    - S'assurer que les pipelines CI/CD vérifient les tests unitaires et le scoverage.
3. **Revue de Code :**
    - Suivre une checklist de bonnes pratiques (noms de variables clairs, absence de code mort, etc.).
4. **Recettes :**
    - Test de la solution par le métier pour vérifier si cela correspond au besoin.
5. **Déclenchement de Pipelines :**
    - Déploiement automatisé pour éviter les erreurs de manipulation en production.

## Méthodologies de Gestion de Projet

### SCRUM

- Structure :

    - Product Owner, Scrum Master, Développeurs.
    - Sprints de deux semaines avec des réunions :
        - Daily Stand-Up
        - Sprint Planning
        - Sprint Review et Rétrospective
    - Organisation :
        - Tâches dans un backlog sous forme de user stories.
        - Inclure des critères d'acceptation (Definition of Done) dans chaque user story.
        - Documenter les points bloquants identifiés durant les Dailies.

### KANBAN

- Différences avec SCRUM :
    - Flux de travail continu.
    - Réduire le WIP (Work in Progress).
- Avantages :
    - Tableau à trois colonnes : À faire, En cours, Terminé.
    - Simplicité de gestion.
- Inconvénients :
    - Nécessite une rigueur accrue pour bien préparer les tâches.

## Organisation et Modélisation (SQL) des Données

### Approches de Gestion des Données

- **Data Mesh :**
    - Organisation par domaine (d'activité).
    - Chaque équipe est responsable de ses données et doit fournir une documentation pour leur utilisation.
    - Application d'une gouvernance fédérée pour garantir l'interopérabilité et la qualité :
        - Standardiser les schémas de données entre domaines pour éviter les silos.
        - Automatiser les tests de qualité des données (tests d'intégrité et d'exactitude).
    - La donnée est un produit.

- **Approche Centralisée :**
    - Données regroupées dans un data lake et un data warehouse central.
    - Création de data marts (ensemble de données) pour subdiviser les données par domaine.
    - Gestion assurée par une équipe centrale.

### Niveaux de Modélisation (MERISE)

1. **MCD (Modèle Conceptuel des Données) :**
    - Décrit les données et leurs relations sans tenir compte de la technologie.
    - Utilise des entités, attributs et associations.
2. **MLD (Modèle Logique des Données) :**
    - Traduit le MCD pour les bases de données.
    - Transforme les entités en tables, les attributs en colonnes, et les associations en clés primaires et étrangères.
3. **MPD (Modèle Physique des Données) :**
    - Adapte le MLD aux contraintes spécifiques des systèmes de gestion de bases de données (SGBD).
    - Définit les types de données, index et partitionnements.

### Types de Modélisation

- **Modèle en étoile :** Table des faits entourée de tables de dimensions.
- **Modèle en flocon (Snowflake) :** Normalisation des tables de dimension pour éviter la redondance.
- **Modèle en constellation :**  Combinaison de plusieurs modèles en étoile.
- **Modèle Data Vault :** Séparation des données de hub, liens et satellites pour flexibilité et évolutivité.

### Formes Normales

- **1FN :** Pas de données répétées ou non atomiques.
- **2FN :** Pas de dépendances partielles.
- **3FN :** Pas de dépendances transitives.
- **4FN :** Pas de dépendances multivaluées.
- **FNBC :** Chaque déterminant est une clé candidate.

## ELT vs ETL

De plus en plus, l'approche ELT (Extract, Load, Transform) est de plus en plus privilégiée à ETL (Extract, Transform, Load). Grâce à la puissance croissante des data warehouses, les transformations peuvent être effectuées directement après l'importation, simplifiant ainsi les flux de données.

## Description des rôles autour de la data

- **Data Architecte :** Conçoit, organise et gère des systèmes de données d'une organisation. Il doit garantir que les données sont accessibles, sécurisées, bien organisées et utilisées efficacement pour répondre aux besoins métier.

- **Data Engineer :** Implémente les systèmes de données définis par le Data Architecte (par ex., construction de pipelines de données, maintenance des bases de données).

- **Data Scientist :** Analyse les données pour extraire des insights, en utilisant les infrastructures conçues par les Data Engineers et Architectes.

- **Data Analyst :** Utilise les données pour produire des rapports et des visualisations pour les prises de décision.

- **Analytics Engineer :** conçoit, développe et maintient des pipelines de données fiables et optimisés pour fournir des analyses exploitables, en comblant le fossé entre les équipes de data engineering et de data analytics.

## Exemple d'ETL avec GCP

### Collecte des Données

Pour ingérer les données, plusieurs outils peuvent être utilisés en fonction des sources et du type de données :

- **Google Cloud SDK (gsutil) :** Commande en ligne simple pour copier et synchroniser des fichiers avec GCS.
- **Package Python `google-cloud-storage` :** Intégration flexible dans des scripts ou applications Python.
- **Apache Kafka avec Connecteurs GCP :** Permet de collecter des données en streaming pour des scénarios en temps réel.
- **Cloud Storage Transfer Service :** Service géré pour transférer des données depuis d'autres sources (HTTP, AWS S3, etc.) vers GCS.

### Traitement des Données

Une fois les données collectées, elles peuvent être transformées en utilisant différents services :

- **Cloud Dataflow :** Plateforme de traitement en streaming ou batch à l'aide de PCollection pour effectuer des transformations avancées.
- **Cloud Dataproc :** Exécute des charges Spark ou Hadoop pour des cas d'usage nécessitant des clusters temporaires.
- **Cloud Functions ou Cloud Run :** Idéal pour des traitements légers ou déclenchés par des événements spécifiques (ex. dépôt de fichiers dans GCS).

### Automatisation

Pour garantir une gestion fluide et récurrente des flux de données, les solutions suivantes peuvent être utilisées :

- **Cloud Functions avec Déclencheurs :** Automatisation du processus de collecte et de traitement, déclenché par des événements comme l'arrivée d'un fichier.
- **Apache Airflow/Cloud Composer :** Orchestration avancée des pipelines ETL avec des DAGs (Directed Acyclic Graphs), permettant de planifier des tâches récurrentes et de gérer des dépendances complexes.
- **Cloud Scheduler :** Planification simple pour exécuter des tâches à des intervalles réguliers (ex. collecte quotidienne).

### Chargement dans BigQuery

BigQuery permet de stocker et interroger les données efficacement :

#### Création des Tables

- **Tables Internes :** Contiennent des données importées directement dans BigQuery via des fichiers ou des flux de données.
- **Tables Externes :** Référencent des données stockées sur GCS ou d'autres sources sans les importer.

#### Utilisation des Vues

- **Avantages :**
    - Évite de stocker des tables intermédiaires en créant des représentations logiques via des requêtes SQL.
    - Restreint l'accès en exposant uniquement les données nécessaires pour chaque cas d’usage.
    - Simplifie les requêtes complexes grâce à l'abstraction.
    - Permet une mise en cache avec des vues matérialisées, améliorant ainsi les performances pour des requêtes fréquentes.
- **Contraintes :**
    - Les performances peuvent être impactées, car chaque vue doit exécuter sa requête sous-jacente.
    - Risque d'incohérence ou de retard dans les mises à jour des données.
    - Une multiplication des vues peut ajouter de la complexité inutile.

### Optimisations dans BigQuery

Pour maximiser les performances et réduire les coûts, l'objectif est de scanner le volume de données minimal pour optimiser les coûts et les temps d'exécution. Plusieurs techniques permet de minimiser :

- **Partitionnement :** Divise les données en segments logiques (ex. par date) pour réduire la quantité de données scannées.
- **Clustering :** Organise les données selon des colonnes fréquemment filtrées pour accélérer les requêtes.
- **Dénormalisation :** Réduire les jointures en intégrant davantage de données dans une seule table, particulièrement adapté pour BigQuery conçu pour l’analytique.
- **Recommandations de Requête :**
    - Éviter SELECT * qui peut scanner des colonnes inutiles.
    - imiter le nombre de lignes retournées avec LIMIT.