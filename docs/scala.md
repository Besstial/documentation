# Scala

Compiling and running code whenever you make a change is a time consuming
process that isn't particularly suitable to a learning environment.
Fortunately, Scala IDE allows us to create special files called Scala Worksheets
that are specifically designed for training and experimentation. Every time
you save a Worksheet, Eclipse automatically compiles and runs your code and
displays the output on the right-hand side of your editor. This provides instant
feedback, which is exactly what we need when investigating new concepts!

## Expressions, Types, and Values

Evaluation in Scala proceeds left to right. 
There are two distinct stages that a Scala program goes through: first it is
compiled, and if it compiles successfully it can then be run or evaluated. We
refer to the first stage as compile-ࢼme and the latter as run-ࢼme.

Compilation is a process of checking that a program makes sense. There are
two ways in which a program must “make sense”:
- It must be syntactically correct,
-  It must type check

If a program passes the checks at compile-time it may then be run. This is the
process of the computer performing the instructions in the program.
Even though a program successfully compiles it can still fail at run-time.

Expressions are part of a program's text—what we type into a file, or the console or worksheet. Expressions exist at compile-time. The defining characteristic of an expression is that it evaluates to a value. A value is information stored in the computer's memory. It exists at run-time. In Scala all values are objects. Types are restrictions on our programs that limit how we can manipulate objects.

At this stage, the most important point about types is that expressions have
types but values do not. We cannot inspect an arbitrary piece of the computer's
memory and divine how to interpret it without knowing the program that created it. For example, in Scala the Int and Float types are both represented by
32-bits of memory. There are no tags or other indications that a given 32-bits
should be interpreted as an Int or a Float.

Types, which exist at compile-time, restrict us to writing programs that give a
consistent interpretation to values. We cannot claim that a particular 32-bits is
at one point an Int and another a Float. When a program type checks, Scala
guarantees that all values are used consistently and thus it does not need to
record type information in a value's representation. This process of removing
type information is called type erasure¹.

Scala code, distinguishes between two kinds of objects. Primitive types don't store any type information along with the value they represent. Object types do store type information. However this type information is not complete and there are occasions where it is lost. 

### Interacting with Objects

An object is a grouping of data and operations on that data. We have some special terminology for the data and operations of an object.
The operations are known as methods. The data is stored in fields.
We interact with objects by calling methods. 

A method call is an expression, and thus evaluates to an object. This means
we can chain method calls together to make more complex programs. Method
parameters are evaluated left-to-right, before the method is called. 

Infix notation is one of several syntactic shorthands that allow us to write simple operator expressions instead of verbose method calls. There are also notations for prefix, postfix, right-associaࢼve, and assignment-style operators, but
there are much less common than infix notation.

Scala uses a set of precedence rules derived from the identifiers
we use as method names that follow our intuitive understanding from mathematics and logic

### Literals

A literal expression represents a fixed value that stands “for itself”. Don't confuse a literal with the value it evaluates to! The literal expression
is the representation in the program text before the program is run, and the
value is the representation in the computer's memory after the program has
run

Numbers share the same types available in Java: 
- Int for 32-bit integers
- Long for 64-bit integers
- Float for 32-bit floating point 
- Double for 64-bit floating point

Booleans are exactly the same as Java: true or false.

Chars are 16-bit Unicode values written as a single character enclosed in single quotes. Strings are exactly Java's strings, and are written as a double quotes.

```
Scala vs Java's Type Hierarchy
Although they are written with initial capitals, Scala's Int, Double,
Float, Long, Short, Byte, Boolen and Char refer to exactly the same
things as int, double, float, long, short, byte, boolean, and char
in Java.
In Scala all of these types act like objects with methods and fields. However, once your code is compiled, a Scala Int is exactly the same as
a Java int. This makes interoperability between the two languages a
breeze.
```

Unit, written (), is the Scala equivalent of Java's void. Unit is the result of
expressions that evaluate to no interesting value

Unit is an important concept in Scala. Many of Scala's syntactic constructs are
expressions that have types and values. We need a placeholder for expressions
that don't yield a useful value, and unit provides just that.

When we write an object literal we use a declaration, which is a different kind of
program to an expression. A declaration does not evaluate to a value. Instead
it associates a name with a value. This name can then be used to refer to the
value in other code.
We can declare an empty object as follows:
```scala
object Test {}
```

This is not an expression—it does not evaluate to a value. Rather, it binds a
name (Test) to a value (an empty object). This is not like any type we've
seen before—it's a new type, created just for our object, called a singleton type


An object can also contain other objects, called fields. We introduce these
using the keywords val or var.
Using val defines an immutable field, meaning we cannot change the value
bound to the name. A var field is mutable, allowing us to change the bound
value.
Always prefer val to var. Scala programmers prefer to use immutable fields
wherever possible, as this maintains substitution.

```
Return is Implicit
The return value of the method is determined by evaluating the body—
there is no need to write return like you would in Java.
```

#### Methods vs fields

You might wonder why we need fields when we can have methods of no arguments that seem to work the same. The difference is subtle—a field gives
a name to a value, whereas a method gives a name to a computation that
produces a value.

Objects and classes (which we'll see later) aren't loaded until they are referenced by other code. This is due to a quirk of Scala and Java called lazy
loading.

The body expression of a field is run only once after which the final value is stored
in the object. The expression is never evaluated again. The body of a method, on the other hand, is evaluated every time we call the
method. Put in different words, fields refer to
values stored within an object, whereas methods refer to computations that
produce values.


### Compound Expressions
#### Conditionals
A conditional allows us to choose an expression to evaluate based on some
condition. Scala's if statement has the same syntax as Java's. One important difference is that Scala's conditional is an expression—it has a type and returns a value.
#### Blocks
A block is a sequence of expressions or declarations surrounded by braces. A
block is also an expression: it executes each of its sub-expressions in order
and returns the value of the last expression. They
are written as a pair of braces containing sub-expressions separated by semicolons or newlines.

## Objects and Classes

Classes are a template for constructing objects. Given a
class we can make many objects that have the same type, similar methods and fields and share common
properties. Like an object declaration, a class declaration binds a name (in this case
Person) and is not an expression. However, unlike an object name, we
cannot use a class name in an expression. A class is not a value, and there is a
different namespace in which classes live.


As it stands our Person class is rather useless: we can create as many new objects as we want but they all have the same firstName and lastName. What
if we want to give each person a different name?
The solution is to introduce a constructor, which allows us to pass parameters
to new objects as we create them.

Constructor arguments and fields are often redundant. Fortunately, Scala provides us a useful short-hand way of declaring both in one go. We can prefix
constructor parameters with the val keyword to have Scala define fields for
them automatically

All Scala methods and constructors support keyword parameters and default
parameter values.
When we call a method or constructor, we can use parameter names as keywords to specify the parameters in an arbitrary order.

### Scala's Type Hierarchy

Unlike Java, which separates primitive and object types, everything in Scala is
an object. As a result, “primitive” value types like Int and Boolean form part
of the same type hierarchy as classes and traits.

Scala has a grand supertype called Any, under which there are two types,
AnyVal and AnyRef. AnyVal is the supertype of all value types, which AnyRef
is the supertype of all “reference types” or classes. All Scala and Java classes
are subtypes of AnyRef¹.
Some of these types are simply Scala aliases for types that exist in Java: Int
is int, Boolean is boolean, and AnyRef is java.lang.Object.
There are two special types at the bottom of the hierarchy. Nothing is the
type of throw expressions, and Null is the type of the value null. These
special types are subtypes of everything else, which helps us assign types to
throw and null while keeping other types in our code sane. 

### Objects as Functions

In Scala, by convention, an object can be “called” like a function if it has a
method called apply. Naming a method apply affords us a special shortened
call syntax: foo.apply(args) becomes foo(args) with foo as an object.

### Companion Objects

Sometimes we want to create a method that logically belongs to a class but is independent of any particular object. In Java we would use a static method for this, but Scala has a simpler solution that we've seen already: singleton objects.
One common use case is auxiliary constructors. Although Scala does have syntax that lets us define multiple constructors for a class, Scala programmers almost always prefer to implement additional constructors as apply methods on an object with the same name as the class. We refer to the object as the companion object of the class.

```scala
class Timestamp(val seconds: Long)

object Timestamp {
    def apply(hours: Int, minutes: Int, seconds: Int): Timestamp =
        new Timestamp(hours*60*60 + minutes*60 + seconds)
}
```

```scala
Timestamp(1, 1, 1).seconds
// res1: Long = 3661
```

A companion object has the same name as its associated class. This doesn't cause
a naming conflict because Scala has two namespaces: the namespace of values
and the namespace of types.

Objects do not have parameters because the objects are immediately constructed once the onject difinition is loaded. The best practice is to store static fields and methods. This kind of code compiles to the same bytecode as a java class with a public construction and a private static variable.

Another small difference to be aware of, which might be more consequential: the type of a class is different from the type of its companion object. This is important, because if we have a method which receives a Kid argument, we can't pass the Kid companion object in there as a value.

```scala
def playAGameWith(kid: Kid) = { /*code*/ }

val bobbie = new Kid("Bobbie", 9)
playAGameWith(bobbie) // OK
playAGameWith(Kid /* <-- the companion*/) // will not compile
```

It is important to note thatthe companion object is not an instance of the class—it
is a singleton object with its own type.To be truly technical, the type of the Kid object is known to the compiler as Kid.type, which is different than Kid (the class name).

### Case Classes

Case classes are an exceptionally useful shorthand for defining a class, a companion object, and a lot of sensible defaults in one go. They are ideal for creating lightweight data-holding classes with the minimum of hassle.
Whenever we declare a case class, Scala automatically generates a class and
companion object. Features of a case class :
- a filed for each constructor argument
- a default toString method
- sensible equals and hashCode methods
- a copy method
- Case classes implement two traits: java.io.Serializable and scala.Product.

Finally, the companion object also contains code to implement an extractor
pattern for use in pattern matching.

#### Case objects
If you find yourself defining a case class with no constructor
arguments you can instead a define a case object.

### Pattern Matching

With case classes we can interact in another way, via pattern matching.
Pattern matching is like an extended if expression that allows us to evaluate
an expression depending on the “shape” of the data.

Pattern Matching Syntax
The syntax of a pattern matching expression is
```scala
expr0 match {
    case pattern1 => expr1
    case pattern2 => expr2
    ...
    case _ => exprn
}
```

## Modelling Data with traits

Traits are templates for creating classes, in the same way that classes are templates for creating objects. Traits allow us to express that two or more classes
can be considered the same, and thus both implement the same operations. In other words, traits allow us to express that multiple classes share a common
super-type (outside of the Any super-type that all classes share). Traits are very much like Java 8's interfaces with default methods.

Like a class, a trait is a named set of field and method definitions. However, it
differs from a class in a few important ways:
- A trait cannot have a constructor—we can't create objects directly from a trait.
- Traits can define abstract methods that have names and type signatures but no implementation.

### Sealed Traits

When we mark a trait as sealed we must define all of its subtypes in the same
file. Once the trait is sealed, the compiler knows the complete set of subtypes
and will warn us if a pattern matching expression is missing a case. We will not get a similar warning from an unsealed trait.

We can still extend the subtypes of a sealed trait outside of the file where
they are defined. If we want to prevent this possibility we should declare them as
sealed (if we want to allow extensions within the file) or final if we want
to disallow all extensions.

```scala
sealed trait Visitor { /* ... */ }
final case class User(/* ... */) extends Visitor
final case class Anonymous(/* ... */) extends Visitor
```
The main advantages of this pattern are:
- the compiler will warn if we miss a case in pattern matching; and
- we can control extension points of sealed traits and thus make stronger
guarantees about the behaviour of subtypes.

### Modelling Data with Traits

An algebraic data type is any data that uses the next below two patterns. In the
functional programming literature, data using the “has-a and” pattern is known
as a product type, and the “is-a or” pattern is a sum type.

- Product Type Pattern: “A has a B and C”
```scala
case class A(b: B, c: C)
```

- The Sum Type Pattern: “A is a B or C”
```scala
sealed trait A
final case class B() extends A
final case class C() extends A
```

- A is a B and C
```scala
trait B
trait C
trait A extends B with C
```

- A has a B or C
```scala
trait A {
def d: D
}
sealed trait D
final case class B() extends D
final case class C() extends D
```

or

```scala
sealed trait A
final case class D(b: B) extends A
final case class E(c: C) extends A
```

### Working with Data
Structural recursion is the precise opposite of the process of building an algebraic data type. 

Construire un ADT (types de données algébriques) consiste à assembler des valeurs.
La récursion structurelle, à l'inverse, consiste à décomposer une structure en ses éléments constitutifs pour la traiter progressivement.

Il existe deux manières d'appliquer la récursion structurelle en Scala :

Avec le polymorphisme → Approche orientée objet (chaque type définit son propre comportement).
Avec le pattern matching → Approche fonctionnelle (on "déconstruit" les valeurs avec match).

#### Structural Recursion using Polymorphism

We can define an implementation in a trait, and change the implementation in
an extending class using the override keyword.
```scala
sealed trait A {
    def foo: String = "It's A!"
}

final case class B() extends A {
    override def foo: String = "It's B!"
}

final case class C() extends A {
    override def foo: String = "It's C!"
}
```

####  Structural Recursion using Pattern Matching

```scala
sealed trait Feline {
def dinner: Food =
this match {
case Lion() => Antelope
case Tiger() => TigerFood
case Panther() => Licorice
case Cat(favouriteFood) => CatFood(favouriteFood)
}
}
```

####  Object-Oriented vs Functional Extensibility

Functional style helps the compiler by ensuring all cases are handled in pattern matching, thanks to sealed traits, which define all possible subtypes. This makes it easier to catch missing cases, especially when modifying subtypes later.

While object-oriented (OO) style offers similar benefits by enforcing method implementation in subtypes, large classes become hard to maintain, often leading to code restructuring that mimics functional style.

The key difference between both styles is extensibility:

OO style makes it easy to add new data (new subtypes) but requires modifying existing code to add new methods.
Functional style makes it easy to add new methods but requires modifying existing code to add new data.
Scala allows both polymorphism and pattern matching, but sealed traits are often preferred for better code safety. Additionally, typeclasses can provide OO-style extensibility

### Recursive Data

This is data that is defined in terms of itself, and allows us to create data of potentially unbounded size.
But We can’t define recursive data like as we could never actually create an instance of such a type—the recursion
never ends. To define valid recursive data we must define a base case, which
is the case that ends the recursion.

```scala
final case class Broken(broken: Broken)
```

#### Exemple

```scala
sealed trait IntList
case object End extends IntList
final case class Pair(head: Int, tail: IntList) extends IntList

def sum(list: IntList): Int =
    list match {
        case End => 0
        case Pair(hd, tl) => hd + sum(tl)
}
```

### Tail recursion

You may be concerned that recursive calls will consume excessive stack space.
Scala can apply an optimisation, called tail recursion, to many recursive functions to stop them consuming stack space.

A tail call is a method call where the caller immediately returns the value. So
this is a tail call

```scala
def method1: Int =
    1
def tailCall: Int =
    method1
```

because tailCall immediately returns the result of calling method1 while

```scala
def notATailCall: Int =
    method1 + 2
```

because notATailCall does not immediatley return—it adds an number to
the result of the call.x

A tail call can be optimised to not use stack space. Due to limitations in the
JVM, Scala only optimises tail calls where the caller calls itself. Since tail recursion is an important property to maintain, we can use the @tailrec annotation to ask the compiler to check that methods we believe are tail recursion
really are. Any non-tail recursion function can be transformed into a tail recursive version by adding an accumulator.

## Sequencing computations

