# Kafka

## Concepts et principes

### ESB (Enterprise Service Bus​)

Un ESB doit répondre combine 3 fonctionnalités principales :​
- Publier et souscrire à des flux de messages​
- Stocker des flux de messages durablement et de manière fiable pour le temps que vous voulez​
- Traiter les messages quand ils arrivent ou rétrospectivement.​

Kafka combine ces 3 fonctionnalités​

Généralement on parle plus souvent d’évènements (events) que de messages ​

- Exemples : logs, métriques, connexions, transactions financières, capteurs d’IoT, …​

Kafka est particulièrement adapté pour les cas d'utilisation de traitement en quasi-temps réel et l'analyse de grandes quantités de données en continu.​

Quelques cas d’usages :​
- Analyse de transactions bancaires. ​
- Suivi de tentatives de connexions​
- Analyse de données moteur de flottes de véhicules​
- Analyse de données des livraisons​.

### Aspect Infrastructure
​
#### Environemment distribué

- Système composé de plusieurs nœuds (serveurs) connectés via un réseau -> Cluster​

- Les ressources (calcul, stockage, données, …) sont réparties entre les nœuds pour permettre ​

    - Une meilleure scalabilité​

    - Une tolérance aux pannes ​

    - De la performance​

- Chaque nœud dans un environnement distribué peut fonctionner indépendamment, mais ils communiquent et collaborent pour donner l'impression d'un système unique et cohérent pour les utilisateurs ou les applications.​

- Exemples : Hadoop, Kubernetes, Elasticsearch, Spark, … et bien sûr Kafka​

#### Cluster Kafka​

- Un cluster Kafka est donc composé de plusieurs nœuds -> les brokers​

- Ils constituent l'élément fondamental d'un cluster Kafka.​

#### Les brokers

Un broker est un serveur logique qui gère le stockage et la distribution des évènements dans le cluster. Chaque broker reçoit des messages provenant des producteurs, les stocke et les met à disposition des consommateurs. ​La coordination entre les brokers se fait via KRaft (Kafka Raft) ou anciennement Zookeeper.​ Zookeeper est « deprecated » depuis la version 3.5 de Kafka.​ Kafka utilise KRaft dans les nouvelles versions. Cela permet de surveiller l’état des brokers et d’assigner des rôles. KRaft s'assure aussi que les brokers soient conscients des nouvelles entrées créées ou des nouvelles consommations demandées.​

![kafka_brokers](./attachments/kafka/kafka_brokers.png)

##### Les différents rôles de brokers :​

- Les brokers « data » : ​
    - Gestion des données​
    - ils assurent la gestion des messages : réception, stockage, mise à disposition aux consommateurs.​

- Les controllers : ​

    - Ils sont responsables de la gestion de la coordination et des décisions critiques au niveau du cluster.​

    - Ils utilisent KRaft (ou Zookeeper) pour maintenir les informations de configuration et de coordination entre les brokers.​

    - L’ensemble des controllers forme le quorum​

    - Parmi ce quorum un des controllers est élu comme leader. Ce leader assure la gestion des responsabilités de coordination. Ses rôles principaux incluent :​

        - Gérer les pannes de broker pour assurer la continuité du service​

        - Assurer que les brokers sont synchronisés correctement entre eux ​

        - Maintenir la cohérence des métadonnées du cluster en surveillant les changements dans l'état du cluster, tels que l'ajout ou la suppression de brokers.​

    - Les controllers non-leader sont les followers​

    - Il n’y a qu’un seul leader actif à la fois. Cela garantit qu'il n'y ait pas de conflits ou d'incohérences dans les décisions liées à la gestion du cluster.​

    - Election du leader :​

        - Lorsqu'un cluster Kafka démarre, un controller est élu en tant que leader via KRaft.​

        - Si le leader tombe une nouvelle élection a lieu pour choisir un autre controller comme leader  haute disponibilité des clusters Kafka​

    - En cas d’absence de leader, les brokers « data »  continuent de fonctionner, mais des actions critiques comme la création de nouveaux topics ne peuvent pas avoir lieu tant qu'un nouveau controller n'est pas élu.​

- Répartition des rôles​

    - En général les brokers ne cumulent pas les rôles « data » et « controller » sauf pour les environnements de dev ou les petits volumes​

    - On parle de modèle control plane / data plane​

    - Une bonne pratique est d’avoir 3 controllers pour assurer la haute disponibilité.​

![kafka_infra](./attachments/kafka/kafka_infra.png)

## Les évènements​

Un évènement constitue un enregistrement de « il s’est passé quelque chose » dans votre organisation. ​

Termes le plus souvent rencontrés : Event/Évènement - Record/enregistrement - Message​

Le format général est :​

- Key : null ou id. Plusieurs évènements peuvent avoir la même clé​

- Value : contenu du message​

- Timestamp : généré par l’émetteur ou le broker​

Exemple d’ évènement système :​
```json
Log_event = {​
    "timestamp": 2024-09-26 12:45:30​,
    "log_level": "INFO"​,
    "application_id": "my-app-123",​
    "host": "server01.mycompany.com",​
    "message": "System startup complete.",​
    "process_id": 12345,​
    "thread_name": "main-thread"​
}​
```

Ce qui est envoyé à Kafka : (value=log_event) ou (key=server01_id, value=log_event) ​

Possibilité d’ajouter des métadonnées sous forme de paires clé-valeur associées à chaque message en utilisant les headers.​
- Cela doit rester simple, limités en taille (quelques kilooctets 1Ko par défaut) et en complexité (généralement des chaînes de caractères ou des bytes simples, pas d’objet).​

Kafka est agnostique du format des événements que vous envoyez :​

- Vous pouvez utiliser JSON, XML ou tout autre format ​

- Attention à gérer la cohérence de la sérialisation et la désérialisation des messages entre les producteurs et les consommateurs.​

Une fois envoyé à Kafka un évènement ne peut plus être modifié​

## Les topics​

Le topic est l’unité de base de l’organisation des évènements ​

La gestion des métadonnées liées aux topics est faite par le control plane et plus particulièrement le controller leader.​

Les topics permettent d’organiser les données selon différents critères de catégories et de cas d’usages​

Un topic peut avoir zéro, un, ou plusieurs producteurs qui écrivent des évènements​

Un topic peut avoir zéro, un, ou plusieurs consommateurs qui souscrivent à ses évènements ​

Les évènements dans un topics peuvent être lus aussi souvent que nécessaire. ​

Les événements ne sont pas forcément supprimés après consommation :​
- Le temps de rétention des évènements peut-être configuré pour chaque topic après quoi ils seront compactés ou supprimés.
- La rétention se configure en temps et/ou en quantité​
- 2 stratégies : compaction ou suppression (défaut) ou les 2.​

Chaque topic dans un cluster Kafka a un nom unique qui est la clé pour identifier et référencer un topic​ :

- Les producteurs envoient des messages à un topic spécifique en utilisant son nom.​

- Les consommateurs s'abonnent à des topics par leur nom pour lire les messages.​

Convention de nommage​ :
- Pas de règles strictes sur les noms des topics, il est recommandé d'adopter une convention de nommage cohérente surtout dans complexe. Par exemple :​
    - Utiliser des préfixes pour regrouper les topics liés à un domaine particulier (moteur.type1, moteur.type2).​
    - Utiliser des tirets ou des points pour séparer les différentes parties du nom (user-activity ou user.activity).​

Limitations de nommage ​:
- Les noms de topics ne peuvent pas contenir plus de 249 caractères.​
- Les caractères valides incluent les lettres majuscules et minuscules (a-z, A-Z), les chiffres (0-9), les points (.), les tirets (- et _).​
- Les espaces ou caractères spéciaux autres que ceux mentionnés ne sont pas autorisés.​

![kafka_topics](./attachments/kafka/kafka_topics.png)

## Les partitions

Un topic est réparti sur plusieurs fragments (buckets) situé sur différents brokers​. Les partitions sont créées avec le topic. Il est possible d’augmenter à posteriori le nombre de partitions mais pas de le diminuer​. Cette distribution permet aux applications clientes d’écrire et de lire à partir de plusieurs brokers en même temps. ​Donc quand un évènement est publié dans un topic il est ajouté dans une des partitions de ce topic. ​Les évènements qui ont la même clé sont écrits dans la même partition sinon round-robin ou une distribution personnalisée​. Dans tous les cas Kafka garantit que les consommateurs d’un topic liront toujours les évènements dans l’ordre ou ils sont arrivés​.

Point d’attention : ​
- Tous les évènements avec la même clé iront toujours vers la même partition​
- Attention au déséquilibrage entre les partitions ​
- Possibilité de systématiquement ignorer la clé​

![kafka_partitions](./attachments/kafka/kafka_partitions.png)

## La réplication

Chaque topic peut être répliqué.​ Le nombre de réplicas est un paramètre au niveau topic.​ 1 = pas de réplication. La valeur par défaut est 3. ​On ne peut pas dynamiquement modifier le facteur de réplication. Il faut utiliser l’outils kafka-reassign-partitions ou l’ API Kafka Admin.​ Le nombre de réplicas ne peut pas dépasser le nombre de brokers dans le cluster.​ Chaque partition a un leader qui gère les lectures et écritures. Les autres réplicas sont en mode follower, synchronisés avec le leader.​ Kafka maintient une liste de « in-sync replica ». Ce sont les réplicas effectivement à jour, ceux ayant répliqué toutes les données jusqu’à la plus récente.​

### Partition leader​

Le broker qui est leader d'une partition est responsable de gérer toutes les lectures et écritures de cette partition. ​ Les producteurs et les consommateurs interagissent directement avec le broker leader.​ Ce leader est responsable d'écrire les évènements dans le journal de la partition et de les répliquer aux brokers followers.​

### Les partitions followers​

- Rôle principal  : la réplication des données. ​
- Rôles indirects : La tolérance aux pannes, La distribution de la charge (depuis la version 2.4), La cohérence des données. ​
- Depuis la version Kafka 2.4, il existe le follower follower fetching . Il est possible de configurer les consommateurs pour qu'ils puissent lire les données directement depuis un follower. Cela permet de répartir la charge de lecture sur plusieurs brokers dans le cas de certains scénarios de lecture intensive.​

![kafka_replication](./attachments/kafka/kafka_replication.png)

## La tolérance aux pannes et hautes disponibilié

Le rôle le plus important d’une partition follower, après la réplication, est de garantir la continuité du service en cas de panne. ​Si le broker leader d'une partition échoue : ​
- Un des followers est promu leader afin de prendre en charge la gestion des lectures et écritures sur cette partition. Cela permet d'assurer la haute disponibilité des données et du cluster.​
- Les consommateurs et producteurs peuvent continuer à interagir avec la partition sans interruption majeure​
- Il y a un paramètre qui permet de fixer le nombre minimum de réplicas synchronisés nécessaires à la validation de l’écriture dans Kafka (1 par défaut)​

Lorsqu'un broker revient en ligne après une panne, ses partitions sont de nouveau synchronisées. Kafka ne réplique donc pas immédiatement les données à un autre broker en cas de panne, mais attend que le broker revienne pour qu'il puisse se resynchroniser.​

### Processus d'élection d’une nouvelle partition leader :​

Détection de la panne :​ Les followers surveillent le leader par le biais de heartbeats. Si le leader ne répond plus pendant un certain temps les followers détectent cette panne.​

Notification au controller :​ Les followers informent le controller de la panne du Leader. ​

Élection d'un nouveau leader :​ Le controller sélectionne un nouveau leader parmi les partitions followers. ​Le controller choisit le follower avec la plus récente réplique des données pour devenir le nouveau leader, garantissant ainsi la continuité des opérations et la consistance des données.​

Mise à jour des métadonnées :​ Une fois qu'un nouveau Leader est élu, le controller met à jour les métadonnées du cluster pour refléter ce changement. ​

## Les producers et les consumers

Producer : une application cliente qui publie des évènements vers Kafka​

Consumer : une application cliente qui lit des évènements depuis Kafka​

Les deux sont complètements découplés l’un de l’autre.​ Ils sont également agnostiques l’un de l’autre (quasiment). Il faut tout de même savoir dans quel topic sont les données​. Il faut un format de donnée commun

## Les APIs et clients

### Les APIs Kafka​

Kafka est livré 5 jeux d’APIs:​
- L’API Producer qui permet aux applications d’envoyer des flux de données au cluster Kafka​
- L’API Consumer qui permet aux applications de lire des flux de données à partir du cluster Kafka​
- L’API Stream permet de transformer des flux de données à partir de topics sources vers des topics cibles.​
- L’API Connect permet relier des systèmes externes à Kafka sans avoir à écrire de code personnalisé pour chaque intégration​
- L’API Admin qui permet de gérer les topics, brokers et tous les objets Kafka ​
- Il n'est pas possible de se connecter directement à Apache Kafka via des appels HTTP :​
    - Kafka utilise son propre protocole binaire qui n'est pas basé sur HTTP et qui repose sur un protocole réseau adapté aux environnements distribués.​
    - Cela nécessite l'utilisation d'une librairie Kafka pour interagir avec ses APIs.​

### Les librairies « clients »​

- Pour utiliser les APIs​
- Seul le client Java est partie intégrante d’Apache Kafka ​
    - Dépendance Maven​
    - Évolue avec les versions de Kafka​
- Pour les autres langages les « clients » sont des projets open-source indépendants. ​
- Confluence propose des clients pour beaucoup de langages populaires. ​
    - Confluence est maintenu par la communauté Apache (ASF Apache Software Foundation)​

## Les stratégies de connexion

- Connexion via un ensemble de brokers (bootstrap servers)​

Le client Kafka est configuré pour se connecter à une liste d'adresses de brokers data appelée bootstrap servers. Lors de la première connexion, Kafka utilise ces serveurs pour obtenir la liste complète des brokers du cluster. Ensuite, le client peut interagir avec n'importe quel broker du cluster, en fonction des partitions et des leaders.​

Avantage : Tolérant aux pannes, car le client peut se reconnecter à d'autres brokers en cas de défaillance du premier contacté.​

Inconvénient (faible) : Nécessite une initialisation correcte de la liste des bootstrap servers.​

Autres méthodes​ :
- Connexion directe à un broker spécifique : simple mais Non tolérant aux pannes.​
- Connexion via un Load Balancer. Permet d’ajouter des mécanismes de tolérance aux pannes ou de sécurité supplémentaire mais introduit un point de latence supplémentaire et une complexité dans l'architecture réseau.​
- Connexion avec équilibrage de charge (Round-Robin ou autre)​

Les clients peuvent être configurés pour utiliser une stratégie d'équilibrage de charge au moment de la connexion, afin de distribuer les connexions entre les différents brokers du cluster. ​

Améliore la répartition des charges dans le cluster mais nécessite une gestion sophistiquée du routage des connexions et des ouitls supplémentaire​

- Connexion à un cluster Kafka avec équilibrage de la charge basé sur le partitionnement​

Kafka assigne les messages à des partitions, et chaque partition a un leader broker. Les clients Kafka se connectent directement au leader de la partition qui les intéresse pour optimiser les performances. Le client Kafka est responsable de déterminer quel broker est le leader d’une partition et de maintenir la connexion à celui-ci.​

Efficace et optimal pour la performance, car les messages vont directement au leader mais le client doit gérer la redirection et la reconnexion en cas de changement du leader.​

Stratégies d’acknowledgement pour les producers​ :
- acks = 0 le producer n’attend pas de réponse du cluster​
- acks = 1 le producer attend une réponse du leader seulement​
- acks = all (ou -1) le producer attend une réponse de tous les in-sync réplicas​

## Les consumer groups
Cette fonctionnalité permet de gérer la consommation d’évènements de manière distribuée​. Un consumer group est un ensemble de consommateurs Kafka qui travaillent ensemble pour consommer des évènements d'un ou plusieurs topics.​ Chaque consumer group est identifié par un nom unique. ​Tous les consommateurs qui partagent le même nom de groupe appartiennent au même consumer group.​ Il est possible d’adresser plusieurs topics pour un seul consumer group avec une stratégie de nommage utilisant les wildcards, topic = moteur.* par exemple​

### Fonctionnement​

Distribution des messages : Les partitions d'un topic Kafka sont distribuées entre les consommateurs d'un même groupe. Chaque partition est consommée par un seul consommateur dans le groupe à un moment donné. Cela permet d'équilibrer la charge de consommation et d'augmenter le débit.​

Équilibrage : Si un consommateur échoue, Kafka réattribue automatiquement les partitions à d'autres consommateurs du même groupe pour assurer la continuité du traitement.​

Lorsque vous utilisez un consumer group pour consommer plusieurs topics dans Kafka, la gestion des offsets se fait de manière distincte pour chaque partition de chaque topic.​

### Avantages​

Scalabilité : Les consumer groups permettent de faire évoluer la consommation de messages en ajoutant ou supprimant des consommateurs sans affecter le reste du système.​

Tolérance aux pannes : En cas de défaillance d'un consommateur, d'autres membres du groupe peuvent prendre le relais.​

Traitement parallèle : Les messages peuvent être traités en parallèle par différents consommateurs, augmentant ainsi le débit global​

## Les offsets

Un offset est un identifiant numérique unique pour chaque évènement dans une partition Kafka, ordonnés et numérotés à partir de zéro​. Il représente la position séquentielle d'un évènement. ​

Unicité : Les offsets sont uniques à l'intérieur d'une partition, mais pas à travers différentes partitions ou topics.​
### Utilisation des offsets​

Suivi de la position : Les offsets permettent de suivre la position des consumers dans une partition. Lorsqu'un consumer lit des évènements, il garde une trace du dernier offset qu'il a lu ce qui lui permet de reprendre la lecture à partir de ce point en cas de redémarrage ou de reprise.​

Gestion des consumer groups : ​
- Dans un consumer group chaque consumer suit les offsets de manière indépendante pour chaque partition. ​
- Kafka stocke les offsets ce qui permet de gérer la consommation de manière fiable et coordonnée entre les membres du groupe.​

### Stockage des offsets​

Stockage Interne : Kafka stocke les offsets dans un topic interne « __consumer_offsets ». Cela garantit que les offsets sont persistants et résilients aux pannes.​ Gestion par les consumers : les consumers peuvent choisir de stocker les offsets de manière externe si cela est nécessaire pour des raisons spécifiques.​

### Consommation des évènements​

Lorsqu'un consumer lit des évènements, il spécifie un offset de départ. Il peut lire à partir d'un offset spécifique ou commencer à lire depuis le début ou la fin de la partition.​

Après avoir traité les évènements, un consumer "commit" l'offset, indiquant à Kafka qu'il a traité les évènements jusqu'à cet offset et qu'il peut avancer. ​

### Réinitialisation des offsets

Rewind : Il est possible de réinitialiser les offsets à un point précédent dans la partition, ce qui permet aux consumers de relire des évènements​

Seek : Les consumers peuvent "chercher" un offset spécifique pour commencer à lire à partir de ce point, ce qui peut être utile pour des cas d'usage tels que la reprise après une panne ou le traitement des données historiques.​

Kafka offre une fonctionnalité pour rechercher des offsets en utilisant le timestamp​. Il n’est pas possible de rechercher sur les headers ou dans le payload.​

### Les consumer groups​

Les offsets sont gérés par consumer group pour suivre jusqu'où chaque consommateur a consommé.​ Lorsque vous utilisez un consumer group pour consommer plusieurs topics dans Kafka, la gestion des offsets se fait de manière distincte pour chaque partition de chaque topic.​

### Autres rôles des offsets​

Réplication : Les brokers qui détiennent les répliques d'une partition utilisent les offsets pour s'assurer qu'ils sont synchronisés avec le leader..​

ISR (In-Sync Replica) : Kafka utilise les offsets pour déterminer si une réplique est dans l'état "In-Sync Replica" (ISR), c'est-à-dire si elle a répliqué toutes les données jusqu'à l'offset le plus récent.​

La gestion du lag​ : C’est la différence entre l'offset du dernier évènement disponible dans une partition et l'offset actuel du consumer. Si un consumer est en retard le lag sera positif.​

## Sécurité

### Sécurité entre les brokers​

La méthode la plus courante et privilégiée est SSL/TLS pour le chiffrement et l'authentification entre les brokers​. Prise en charge des keystore/trustore et authentification mutuelle​

### Sécurité d’accès – Authentification​

La méthode la plus courante est SASL (Simple Authentication and Security Layer) associée à SSL/TLS.​ Les clients Kafka doivent fournir des certificats et/ou des informations d'authentification pour se connecter aux brokers.​

Avantage : Sécurise les données en transit et les connexions. ​

Inconvénient : Complexité dans la configuration et la gestion des certificats ou des mécanismes d'authentification.​ Il faut à la fois configurer les brokers et les clients​

Méthodes d’authentification​ :
- SASL plain text : possible mais peu acceptable​
- SASL_SSL : là c’est mieux ​

Liste des utilisateurs potentiels :​
- Configuration par fichier texte pour du SASL simple (attention : redémarrage des brokers nécessaires en cas de modifications)​
- Utiliser une gestion centralisée des utilisateurs​ :
    - LDAP​
    - OpenID Connect (OIDC)​
    - Kerberos​

## Les ACLs (Access Control List)​

Il s’agit de gérer les autorisations d'accès aux ressources de Kafka, comme les topics, les groupes de consommateurs et le cluster. ​C’est le seul moyen de gérer les accès dans Kafka​. Il n’y a pas de gestion de rôles mais des permissions qui peuvent être considérer comme des rôles. ​

Liste exhaustive des permissions :​
- Read : Permet à un utilisateur de lire des messages d'un topic spécifié.​
- Write : Permet à un utilisateur d'écrire des messages dans un topic spécifié.​
- Create : Permet à un utilisateur de créer un nouveau topic.​
- Delete : Permet à un utilisateur de supprimer un topic existant.​
- Alter : Permet à un utilisateur de modifier la configuration d'un topic ou d'un groupe de consommateurs (par exemple, changer les partitions, la rétention, etc.).​
- Describe : Permet à un utilisateur de décrire un topic ou un groupe de consommateurs, c'est-à-dire d'obtenir des métadonnées sur le topic ou le groupe (comme le nombre de partitions, le leader, etc.).​
- All : Accorde toutes les permissions disponibles pour une ressource spécifique. ​

Niveau d’application des ACLs :​ 
- Le cluster pour autoriser des opérations administratives, telles que la création ou la suppression de topics, la modification des configurations de cluster.​
- Les consumers groups​
- Les topics ​
​
## Geo-replication

2 types d’architectures​

### Stretched cluster ​

Un seul cluster sur plusieurs data centers. ​

La vitesse du réseau est un facteur critique pour la réplication des données entre les régions. Pour une réplication synchrone fiable pour des charges modérées, une latence réseau inférieure à 10 ms et une bande passante d'au moins 1 Gbps sont recommandées. Cependant, en fonction du volume des données (plrs To/jour), une bande passante de 10 Gbps ou plus pourrait être nécessaire pour assurer une réplication fluide et éviter les congestions.​

Follower fetching : les brokers followers dans une région spécifique peuvent être utilisés pour les lectures locales afin de réduire la latence réseau. Les consommateurs dans une région donnée peuvent lire les messages à partir des followers locaux, même si le leader de la partition se trouve dans une autre région.​

### Connected clusters ​

La réplication est asynchrone​. Il faut utiliser un outil séparé pour copier les données d’un cluster à l’autre, le plus courant est « MirrorMaker 2 »​. C’est généralement utilisé avec le cluster source réservé à l’écriture et le cluster miroir à la lecture​

![kafka_geoclusters](./attachments/kafka/kafka_geoclusters.png)

## Console d’administration CMAK​ (Cluster Manager for Apache Kafka​)

Lien GitHub : https://github.com/yahoo/kafka-manager