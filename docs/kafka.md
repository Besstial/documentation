# Kafka

## Concepts et principes

### ESB (Enterprise Service Bus​)

Un ESB doit répondre combine 3 fonctionnalités principales :​
- Publier et souscrire à des flux de messages​
- Stocker des flux de messages durablement et de manière fiable pour le temps que vous voulez​
- Traiter les messages quand ils arrivent ou rétrospectivement.​

Kafka combine ces 3 fonctionnalités​

Généralement on parle plus souvent d’évènements (events) que de messages ​

- Exemples : logs, métriques, connexions, transactions financières, capteurs d’IoT, …​

Kafka est particulièrement adapté pour les cas d'utilisation de traitement en quasi-temps réel et l'analyse de grandes quantités de données en continu.​

Quelques cas d’usages :​
- Analyse de transactions bancaires. ​
- Suivi de tentatives de connexions​
- Analyse de données moteur de flottes de véhicules​
- Analyse de données des livraisons​.

### Aspect Infrastructure
​
#### Environemment distribué

- Système composé de plusieurs nœuds (serveurs) connectés via un réseau -> Cluster​

- Les ressources (calcul, stockage, données, …) sont réparties entre les nœuds pour permettre ​

    - Une meilleure scalabilité​

    - Une tolérance aux pannes ​

    - De la performance​

- Chaque nœud dans un environnement distribué peut fonctionner indépendamment, mais ils communiquent et collaborent pour donner l'impression d'un système unique et cohérent pour les utilisateurs ou les applications.​

- Exemples : Hadoop, Kubernetes, Elasticsearch, Spark, … et bien sûr Kafka​

#### Cluster Kafka​

- Un cluster Kafka est donc composé de plusieurs nœuds -> les brokers​

- Ils constituent l'élément fondamental d'un cluster Kafka.​

#### Les brokers

Un broker est un serveur logique qui gère le stockage et la distribution des évènements dans le cluster. Chaque broker reçoit des messages provenant des producteurs, les stocke et les met à disposition des consommateurs. ​La coordination entre les brokers se fait via KRaft (Kafka Raft) ou anciennement Zookeeper.​ Zookeeper est « deprecated » depuis la version 3.5 de Kafka.​ Kafka utilise KRaft dans les nouvelles versions. Cela permet de surveiller l’état des brokers et d’assigner des rôles. KRaft s'assure aussi que les brokers soient conscients des nouvelles entrées créées ou des nouvelles consommations demandées.​

![kafka_brokers](./attachments/kafka/kafka_brokers.png)

##### Les différents rôles de brokers :​

- Les brokers « data » : ​
    - Gestion des données​
    - ils assurent la gestion des messages : réception, stockage, mise à disposition aux consommateurs.​

- Les controllers : ​

    - Ils sont responsables de la gestion de la coordination et des décisions critiques au niveau du cluster.​

    - Ils utilisent KRaft (ou Zookeeper) pour maintenir les informations de configuration et de coordination entre les brokers.​

    - L’ensemble des controllers forme le quorum​

    - Parmi ce quorum un des controllers est élu comme leader. Ce leader assure la gestion des responsabilités de coordination. Ses rôles principaux incluent :​

        - Gérer les pannes de broker pour assurer la continuité du service​

        - Assurer que les brokers sont synchronisés correctement entre eux ​

        - Maintenir la cohérence des métadonnées du cluster en surveillant les changements dans l'état du cluster, tels que l'ajout ou la suppression de brokers.​

    - Les controllers non-leader sont les followers​

    - Il n’y a qu’un seul leader actif à la fois. Cela garantit qu'il n'y ait pas de conflits ou d'incohérences dans les décisions liées à la gestion du cluster.​

    - Election du leader :​

        - Lorsqu'un cluster Kafka démarre, un controller est élu en tant que leader via KRaft.​

        - Si le leader tombe une nouvelle élection a lieu pour choisir un autre controller comme leader  haute disponibilité des clusters Kafka​

    - En cas d’absence de leader, les brokers « data »  continuent de fonctionner, mais des actions critiques comme la création de nouveaux topics ne peuvent pas avoir lieu tant qu'un nouveau controller n'est pas élu.​

- Répartition des rôles​

    - En général les brokers ne cumulent pas les rôles « data » et « controller » sauf pour les environnements de dev ou les petits volumes​

    - On parle de modèle control plane / data plane​

    - Une bonne pratique est d’avoir 3 controllers pour assurer la haute disponibilité.​

![kafka_infra](./attachments/kafka/kafka_infra.png)

## Les évènements​

Un évènement constitue un enregistrement de « il s’est passé quelque chose » dans votre organisation. ​

Termes le plus souvent rencontrés : Event/Évènement - Record/enregistrement - Message​

Le format général est :​

- Key : null ou id. Plusieurs évènements peuvent avoir la même clé​

- Value : contenu du message​

- Timestamp : généré par l’émetteur ou le broker​

Exemple d’ évènement système :​
```json
Log_event = {​
    "timestamp": 2024-09-26 12:45:30​,
    "log_level": "INFO"​,
    "application_id": "my-app-123",​
    "host": "server01.mycompany.com",​
    "message": "System startup complete.",​
    "process_id": 12345,​
    "thread_name": "main-thread"​
}​
```

Ce qui est envoyé à Kafka : (value=log_event) ou (key=server01_id, value=log_event) ​

Possibilité d’ajouter des métadonnées sous forme de paires clé-valeur associées à chaque message en utilisant les headers.​
- Cela doit rester simple, limités en taille (quelques kilooctets 1Ko par défaut) et en complexité (généralement des chaînes de caractères ou des bytes simples, pas d’objet).​

Kafka est agnostique du format des événements que vous envoyez :​

- Vous pouvez utiliser JSON, XML ou tout autre format ​

- Attention à gérer la cohérence de la sérialisation et la désérialisation des messages entre les producteurs et les consommateurs.​

Une fois envoyé à Kafka un évènement ne peut plus être modifié​

## Les topics​

Le topic est l’unité de base de l’organisation des évènements ​

La gestion des métadonnées liées aux topics est faite par le control plane et plus particulièrement le controller leader.​

Les topics permettent d’organiser les données selon différents critères de catégories et de cas d’usages​

Un topic peut avoir zéro, un, ou plusieurs producteurs qui écrivent des évènements​

Un topic peut avoir zéro, un, ou plusieurs consommateurs qui souscrivent à ses évènements ​

Les évènements dans un topics peuvent être lus aussi souvent que nécessaire. ​

Les événements ne sont pas forcément supprimés après consommation :​
- Le temps de rétention des évènements peut-être configuré pour chaque topic après quoi ils seront compactés ou supprimés.
- La rétention se configure en temps et/ou en quantité​
- 2 stratégies : compaction ou suppression (défaut) ou les 2.​

Chaque topic dans un cluster Kafka a un nom unique qui est la clé pour identifier et référencer un topic​ :

- Les producteurs envoient des messages à un topic spécifique en utilisant son nom.​

- Les consommateurs s'abonnent à des topics par leur nom pour lire les messages.​

Convention de nommage​ :
- Pas de règles strictes sur les noms des topics, il est recommandé d'adopter une convention de nommage cohérente surtout dans complexe. Par exemple :​
    - Utiliser des préfixes pour regrouper les topics liés à un domaine particulier (moteur.type1, moteur.type2).​
    - Utiliser des tirets ou des points pour séparer les différentes parties du nom (user-activity ou user.activity).​

Limitations de nommage ​:
- Les noms de topics ne peuvent pas contenir plus de 249 caractères.​
- Les caractères valides incluent les lettres majuscules et minuscules (a-z, A-Z), les chiffres (0-9), les points (.), les tirets (- et _).​
- Les espaces ou caractères spéciaux autres que ceux mentionnés ne sont pas autorisés.​

![kafka_topics](./attachments/kafka/kafka_topics.png)

## Les partitions

Un topic est réparti sur plusieurs fragments (buckets) situé sur différents brokers​. Les partitions sont créées avec le topic. Il est possible d’augmenter à posteriori le nombre de partitions mais pas de le diminuer​. Cette distribution permet aux applications clientes d’écrire et de lire à partir de plusieurs brokers en même temps. ​Donc quand un évènement est publié dans un topic il est ajouté dans une des partitions de ce topic. ​Les évènements qui ont la même clé sont écrits dans la même partition sinon round-robin ou une distribution personnalisée​. Dans tous les cas Kafka garantit que les consommateurs d’un topic liront toujours les évènements dans l’ordre ou ils sont arrivés​.

Point d’attention : ​
- Tous les évènements avec la même clé iront toujours vers la même partition​
- Attention au déséquilibrage entre les partitions ​
- Possibilité de systématiquement ignorer la clé​

![kafka_partitions](./attachments/kafka/kafka_partitions.png)

## La réplication

Chaque topic peut être répliqué.​ Le nombre de réplicas est un paramètre au niveau topic.​ 1 = pas de réplication. La valeur par défaut est 3. ​On ne peut pas dynamiquement modifier le facteur de réplication. Il faut utiliser l’outils kafka-reassign-partitions ou l’ API Kafka Admin.​ Le nombre de réplicas ne peut pas dépasser le nombre de brokers dans le cluster.​ Chaque partition a un leader qui gère les lectures et écritures. Les autres réplicas sont en mode follower, synchronisés avec le leader.​ Kafka maintient une liste de « in-sync replica ». Ce sont les réplicas effectivement à jour, ceux ayant répliqué toutes les données jusqu’à la plus récente.​

### Partition leader​

Le broker qui est leader d'une partition est responsable de gérer toutes les lectures et écritures de cette partition. ​ Les producteurs et les consommateurs interagissent directement avec le broker leader.​ Ce leader est responsable d'écrire les évènements dans le journal de la partition et de les répliquer aux brokers followers.​

### Les partitions followers​

- Rôle principal  : la réplication des données. ​
- Rôles indirects : La tolérance aux pannes, La distribution de la charge (depuis la version 2.4), La cohérence des données. ​
- Depuis la version Kafka 2.4, il existe le follower follower fetching . Il est possible de configurer les consommateurs pour qu'ils puissent lire les données directement depuis un follower. Cela permet de répartir la charge de lecture sur plusieurs brokers dans le cas de certains scénarios de lecture intensive.​

![kafka_replication](./attachments/kafka/kafka_replication.png)

## La tolérance aux pannes et hautes disponibilié

Le rôle le plus important d’une partition follower, après la réplication, est de garantir la continuité du service en cas de panne. ​Si le broker leader d'une partition échoue : ​
- Un des followers est promu leader afin de prendre en charge la gestion des lectures et écritures sur cette partition. Cela permet d'assurer la haute disponibilité des données et du cluster.​
- Les consommateurs et producteurs peuvent continuer à interagir avec la partition sans interruption majeure​
- Il y a un paramètre qui permet de fixer le nombre minimum de réplicas synchronisés nécessaires à la validation de l’écriture dans Kafka (1 par défaut)​

Lorsqu'un broker revient en ligne après une panne, ses partitions sont de nouveau synchronisées. Kafka ne réplique donc pas immédiatement les données à un autre broker en cas de panne, mais attend que le broker revienne pour qu'il puisse se resynchroniser.​

### Processus d'élection d’une nouvelle partition leader :​

Détection de la panne :​ Les followers surveillent le leader par le biais de heartbeats. Si le leader ne répond plus pendant un certain temps les followers détectent cette panne.​

Notification au controller :​ Les followers informent le controller de la panne du Leader. ​

Élection d'un nouveau leader :​ Le controller sélectionne un nouveau leader parmi les partitions followers. ​Le controller choisit le follower avec la plus récente réplique des données pour devenir le nouveau leader, garantissant ainsi la continuité des opérations et la consistance des données.​

Mise à jour des métadonnées :​ Une fois qu'un nouveau Leader est élu, le controller met à jour les métadonnées du cluster pour refléter ce changement. ​

## Les producers et les consumers

Producer : une application cliente qui publie des évènements vers Kafka​

Consumer : une application cliente qui lit des évènements depuis Kafka​

Les deux sont complètements découplés l’un de l’autre.​ Ils sont également agnostiques l’un de l’autre (quasiment). Il faut tout de même savoir dans quel topic sont les données​. Il faut un format de donnée commun

## Les APIs et clients

### Les APIs Kafka​

Kafka est livré 5 jeux d’APIs:​
- L’API Producer qui permet aux applications d’envoyer des flux de données au cluster Kafka​
- L’API Consumer qui permet aux applications de lire des flux de données à partir du cluster Kafka​
- L’API Stream permet de transformer des flux de données à partir de topics sources vers des topics cibles.​
- L’API Connect permet relier des systèmes externes à Kafka sans avoir à écrire de code personnalisé pour chaque intégration​
- L’API Admin qui permet de gérer les topics, brokers et tous les objets Kafka ​
- Il n'est pas possible de se connecter directement à Apache Kafka via des appels HTTP :​
    - Kafka utilise son propre protocole binaire qui n'est pas basé sur HTTP et qui repose sur un protocole réseau adapté aux environnements distribués.​
    - Cela nécessite l'utilisation d'une librairie Kafka pour interagir avec ses APIs.​

### Les librairies « clients »​

- Pour utiliser les APIs​
- Seul le client Java est partie intégrante d’Apache Kafka ​
    - Dépendance Maven​
    - Évolue avec les versions de Kafka​
- Pour les autres langages les « clients » sont des projets open-source indépendants. ​
- Confluence propose des clients pour beaucoup de langages populaires. ​
    - Confluence est maintenu par la communauté Apache (ASF Apache Software Foundation)​

## Les stratégies de connexion

- Connexion via un ensemble de brokers (bootstrap servers)​

Le client Kafka est configuré pour se connecter à une liste d'adresses de brokers data appelée bootstrap servers. Lors de la première connexion, Kafka utilise ces serveurs pour obtenir la liste complète des brokers du cluster. Ensuite, le client peut interagir avec n'importe quel broker du cluster, en fonction des partitions et des leaders.​

Avantage : Tolérant aux pannes, car le client peut se reconnecter à d'autres brokers en cas de défaillance du premier contacté.​

Inconvénient (faible) : Nécessite une initialisation correcte de la liste des bootstrap servers.​

Autres méthodes​ :
- Connexion directe à un broker spécifique : simple mais Non tolérant aux pannes.​
- Connexion via un Load Balancer. Permet d’ajouter des mécanismes de tolérance aux pannes ou de sécurité supplémentaire mais introduit un point de latence supplémentaire et une complexité dans l'architecture réseau.​
- Connexion avec équilibrage de charge (Round-Robin ou autre)​

Les clients peuvent être configurés pour utiliser une stratégie d'équilibrage de charge au moment de la connexion, afin de distribuer les connexions entre les différents brokers du cluster. ​

Améliore la répartition des charges dans le cluster mais nécessite une gestion sophistiquée du routage des connexions et des ouitls supplémentaire​

- Connexion à un cluster Kafka avec équilibrage de la charge basé sur le partitionnement​

Kafka assigne les messages à des partitions, et chaque partition a un leader broker. Les clients Kafka se connectent directement au leader de la partition qui les intéresse pour optimiser les performances. Le client Kafka est responsable de déterminer quel broker est le leader d’une partition et de maintenir la connexion à celui-ci.​

Efficace et optimal pour la performance, car les messages vont directement au leader mais le client doit gérer la redirection et la reconnexion en cas de changement du leader.​

Stratégies d’acknowledgement pour les producers​ :
- acks = 0 le producer n’attend pas de réponse du cluster​
- acks = 1 le producer attend une réponse du leader seulement​
- acks = all (ou -1) le producer attend une réponse de tous les in-sync réplicas​

## Les consumer groups
Cette fonctionnalité permet de gérer la consommation d’évènements de manière distribuée​. Un consumer group est un ensemble de consommateurs Kafka qui travaillent ensemble pour consommer des évènements d'un ou plusieurs topics.​ Chaque consumer group est identifié par un nom unique. ​Tous les consommateurs qui partagent le même nom de groupe appartiennent au même consumer group.​ Il est possible d’adresser plusieurs topics pour un seul consumer group avec une stratégie de nommage utilisant les wildcards, topic = moteur.* par exemple​

### Fonctionnement​

Distribution des messages : Les partitions d'un topic Kafka sont distribuées entre les consommateurs d'un même groupe. Chaque partition est consommée par un seul consommateur dans le groupe à un moment donné. Cela permet d'équilibrer la charge de consommation et d'augmenter le débit.​

Équilibrage : Si un consommateur échoue, Kafka réattribue automatiquement les partitions à d'autres consommateurs du même groupe pour assurer la continuité du traitement.​

Lorsque vous utilisez un consumer group pour consommer plusieurs topics dans Kafka, la gestion des offsets se fait de manière distincte pour chaque partition de chaque topic.​

### Avantages​

Scalabilité : Les consumer groups permettent de faire évoluer la consommation de messages en ajoutant ou supprimant des consommateurs sans affecter le reste du système.​

Tolérance aux pannes : En cas de défaillance d'un consommateur, d'autres membres du groupe peuvent prendre le relais.​

Traitement parallèle : Les messages peuvent être traités en parallèle par différents consommateurs, augmentant ainsi le débit global​

## Les offsets

Un offset est un identifiant numérique unique pour chaque évènement dans une partition Kafka, ordonnés et numérotés à partir de zéro​. Il représente la position séquentielle d'un évènement. ​

Unicité : Les offsets sont uniques à l'intérieur d'une partition, mais pas à travers différentes partitions ou topics.​
### Utilisation des offsets​

Suivi de la position : Les offsets permettent de suivre la position des consumers dans une partition. Lorsqu'un consumer lit des évènements, il garde une trace du dernier offset qu'il a lu ce qui lui permet de reprendre la lecture à partir de ce point en cas de redémarrage ou de reprise.​

Gestion des consumer groups : ​
- Dans un consumer group chaque consumer suit les offsets de manière indépendante pour chaque partition. ​
- Kafka stocke les offsets ce qui permet de gérer la consommation de manière fiable et coordonnée entre les membres du groupe.​

### Stockage des offsets​

Stockage Interne : Kafka stocke les offsets dans un topic interne « __consumer_offsets ». Cela garantit que les offsets sont persistants et résilients aux pannes.​ Gestion par les consumers : les consumers peuvent choisir de stocker les offsets de manière externe si cela est nécessaire pour des raisons spécifiques.​

### Consommation des évènements​

Lorsqu'un consumer lit des évènements, il spécifie un offset de départ. Il peut lire à partir d'un offset spécifique ou commencer à lire depuis le début ou la fin de la partition.​

Après avoir traité les évènements, un consumer "commit" l'offset, indiquant à Kafka qu'il a traité les évènements jusqu'à cet offset et qu'il peut avancer. ​

### Réinitialisation des offsets

Rewind : Il est possible de réinitialiser les offsets à un point précédent dans la partition, ce qui permet aux consumers de relire des évènements​

Seek : Les consumers peuvent "chercher" un offset spécifique pour commencer à lire à partir de ce point, ce qui peut être utile pour des cas d'usage tels que la reprise après une panne ou le traitement des données historiques.​

Kafka offre une fonctionnalité pour rechercher des offsets en utilisant le timestamp​. Il n’est pas possible de rechercher sur les headers ou dans le payload.​

### Les consumer groups​

Les offsets sont gérés par consumer group pour suivre jusqu'où chaque consommateur a consommé.​ Lorsque vous utilisez un consumer group pour consommer plusieurs topics dans Kafka, la gestion des offsets se fait de manière distincte pour chaque partition de chaque topic.​

### Autres rôles des offsets​

Réplication : Les brokers qui détiennent les répliques d'une partition utilisent les offsets pour s'assurer qu'ils sont synchronisés avec le leader..​

ISR (In-Sync Replica) : Kafka utilise les offsets pour déterminer si une réplique est dans l'état "In-Sync Replica" (ISR), c'est-à-dire si elle a répliqué toutes les données jusqu'à l'offset le plus récent.​

La gestion du lag​ : C’est la différence entre l'offset du dernier évènement disponible dans une partition et l'offset actuel du consumer. Si un consumer est en retard le lag sera positif.​

## Sécurité

### Sécurité entre les brokers​

La méthode la plus courante et privilégiée est SSL/TLS pour le chiffrement et l'authentification entre les brokers​. Prise en charge des keystore/trustore et authentification mutuelle​

### Sécurité d’accès – Authentification​

La méthode la plus courante est SASL (Simple Authentication and Security Layer) associée à SSL/TLS.​ Les clients Kafka doivent fournir des certificats et/ou des informations d'authentification pour se connecter aux brokers.​

Avantage : Sécurise les données en transit et les connexions. ​

Inconvénient : Complexité dans la configuration et la gestion des certificats ou des mécanismes d'authentification.​ Il faut à la fois configurer les brokers et les clients​

Méthodes d’authentification​ :
- SASL plain text : possible mais peu acceptable​
- SASL_SSL : là c’est mieux ​

Liste des utilisateurs potentiels :​
- Configuration par fichier texte pour du SASL simple (attention : redémarrage des brokers nécessaires en cas de modifications)​
- Utiliser une gestion centralisée des utilisateurs​ :
    - LDAP​
    - OpenID Connect (OIDC)​
    - Kerberos​

## Les ACLs (Access Control List)​

Il s’agit de gérer les autorisations d'accès aux ressources de Kafka, comme les topics, les groupes de consommateurs et le cluster. ​C’est le seul moyen de gérer les accès dans Kafka​. Il n’y a pas de gestion de rôles mais des permissions qui peuvent être considérer comme des rôles. ​

Liste exhaustive des permissions :​
- Read : Permet à un utilisateur de lire des messages d'un topic spécifié.​
- Write : Permet à un utilisateur d'écrire des messages dans un topic spécifié.​
- Create : Permet à un utilisateur de créer un nouveau topic.​
- Delete : Permet à un utilisateur de supprimer un topic existant.​
- Alter : Permet à un utilisateur de modifier la configuration d'un topic ou d'un groupe de consommateurs (par exemple, changer les partitions, la rétention, etc.).​
- Describe : Permet à un utilisateur de décrire un topic ou un groupe de consommateurs, c'est-à-dire d'obtenir des métadonnées sur le topic ou le groupe (comme le nombre de partitions, le leader, etc.).​
- All : Accorde toutes les permissions disponibles pour une ressource spécifique. ​

Niveau d’application des ACLs :​ 
- Le cluster pour autoriser des opérations administratives, telles que la création ou la suppression de topics, la modification des configurations de cluster.​
- Les consumers groups​
- Les topics ​
​
## Geo-replication

2 types d’architectures​

### Stretched cluster ​

Un seul cluster sur plusieurs data centers. ​

La vitesse du réseau est un facteur critique pour la réplication des données entre les régions. Pour une réplication synchrone fiable pour des charges modérées, une latence réseau inférieure à 10 ms et une bande passante d'au moins 1 Gbps sont recommandées. Cependant, en fonction du volume des données (plrs To/jour), une bande passante de 10 Gbps ou plus pourrait être nécessaire pour assurer une réplication fluide et éviter les congestions.​

Follower fetching : les brokers followers dans une région spécifique peuvent être utilisés pour les lectures locales afin de réduire la latence réseau. Les consommateurs dans une région donnée peuvent lire les messages à partir des followers locaux, même si le leader de la partition se trouve dans une autre région.​

### Connected clusters ​

La réplication est asynchrone​. Il faut utiliser un outil séparé pour copier les données d’un cluster à l’autre, le plus courant est « MirrorMaker 2 »​. C’est généralement utilisé avec le cluster source réservé à l’écriture et le cluster miroir à la lecture​

![kafka_geoclusters](./attachments/kafka/kafka_geoclusters.png)

## Console d’administration CMAK​ (Cluster Manager for Apache Kafka​)

Lien GitHub : https://github.com/yahoo/kafka-manager

## Les listeners

C'est le point d'entrée pour les connexions réseau au broker Kafka​.

### Configuration : ​
- listeners : ​
    - Per-broker – Default =PLAINTEXT://:9092 ​
    - Indique les adresses et ports où le broker écoutera les connexions.​
    - On peut nommer les connexions par exemple ​
        - INSIDE_LISTENER : Pour les connexions internes entre brokers et controllers et les clients sur le réseau local. ​
        - OUTSIDE_LISTENER : Pour les connexions externes avec des clients comme les producers et consumers​
        - Exemple Listeners = INSIDE://kafka-broker1:9092,OUTSIDE://localhost:19092​
        - Ces noms sont utilisables par d’autres paramètres.​
- advertised.listeners​
    - Per-broker – Default = null​
    - Indique aux clients quel endpoint utiliser pour se connecter au broker.​
    - Même principe que listeners​
    - Si pas configuré listeners est utilisé.​
- listener.security.protocol.map​
    - Per-broker – Default = SASL_SSL:SASL_SSL,PLAINTEXT:PLAINTEXT,SSL:SSL,SASL_PLAINTEXT:SASL_PLAINTEXT​
    - Protocoles : PLAINTEXT, SSL, SASL_SSL, SASL_PLAINTEXT​
    - Ne concerne que les listeners​
    - Mappage entre les noms des listeners et les protocoles de sécurité. Cela permet d'indiquer quel protocole utiliser pour chaque listener.​
        - Exemple : listener.security.protocol.map = INSIDE_LISTENER:PLAINTEXT, EXTERNAL_LISTENER:SASL_SSL​
- inter.broker.listener.name​
    - Static – default = null​
    - Listener a utilisé pour la communication inter-brokers​
    - Si pas configuré security.inter.broker.protocol est utilisé​
- controller.listener.names​
    - Static – default = null​
    - inter.broker.listener.name​
- security.inter.broker.protocol​
    - Static – default = PLAINTEXT​
    - Si pas explicitement configurer pas de conflit avec inter.broker.listener.name et le mappage​
    - Protocole a utilisé pour la communication inter-brokers​
    - Protocoles : PLAINTEXT, SSL, SASL_SSL, SASL_PLAINTEXT​
- On ne doit pas configurer « inter.broker.listener.name » et « security.inter.broker.protocol » en même temps​
- ssl.client.auth​
    - Per-broker – default = none​
    - Détermine si l'authentification des clients doit être requise, facultative ou non utilisée lors des connexions SSL.​
    - None, required ou requested​

## Sécurité du cluster

### Sécurité avec SSL inter-brokers/controllers et Client SASL PLAINTEXT​

Il faut fournir un fichier appelé « jaas.conf » avec 2 entrées :​
- KafkaServer même si SASL n’est pas utiliser pour la communication inter-broker​
- KafkaClient pour d'ajouter les utilisateurs SASL ​
- Redémarrage du cluster nécessaire pour ajouter un client​

```text
KafkaClient {​
    org.apache.kafka.common.security.plain.PlainLoginModule required​
    username="client_user"​
    password="123456";
};​

KafkaServer {​
    org.apache.kafka.common.security.plain.PlainLoginModule required ​
    users = "broker_user:123456";  ​
};​
```

### Sécurité avec SSL inter-brokers/controllers et Client SASL SCRAM​

Modification sur la configuration :​
- KAFKA_SASL_ENABLED_MECHANISMS=SCRAM-SHA-256 ou SCRAM-SHA-512​
- KAFKA_SASL_MECHANISM_INTER_BROKER_LISTENER= SCRAM-SHA-256 ou SCRAM-SHA-512​

Détails des paramètres​ : 
- KafkaServer : Configuration pour les brokers, ici, l'utilisateur admin est utilisé pour les opérations administratives.​
- KafkaClient : Configuration pour les clients Kafka, qui utiliseront client_user pour se connecter aux brokers.​
- KafkaBroker : Cette section est spécifique aux connexions inter-broker, ce qui permet d'utiliser un utilisateur et un mot de passe différents pour sécuriser les communications entre les brokers.​

​Redémarrage du cluster nécessaire pour ajouter un client​
​
```text
KafkaServer {​
   org.apache.kafka.common.security.scram.ScramLoginModule required​
   username="admin"​
   password="admin_password";​
};​

KafkaClient {​
   org.apache.kafka.common.security.scram.ScramLoginModule required​
   username="client_user"​
   password="client_password";​
};​

KafkaBroker {​
   org.apache.kafka.common.security.scram.ScramLoginModule required​
   username="inter_broker_user"​
   password="inter_broker_password";​
};​
```

### Sécurité avec SSL inter-brokers/controllers et client SASL GSSPI​

GSSAPI nécessite Kerberos pour fonctionner​. Il faut configurer un environnement Kerberos​.
- Un serveur KDC, ​
- Des principals pour Kafka et les clients​
- Des fichiers keytab et de configuration appropriée.​

Modification sur la configuration :​
- KAFKA_SASL_ENABLED_MECHANISMS=GSSAPI ​
- KAFKA_SASL_MECHANISM_INTER_BROKER_LISTENER=GSSAPI​

​Détails des paramètres​ :
- useKeyTab=true : Indique que le module doit utiliser un fichier de clé (keytab) pour l'authentification. Cela permet au service de se connecter sans avoir besoin d'un mot de passe explicite.​
- keyTab : Spécifie le chemin vers le fichier keytab. Vous devez vous assurer que le chemin correspond à celui où votre fichier keytab est stocké.​
- principal : Le principal Kerberos utilisé pour l'authentification. Cela doit correspondre au principal de votre service Kafka dans le KDC (Key Distribution Center).​
- storeKey=true : Permet au module de stocker la clé dans le cache des clés pour une utilisation ultérieure.​
- Redémarrage du cluster nécessaire pour ajouter un client​
```text
KafkaClient {​
  com.sun.security.auth.module.Krb5LoginModule required​
  useKeyTab=true​
  keyTab="/etc/kafka/secrets/your.keytab"   // Chemin vers votre fichier keytab​
  principal="your_principal@YOUR_REALM"      // Principal Kerberos​
  storeKey=true;​
};​

KafkaServer {​
  com.sun.security.auth.module.Krb5LoginModule required​
  useKeyTab=true​
  keyTab="/etc/kafka/secrets/your.keytab"   // Chemin vers votre fichier keytab​
  principal="your_principal@YOUR_REALM"      // Principal Kerberos​
  storeKey=true;​
};​
```

### Sécurité avec SSL cluster et client​
- PAS de fichier jaas.conf​
- Double authentification (mTLS)​
    - Côté Kafka : Vous configurez les keystores et truststores pour que Kafka puisse accepter les connexions SSL et s'authentifier avec les clients.​
        - Keystore : Contient les clés privées et les certificats associés aux brokers. Cela permet au cluster de s'authentifier auprès de l’application cliente​
        - Truststore : Contient les certificats des autorités de certification (CA) qui ont signé le certificat du client. Cela permet au cluster de vérifier l'identité du client lors de l'établissement de la connexion.​
    - Côté Client : Vous configurez également les keystores et truststores pour que le client puisse s'authentifier auprès de Kafka et vérifier son identité.​
        - Keystore : Contient les clés privées et les certificats associés à l’application cliente. Cela permet au client de s'authentifier auprès du serveur Kafka.​
        - Truststore : Contient les certificats des autorités de certification (CA) qui ont signé le certificat du serveur Kafka. Cela permet au client de vérifier l'identité du serveur Kafka lors de l'établissement de la connexion.​
- Redémarrage du cluster nécessaire pour ajouter un client

### Sécurité avec SSL inter-brokers/controllers et client SASL_SSL​
- SSL pure ne fournit pas d’authentification client​
- SASL_SSL permet d'ajouter des mécanismes d'authentification​
- Authentification possible AVEC PLAINTEXT, SCRAM ou GSSPI​
- Redémarrage du cluster nécessaire pour ajouter un client​
​
```text
KafkaServer {​
    org.apache.kafka.common.security.plain.PlainLoginModule required​
    username="broker"​
    password="broker-password"​
    user_broker="broker-password"​
    user_admin="admin-password";​
};​

Client {​
   org.apache.kafka.common.security.plain.PlainLoginModule required​
   username="admin" ​
   password="admin-password"; ​
};​
```

## Les ACLs
Les ACLs définissent qui peut effectuer quelles actions sur quelles ressources. ​

Types d'actions : 
- READ: Permet de lire des données à partir d'un topic.​
- WRITE: Permet d'écrire des données dans un topic.​
- CREATE: Permet de créer de nouveaux topics.​
- DELETE: Permet de supprimer des topics.​
- ALTER: Permet de modifier les configurations de topics.​
- DESCRIBE: Permet de décrire les configurations et les métadonnées des topics.​

Ressources que l’on peut protéger avec des ACLs :​
- Topic​
- Group​
- Cluster​

Protocoles possibles​ :
- Avec SSL seul​
    - user = CN (ou l'entrée dans SAN) du certificat du client ​
    - Exemple kafka-acls.sh --bootstrap-server <broker_address>:<broker_port> --add --allow-principal User:CN --operation All --topic <topic_name>​
- Avec SASL​
    - User = username du client (jaas.conf)​
    - Exemple kafka-acls.sh --bootstrap-server <broker_address>:<broker_port> --add --allow-principal User:CN --operation All --topic <topic_name>​

## Administration du cluster

Paramètres statiques : ​
- Ceux définis dans les fichiers de configuration du broker (ex. server.properties) ou via Docker Compose. ​
- Ils sont appliqués au démarrage du broker, mais ne sont pas visibles via les commandes de kafka-configs.sh.​
- Redémarrage nécessaire​
- Les paramètres statiques (read-only) sont visibles dans le fichier server.properties​ communément dans /opt/kafka/config/server.properties 

Paramètres dynamiques : ​
- Conçus pour permettre des ajustements à chaud.​
- Ceux qui peuvent être modifiés et consultés en direct (runtime) via kafka-configs.sh. ​
- Par exemple, les configurations spécifiques aux topics peuvent être modifiées et visibles dynamiquement.​
- Pas de redémarrage nécessaire​
- Ils sont conservés après redémarrage​

Update Mode:​
- read-only: ​
    - Static - Requires a broker restart for update – per broker​
- per-broker: ​
    - Dynamic - May be updated dynamically for each broker​
- cluster-wide: ​
    - Dynamic - May be updated dynamically as a cluster-wide default. ​
    - Updating once should reflect for other brokers​
- Avant de modifier un paramètre vérifier si il sera nécessaire de redémarrer le cluster​

### Configuration - Les bases

**CLUSTER_ID** ​: Il est défini automatiquement par Kafka, on ne peut pas le paramétrer ou modifier.​

**​auto.create.topics.enable** : ​
- Statique - Default = true ​
- Permet la création automatique de topics lors de l'envoi de messages. ​

**broker.id** :​
- Statique - Default = -1​
- ID unique du broker dans le cluster. À définir dans la config​

**broker.id.generation.enable** : Génération automatique d’un ID unique pour chaque broker au démarrage sauf si le broker.id est présent​
- Statique - Default = true​
- Si aucun ID n'est fourni, le broker échouera au démarrage.​
- Les IDs générés automatiquement commence à reserved.broker.max.id + 1​

**compression.type** : ​
- Cluster-wide - Default = producer​
- Type de compression à utiliser pour les messages ​
- valeurs possibles : none, gzip, snappy, lz4, zstd, producer​

**compression.gzip.level** : ​
- Cluster-wide - Default = -1​

**compression.lz4.level​** :
- Cluster-wide - Default = 9​

**compression.zstd.level​** :
- Cluster-wide - Default = 3​

**delete.topic.enable** : ​Permet la suppression des topics par un administrateur. ​
- Statique - Default : true​


### Configuration - Cleanup policies & log retention​

**log.cleanup.policy​** : Contrôle le nettoyage des messages. Il permet de spécifier si les logs doivent être supprimés ou compressés ou les deux (delete, compact)​
- Cluster-wide - Default = delete​

#### Compaction

**log.cleaner.enable** : ​Active ou désactive le compactage de logs. Fonctionne avec cleanup.policy = compact​
- Statique - Default = true​

**log.cleaner.min.compaction.lag.ms** : ​Durée de rétention d’un message marqué « à supprimer » par le compactage. Fonctionne avec log.cleanup.policy = compact​
- Cluster-wide - Default = 86400000 (24 heures)​

#### Suppression

**log.retention.ms** : ​Durée en millisecondes pendant laquelle les messages sont conservés dans les logs. Fonctionne avec log.cleanup.policy = delete​
- Cluster-wide - Default =  604800000 (7 jours).​
- Priorité la plus élevée : si ce paramètre est défini, Kafka l’utilise et ignore log.retention.minutes, log.retention.hours et log.retention.days​

**log.retention.minutes** : ​
- Cluster-wide - Default = null​
- Si log.retention.ms n’est pas défini, Kafka regarde ensuite ce paramètre (en minutes).​

**log.retention.hours** : ​
- Cluster-wide - Default = 168 (7 jours)​
- Si ni log.retention.ms ni log.retention.minutes ne sont définis, Kafka utilise log.retention.hours.​

**log.retention.bytes** :​
- Cluster-wide - Default = -1​
- Taille maximale des logs (en octets) à conserver pour chaque topic. Fonctionne indépendamment des paramètres de rétention temporelle. ​

### Configuration - Suppression & segments​

Topic -> Partition/Replica -> Segments

Chaque partition de Kafka est composée de plusieurs segments. Chaque segment contient un ensemble de messages dans un fichier sur disque.​ Les segments sont stockés dans un répertoire spécifique au broker, généralement sous une hiérarchie de dossiers correspondant aux topics et partitions. ​La rotation des segments est paramétrée par taille ou temps, 1Go ET 7 jours par default.​

Intérêts :​
- *Performance* : La lecture/écriture dans des segments plus petits que des partitions entières sont plus rapides et plus efficaces en termes d'entrées/sorties disque (I/O).​
- *Suppression facile* : En supprimant simplement des segments entiers on évite des opérations coûteuses de suppression de messages individuels.​
- *Compaction efficace* : Le compactage se fait à l'échelle des segments,ce qui permet de maintenir un état à jour pour les clés, tout en réduisant l'utilisation du disque.​

Quant à la suppresion, Kafka supprime toujours des segments entiers, pas des messages individuels, ce qui simplifie le processus de suppression.​

**log.segment.bytes​** : Définit la taille maximale d'un segment de log. Lorsque cette taille est atteinte le segment est "fermé" et un nouveau segment est créé.
- Cluster-wide - Default : 1073741824 octets (1 Go)​

​**log.roll.ms** : ​
- Cluster-wide - Default = null​
- Spécifie la durée maximale pendant laquelle un segment de log reste actif. Lorsque cette durée est dépassée le segment est "fermé" et un nouveau segment est créé.​
- Priorité sur log.rolls.hours​​

**log.roll.hours** : ​
- Statique - Default = 168 (7 jours)​
- Spécifie la durée maximale pendant laquelle un segment de log reste actif. Lorsque cette durée est dépassée, un nouveau segment est créé.​

Concernant le compactage, Kafka fusionne les messages dans les segments fermés pour ne garder que les dernières versions de chaque clé. ​

### Configuration - Leader Retention​

**log.local.retention​** : Ces paramètres définissent la durée de rétention des logs pour les partitions leader. Cela signifie que les messages sont conservés dans les logs sur le disque du leader tant que cette période n'est pas écoulée.​ Les réplicas ne sont pas concernés​
- Non activé par défaut​
- Attention à l’incohérence des données entre les leaders et les partitions​

**log.local.retention.ms​** :
- Cluster-wide - Default = -2 -> log.retention.ms est utilisé​

**log.local.retention.bytes​** :
- Cluster-wide - Default = -2 -> log.retention.bytes est utilisé​

​**message.max.bytes​** :
- Cluster-wide - Default = 1 048 588​
- Taille maximale d’un batch de messages qu'un broker peut accepter (en octets) après compression (si activée)​
- Retourne e code d'erreur 8, qui correspond à MESSAGE_TOO_LARGE dans le protocole de Kafka.​

​**replica.fetch.max.bytes** : ​
- Statique - Default = 1 048 576 (1 mebibyte)​
- Taille maximale des messages compressés que les réplicas peuvent récupérer du leader. ​
- Important de le configurer en conséquence si vous modifiez message.max.bytes pour que les réplicas puissent toujours récupérer les messages du leader.​

**fetch.max.bytes** :​
- Statique - Default = 52428800 (50 mebibytes)​
- Taille maximale totale des batches de données que le consommateur peut récupérer dans une seule demande. Cela inclut des messages provenant de plusieurs partitions​

**log.message.timestamp.type​** :
- Cluster-wide - Default = CreateTime​
- Détermine le type d'horodatage qui est associé à chaque message dans un topic. Deux valeurs possibles : CreateTime et LogAppendTime.​
    - CreateTime :​
        - Utilise l'horodatage défini par le producer au moment de la création du message. Chaque producer est responsable de son horodatage.​
        - Si le producer n'inclut pas de timestamp dans le message, Kafka renvoie une erreur du type InvalidTimestampException au moment de la réception du message.​
    - LogAppendTime :​
        - L'horodatage est défini par le broker lorsque le message est reçu.​

**num.partitions​** :
- Statique - Default = 1​
- Nombre par défaut de partitions créées pour chaque nouveau topic​

**default.replication.factor​** :
- Statique - Default = 1​
- Facteur de réplication par défaut pour les topics créés automatiquement​

**min.insync.replicas​** :
- Cluster-wide - Default = 1​
- Nombre minimum de réplicas qui doivent être en ligne pour qu'une écriture réussisse.​
- Lié avec la configuration du producers acks à "all“​
- Si échec retourne NotEnoughReplicas ou NotEnoughReplicasAfterAppend​

**replica.lag.time.max.ms​** :
- Statique - Default : 30000 (30 seconds)​
- Si une réplique ne parvient pas à récupérer les données du leader pendant cette période, elle sera retirée de la liste des répliques synchronisées (ISR).​

**unclean.leader.election.enable​** :
- Cluster-wide - Default = false​
- Permet l'élection d’un replica non synchronisé en dernier ressort.​

### Configuration - Sécurité Brokers​

**ssl.keystore.location​**
- Per-broker - Default = null​
- Chemin vers le keystore.​

​**ssl.keystore.password​**
- Per-broker - Default = null​
- Mot de passe pour le keystore SSL.​

​**ssl.truststore.location​**
- Per-broker - Default = null​
- Chemin vers le truststore SSL.​

​**ssl.truststore.password​**
- Per-broker - Default = null​
- Mot de passe pour le truststore SSL.​

### Configuration - Les topics

Les configurations principales des topics ont un équivalent au niveau cluster/broker. ​Si aucune configuration « per-topic » n’est définie celle du cluster/broker est prise en compte. ​Les modifications apportées aux configurations des topics sont appliquées immédiatement sans nécessiter un redémarrage du cluster.​ Il n’y a pas d’équivalent pour num.partitions au niveau topic mais on peut quand même spécifier ce paramètre.

topic|Cluster-Broker
---|---
cleanup.policy​|log.cleanup.policy​
compression.type, compression.gzip.level , compression.lz4.level, compression.lz4.level, compression.zstd.level​|idem​
local.retention.bytes, local.retention.ms​|log.local.retention.bytes, log.local.retention.ms​
message.timestamp.type​|log.message.timestamp.type​
max.message.bytes​|message.max.bytes​
min.compaction.lag.ms​|log.cleaner.min.compaction.lag.ms​
min.insync.replicas​|min.insync.replicas​
retention.bytes, retention.ms​|log.retention.bytes, log.retention.ms​
replication.factor​|default.replication.factor​
segment.ms, segment.bytes​|log.roll.ms, log. segment.bytes​
unclean.leader.election.enable​|idem​

#### Information générale du Cluster​
```bash
kafka-metadata-quorum.sh --bootstrap-server INSIDE://kafka-broker1:9092 describe –-status​
```
```
ClusterId:              	5L6g3nShT-eMCtK--X86sw​
LeaderId:               	3​
LeaderEpoch:            	1​
HighWatermark:          	8510​
MaxFollowerLag:         	0​
MaxFollowerLagTimeMs:       320​
CurrentVoters:          	[1,2,3]​
CurrentObservers:       	[4,5,6,7,8]
```

### Configuration - Les scripts

#### Commandes en ligne​
- -- help ​
- Il y a toujours le paramètre –bootstrap-server pour se connecter à un des nœuds data​
- On peut également utiliser –bootstrap-controller pour cibler un controller​

#### Brokers​

kafka-configs.sh --bootstrap-server  <broker-data> –version . ​

kafka-configs.sh --bootstrap-controller <broker-controller> --version​

kafka-configs.sh --bootstrap-server <broker-data> --entity-type brokers --entity-name 4 --alter --add-config message.max.bytes=1000000​

kafka-configs.sh --bootstrap-server <broker-data> --entity-type brokers --entity-name 4 --describe​

#### Topics​

kafka-topics.sh --bootstrap-server <broker-data> --create --topic <topic_name> --partitions 2 --replication-factor 2​

kafka-topics.sh --bootstrap-server <broker-data> --describe --topic <topic_name>​

kafka-configs.sh --bootstrap-server <broker-data> --entity-type topics --entity-name <mon_topic> --alter --add-config retention.ms=500 000 000​

kafka-configs.sh --bootstrap-server <broker-data> --entity-type topics --entity-name <mon_topic> --alter --delete-config retention.ms​

#### Events​

kafka-console-producer.sh --bootstrap-server <broker-data> --topic <topic_name> ​

kafka-console-consumer.sh --bootstrap-server <broker-data> --topic <topic_name> --from-beginning​

#### Test de charge​

kafka-producer-perf-test.sh --topic <topic_name> --num-records 1000 --record-size 100 --throughput -1 --producer-props bootstrap.servers=INSIDE://kafka-broker1:9092​

kafka-consumer-perf-test.sh --topic <topic_name> --bootstrap-server INSIDE://kafka-broker4:9092 --messages 1000​

Scripts|Description
---|---
kafka-acls.sh|Gère les Access Control Lists (ACLs) pour la sécurisation des topics et des opérations Kafka.​
kafka-consumer-groups.sh​|Permet de gérer les groupes de consommateurs, y compris la visualisation et la modification de l'état.​
kafka-console-consumer.sh​|Fournit une interface en ligne de commande pour consommer des messages à partir d'un topic.​
kafka-console-producer.sh​|Fournit une interface en ligne de commande pour produire des messages dans un topic.​
kafka-exporter.sh​|Permet d'exporter des données de Kafka vers un autre système.​
kafka-topics.sh​|Permet de créer, modifier, supprimer et lister les topics dans Kafka.​
kafka-configs.sh​|Gère les configurations des brokers, des topics et des utilisateurs.​
kafka-reassign-partitions.sh​|Gère le ré-assignement des partitions entre les brokers.​
kafka-storage.sh​|Fournit des outils pour interagir avec les logs de stockage et effectuer des opérations de maintenance.​
kafka-leader-election.sh​|Gère les élections de leaders pour les partitions de topics.​


### Configuration - Les logs

Types de logs utiles à surveiller​
- Server logs : événements serveur Kafka.​
- Controller logs : informations sur les décisions prises par le contrôleur du cluster.​
- Logs de réplicas : montrent les informations sur la synchronisation des réplicas entre les brokers.
- Folder : opt/kafka/log

### Configuration - Les métriques

Ce sont les données de performance et d'état du cluster, comme le suivi des latences, du débit des messages ou de l'utilisation des ressources. ​Elles permettent de comprendre comment Kafka se comporte en production. ​Elles sont exposées via JMX (Java Management Extensions). Elles peuvent être capturées via des systèmes de surveillance comme Prometheus, Grafana, ou d'autres outils de monitoring.​ Voici quelques exemples de métriques importantes dans Kafka :​

#### Métriques des Brokers​
- Messages In/Out Rate : Taux de messages produits ou consommés par les topics.​
- Byte Rate : Taux de bytes produits ou consommés.​
- Request Latency : Temps de latence pour les différentes requêtes comme PRODUCE, FETCH.​

#### Métriques de Topic/Partition​
- Partition Under-Replicated : Nombre de partitions sous-répliquées (réplicas ne sont pas synchronisés).​
- Partition Lag : Indique le décalage des réplicas d'une partition par rapport à l’ISR (In-Sync Replica).​

#### Métriques de Consommateurs (Consumers)​
- Consumer Lag : Décalage entre le dernier offset consommé et l’offset de fin de partition.​
- Records Consumed Rate : Taux de messages consommés.      ​

#### Métriques de Producteurs (Producers)​
- Record Send Rate : Nombre de messages envoyés par seconde.​
- Request Latency : Temps de latence des requêtes PRODUCE.​

#### Métriques KRaft (Kafka Raft)​
- Raft Election Metrics : Indicateur de l'état des élections de Raft.​
- Raft Log Replication : Informations sur la réplication des journaux Raft.​

### Sizing - Bonnes pratiques

#### Évaluer le Volume de Données​

**Débit** (Throughput)​
- Débit de production : Mesure le nombre de messages produits par seconde.​
- Débit de consommation : Mesure le nombre de messages consommés par seconde.​
- Débit total=Débit de production + Débit de consommation​

**Taille des Messages​**
- Évaluer la taille moyenne des messages. Par exemple, 10 000 messages par seconde avec une taille moyenne de 1 Ko, cela représente 10 Mo par seconde.​
- Déterminer la rétention des Données​
- Déterminer le facteur de rétention​

#### Calcul de l'espace de stockage requis :​
- Espace de stockage requis = Débit total × Taille moyenne des messages x Durée de rétention en seconde x facteur de rétention​
- Les controllers n’ont pas besoin de beaucoup d’espace disque​

#### Calcule du nombre de brokers​

- Nombre de brokers = Espace de stockage total requis​ / capacité de stockage par broker​

#### Configuration du partitionnement​

Chaque partition doit être répliquée sur au moins deux brokers pour éviter la perte de données. Un bon point de départ est d'avoir entre 3 et 6 partitions par topic pour un petit cluster et d'augmenter en fonction de la charge.​ Une taille de partition entre 1 Go et 10 Go est souvent considérée comme optimale pour la plupart des cas d'utilisation​. Une règle générale est d'avoir entre 50 et 200 partitions par broker. Au-delà de 100 ce sont des cas particuliers.​ Il faut surveiller régulièrement les performances (métrique JMX) pour ajuster le nombre des partitions et donc la taille.​ Il n'y a pas de nombre maximal de partitions définit dans Kafka, MAIS la pratique recommande :​
- Nombre maximal par Topic  2 000 à 3 000 (1000 à 2000 c’est déjà bien)​
- Nombre maximal dans le cluster : 20 000 à 50 0000​
- Pour les petits clusters (2-3 brokers) : 5 000 à 10 000 partitions au total.​
- Pour les clusters de taille moyenne (4-10 brokers) : 10 000 à 20 000 partitions au total.​
- Pour les grands clusters (plus de 10 brokers) : 20 000 à 30 000 partitions au total sous surveillance des performances​

#### CPU ​

Il faudrait connaitre le nombre de message que peut traiter la CPU par secondes = K​
- Nombre _CPU = Débit des messages x Taille des messages / K​

#### Mémoire​

Estimation de la mémoire par partition : environ 10 à 20 Mo, cela varie en fonction de l'utilisation spécifique et des paramètres de configuration.​
- Mémoire_Utilisée = Mémoire_Par_Partition × Nombre de Partitions​

#### Réseau​

Pour une réplication synchrone fiable pour des charges modérées, une latence réseau inférieure à 10 ms et une bande passante d'au moins 1 Gbps sont recommandées. ​Pour un gros volume des données (plusieurs To/jour), une bande passante de 10 Gbps ou plus peut être nécessaire pour assurer une réplication fluide et éviter les congestions.​
