# Ansible 

## Introduction

### How Ansible Works

It’s important to note the following:

- Ansible runs each task in parallel across all hosts.
- Ansible waits until all hosts have completed a task before moving to the next task. 
- Ansible runs the tasks in the order that you specify them.

### A Note About Versions

For years the Ansible community has been highly active in creating roles and modules—so active that there are thousands of modules and more than 20,000 roles. The difficulties of managing a project of this scale led creators to reorganize the Ansible content into three parts:

- <u>Core components</u>, created by the Ansible team
- <u>Certified content</u>, created by Red Hat’s business partners
- <u>Community content</u>, created by thousands of enthusiasts worldwide

### What’s So Great About Ansible?

In short: Ansible is simple, powerful, and secure.

- <u>Easy-to-read syntax</u>

Ansible uses the YAML file format and Jinja2 templating,Ansible configuration management scripts are called playbooks. Ansible actually builds the playbook syntax on top of YAML, which is a data format language that was designed to be easy for humans to read and write. In a way, YAML is to JSON what Markdown is to HTML.

- <u>Little to nothing to install on the remote hosts</u>

To manage servers with Ansible, Linux servers need to have SSH and Python installed, while Windows servers need WinRM enabled. On Windows, Ansible uses PowerShell instead of Python, so there is no need to preinstall an agent or any other software on the host.

- <u>Easy to share</u>

The primary unit of reuse in the Ansible community nowadays is the collection. You can organize your modules, plug-ins, libraries, roles, and even playbooks into a collection and share it on Ansible Galaxy. 

- <u>Declarative</u>

Ansible modules are declarative. You use them to describe the state you want the server to be in.

- <u>Push-based</u>

Chef and Puppet are configuration management systems that use agents. They are pull-based by default. Agents installed on the servers periodically check in with a central service and download configuration information from the service. In contrast, Ansible is push-based by default. As soon as you run the ansible-playbook command, Ansible connects to the remote servers and does its thing; this lowers the risk of random servers potentially breaking whenever their scheduled tasks fail to change things successfully. The push-based approach has a significant advantage: you control when the changes happen to the servers. You do not need to wait around for a timer to expire. Each step in a playbook can target one or a group of servers. You get more work done instead of logging into the servers by hand.

- <u>Really scalable</u>

- <u>Equivalent environments</u>

Ansible has a clever way to organize content that helps define configuration at the proper level. It is easy to create a setup for distinct development, testing, staging, and production environments.

- <u>Secure transport</u>

Ansible simply uses Secure Shell (SSH) for Linux and WinRM for Windows.

- <u>Idempotency</u>

Modules are also idempotent. Idempotence is a nice property because it means that it is safe to run an Ansible playbook multiple times against a server.

- <u>No daemons</u>

There is no Ansible agent listening on a port. Therefore, when you use Ansible, there is no extra attack surface.


## Installation and Setup

Ansible can manage only the servers it explicitly knows about. You provide Ansible with information about servers by specifying them in an inventory. We can use the ansible command-line tool to verify that Ansible is able to connect to the server. You won’t use the ansible command often; it’s mostly used for ad hoc, one-off things. The "changed": false part of the output tells us that executing the module did not change the state of the server.

Traditional system administrators are cautious when tools are introduced that need system privileges, because typically only the system administrators themselves have these permissions. A common pattern on Unix is to delegate only specific commands to developers using the sudo tool with carefully crafted files in /etc/sudoers.d/. This approach does not work with Ansible, nor does a restrictive shell like rbash. Ansible creates temporary directories with random names for various Python scripts, while sudo needs exact commands. The alternative is shifting the focus on the content of changes to version control, in a staging environment, and having a sudoers file for the ansible group like:

```bash
%ansible    ALL=(ALL) ALL
```

## Playbooks

Playbook is the term that Ansible uses for a configuration management script. The ```ansible-playbook``` command executes playbooks. Ansible playbooks are in YAML syntax. It is good practice to check all YAML files with a command-line tool called ```yamllint```.

### Plays

Looking at the YAML, it should be clear that a playbook is a list of dictionaries. Specifically, a playbook is a list of plays. Every play must contain the hosts variable

### Tasks

### Modules

Modules are scripts that come packaged with Ansible and perform some kind of action on a host. Ansible executes a task on a host by generating a custom script based on the module name and arguments, and then copies this script to the host and runs it. The modules for Unix/Linux that ship with Ansible are written in Python, and the modules for Windows are written in PowerShell

Putting it all together, a playbook contains one or more plays. A play associates an unordered set of hosts with an ordered list of tasks. Each task is associated with exactly one module.

![playbook_overview](./attachments/ansible/playbook_overview.png)

### Did Anything Change? Tracking Host State

Any Ansible task that runs has the potential to change the state of the host in some way. Ansible modules will first check to see whether the state of the host needs to be changed before taking any action. If the host’s state matches the module’s arguments, Ansible takes no action on the host and responds with a state of “ok.” On the other hand, if there is a difference between the host’s state and the module’s arguments, Ansible will change the state of the host and return “changed.” You can use Ansible’s state change detection to trigger additional actions using handlers. But, even without using handlers, seeing  what changes and where, as the playbook runs, is still a detailed form of feedback.

### Generating a file Template

We use the .j2 extension to indicate that the file is a Jinja2 template. However, you can use a different extension if you like; Ansible doesn’t care. Ansible also uses the Jinja2 template engine to evaluate variables in playbooks.

### Handlers

Handlers are one of the conditional forms that Ansible supports. A handler is similar to a task, but it runs only if it has been notified by a task. A task will fire the notification if Ansible recognizes that the task has changed the state of the system. Handlers usually run at the end of the play after all of the tasks have been run. To force a notified handler in the middle of a play, we can use the meta clause.

### Validation

```ansible-lint``` is a Python tool that helps you find potential problems in playbooks.


## Inventory

The Ansible inventory is a very flexible object. It can be a file, a directory, or an executable, and some executables are bundled as plug-ins. Inventory plug-ins allow us to point at data sources, like your cloud provider, to compile the inventory. An inventory can be stored separately from your playbooks. This means  that you can create one inventory directory to use with Ansible on the command line, with hosts running in Vagrant, Amazon EC2, Google Cloud Platform, or Microsoft Azure, or wherever you like!

### Behavioral Inventory Parameters:

![inventory_parameters](./attachments/ansible/inventory_parameters.png)

### Groups

We can perform configuration actions on groups of hosts, rather than on an individual host. Ansible automatically defines a group called all (or *), which includes all the hosts in the inventory. Ansible also allows you to define groups that are made up of other groups. 

### Aliases and Ports

Ansible resolves hostnames using the inventory, your SSH config file, /etc/hosts, and DNS. This flexibility is useful in development but can be a cause of confusion.

### Host and Group Variables: In Their Own Files

The inventory file is a reasonable place to put host and group variables if you don’t have too many hosts. But as your inventory gets larger, it gets more difficult to manage variables this way. Ansible offers a more scalable approach to keep track of host and group variables: you can create a separate variable file for each host and each group. Ansible expects these variable files to be in YAML format. It looks for host variable files in a directory called host_vars and group variable files in a directory called group_vars.

### Dynamic Inventory

Up until this point, we’ve been explicitly specifying all our hosts in our hosts inventory file. However, you might have a system external to Ansible that keeps track of your hosts. Ansible supports a feature called dynamic inventory that allows you to avoid this duplication. If the inventory file is marked executable, Ansible will assume it is a dynamic inventory script and will execute the file instead of reading it.

### add_host

The add_host module adds a host to the inventory; this is useful if you’re using Ansible to provision new virtual machine instances inside an infrastructure-as-a-service cloud.

### group_by

Ansible’s group_by module allows you to create new groups while a playbook is executing. Any group you create will be based on the value of a variable that has been set on each host, which Ansible refers to as a fact.


## Variables and Facts

The simplest way to define variables is to put a vars section in your playbook with the names and values of your variables. 

### Registering Variables

Often, you’ll need to set the value of a variable based on the result of a task. Remember that each Ansible module returns results in JSON format. To use these results, you create a registered variable using the register clause when invoking a module. 

### Facts

When Ansible gathers facts, it connects to the hosts and queries it for all kinds of details about the hosts: CPU architecture, operating system, IP addresses, memory info, disk info, and more. You can access this data in the ansible_facts variable.

### Built-In Variables

Ansible defines several variables that are always available in a playbook.

![builtin_vars](./attachments/ansible/builtin_vars.png)

### Precedence

When the same variable is defined in multiple ways, the precedence rules determine which value wins (or override). Here is a simple rule of thumb: the closer to the host, the higher the precedence.


## Debugging Ansible Playbooks

### Common SSH Challenges

Ansible uses SSH to connect to and manage hosts, often with administrative privileges. It is worthwhile to know about its security challenges, which can puzzle casual users at first.

- PasswordAuthentication no

PasswordAuthentication does not greatly improve the security of your servers. By default, Ansible assumes you are using SSH keys to connect to remote machines. Having a SSH key pair is one thing, but the public key needs to be distributed to the machines you want to manage. This is traditionally done with ```ssh-copy-id```, but when PasswordAuthentication is disabled, an administrator needs to use an account with public keys in place to copy your public key to the servers, preferably with the authorized_key module.

- SSH as a Different User

You can connect to different hosts with different users. Restrict users from logging in as the root user as much as possible. If you need a particular user per machine, then you can set ansible_user in the inventory.

- Host Key Verification Failed

If that happens, don’t disable StrictHostKeyChecking in the SSH config. Instead, remove the old host key and add the new key.

### Checking Your Playbook Before Execution

The ansible-playbook command supports several flags that allow you to "sanity-check" your playbook before you execute it. They do not execute the playbook.

- <u>Syntax Check</u>: the ```--syntax-check``` flag checks that your playbook’s syntax is valid.
- <u>List Hosts</u>: the ```--list-hosts``` flag outputs the hosts against which the playbook will run.
- <u>List Tasks</u>: the ```--list-tasks``` flag outputs the tasks against which the playbook will run.
- <u>Check Mode</u>: the ```-C``` and ```--check``` flags run Ansible in check mode (sometimes called a dry run). 
- <u>Diff (Show File Changes)</u>: the ```-D``` and ```-diff``` flags output differences for any files that are changed on the remote machine.
- <u>Tags</u>: Use the ```-t``` tagnames or ```--tags``` tagnames flag to tell Ansible to run only plays and tasks that have certain tags. Use the ```--skip-tags``` tagnames flag to tell Ansible to skip plays and tasks that have certain tags.
- <u>Limits</u>: Ansible allows you to restrict the set of hosts targeted for a playbook with a ```--limit``` flag.


## Roles: Scaling Up Your Playbooks

In Ansible, the role is the primary mechanism for breaking a playbook into multiple files. This simplifies writing complex playbooks, and it makes them easier to reuse. Think of a role as something you assign to one or more hosts. For example, you’d assign a database role to the hosts that will function as database servers. One of the things I like about Ansible is how it scales both up and down. Ansible scales down well because simple tasks are easy to implement. It scales up well because it provides mechanisms for decomposing complex jobs into smaller pieces. A role is very structured and doesn’t have any site-specific data in it, so it can be shared with others, who can compose their site by combining roles in their own playbooks.

### Basic Structure of a Role

An Ansible role has a name, such as database. Files associated with the database role go in the roles/database directory, which contains the following files and directories:

- defaults/
- files/
- handlers/
- meta/
- tasks/
- templates/
- vars/

### Pre-Tasks and Post-Tasks

Ansible allows you to define the order in your playbooks:

- A list of tasks that execute before the roles with a pre_tasks section
- A list of roles to execute
- A list of tasks that execute after the roles with a post_tasks section

There’s one important caveat when it comes to using the copy, script, or template modules. There is a difference between tasks defined in a role and tasks defined in a regular playbook. When invoking copy or script in a task defined in a role, Ansible will look in this order in these directories for the location of the file to copy or run and will use the first one found. These paths are relative to the directory where you start the top-level playbook from: 

- ./roles/role_name/files/
- ./roles/role_name/
- ./roles/role_name/tasks/files/
- ./roles/role_name/tasks/
- ./files/
- ./

Similarly, when invoking template in a task defined in a role, Ansible will first check the role_name/templates directory and then the playbooks/templates directory for the location of the template to use (along with less obvious directories). This way, roles define default files in their files/ and templates/ directories, but you cannot simply override them with files in the files/ and templates/ subdirectories of your project.

### Creating Role Files and Directories with ansible-galaxy

Its primary purpose is to download roles that have been shared by the community

```bash
ansible-galaxy role init --init-path playbooks/roles <name>
```

### Dependent Roles

Ansible supports a feature called dependent roles to deal with this scenario. When you define a role, you can specify that it depends on one or more other roles. Ansible will ensure that roles that are specified as dependencies are executed first. Role dependencies are stored in the meta/main.yml file.

### Ansible Galaxy/Command-Line Interface

- ```ansible-galaxy install <role_name>```:  install the role
- ```ansible-galaxy list```:  list installed roles
- ```ansible-galaxy remove <role_name>```: remove a role

### Ansible Galaxy/Role Requirements in Practice

It is common practice to list dependencies in a file called requirements.yml in the roles directory, located at <project_directory>/roles/requirements.yml. 


## Complex Playbooks

### Dealing with Badly Behaved Commands

We can use changed_when and failed_when clauses to change how Ansible detects that a task has changed state or failed.

### Lookups

Ansible supports a collection of lookups for retrieving data from diverse sources like file, items, vars…

### With Lookup Plug-in

It’s good to know that with_items relies on a lookup plug-in; items is just one of the lookups. Ansible provides a bunch of constructs for looping with a lookup plug-in. You can even hook up your own lookup plug-in to iterate. Exemples: with_items, with_lines, with_dict… 

### Labeling the Output

The label control was added in Ansible 2.2 and provides some control over how the loop output will be shown to the user during execution. We can simply add a label in the loop_control clause describing what should be printed when we iterate over the items. Cautious note: Keep in mind that running in verbose mode (using ```-v```) will show the full dictionary; don’t use label to hide your passwords from log output! Set no_log: true on the task instead.

### Imports and Includes

The import_* feature allows you to include tasks, or even whole roles, in the tasks section of a play through the use of the keywords import_tasks and import_role. When importing files in other playbooks statically, Ansible runs the plays and tasks in each imported playbook in the order they are listed, just as if they had been defined directly in the main playbook.

The include_* features allow you to dynamically include tasks, vars, or even whole roles by the use of the keywords include_tasks, include_vars, and include_role. This is often used in roles to separate or even group tasks and task arguments to each task in the included file. Included roles and tasks may or may not run, depending on the results of other tasks in the playbook. When a loop is used with include_tasks or include_role, the included tasks or role will be executed once for each item in the loop. Please note that the bare include keyword is deprecated in favor of the keywords include_tasks, include_vars, and include_role.

### Blocks

Much like the include_* clauses, the block clause provides a mechanism for grouping tasks. It allows you to set conditions or arguments for all tasks within a block at once.

### Error Handling with Blocks

- <u>block</u> starts the construct. 
- <u>rescue</u> lists tasks to be executed in case of a failure in the block clause. 
- <u>always</u> lists tasks to execute either way. 

The tasks under the always clause will be executed, even if an error occurs in the rescue clause! Be careful what you put in the always clause.

### Encrypting Sensitive Data with ansible-vault

Ansible provides an alternative solution: instead of keeping the secrets.yml file out of version control, we can commit an encrypted file. That way, even if our version control repository is compromised, the attacker can’t access the contents of the file unless they also have the password used for the encryption. The ```ansible-vault``` command-line tool allows us to create and edit an encrypted file that ```ansible-playbook``` will recognize and decrypt automatically, given the password.

```ansible-playbook``` needs to prompt us for the password of the encrypted file, or it will simply error out. Do so by using the ```--ask-vault-pass``` argument: 

```bash
ansible-playbook --ask-vault-pass playbook.yml
```

You can also store the password in a text file and tell ```ansible-playbook``` its location by using the ANSIBLE_VAULT_PASSWORD_FILE environment variable or the ```--vault-password-file``` argument: 

```bash
ansible-playbook playbook.yml --vault-password-file ~/password.txt
```

If the argument to ```--vault-password-file``` has the executable bit set, Ansible will execute it and use the contents of standard out as the vault password. This allows you to use a script to supply the password to Ansible.

![vault_commands](./attachments/ansible/vault_commands.png)


## Customizing Hosts, Runs, and Handlers

![pattern_hosts](./attachments/ansible/pattern_hosts.png)

### Running a Task on the Control Machine

Sometimes you want to run a particular task on the control machine instead of on the remote host. To support this, Ansible provides the delegate_to: localhost clause for tasks. You want to execute the task on a different server. 

### Running on One Host at a Time

By default, Ansible runs each task in parallel across all hosts. Sometimes you want to run your task on one host at a time. The canonical example is when upgrading application servers that are behind a load balancer. Typically, you take the application server out of the load balancer, upgrade it, and put it back. But you don’t want to take all of your application servers out of the load balancer, or your service will become  unavailable. You can use the serial clause on a play to tell Ansible to restrict the number of hosts on which a play runs.

Normally, when a task fails, Ansible stops running tasks against the host that fails but continues to run them against other hosts. In the load-balancing scenario, you might want Ansible to fail the entire play before all hosts have failed a task. Otherwise, you might end up with no hosts left inside your load balancer (you have taken each host out of the load balancer and they all fail). You can use a max_fail_percentage clause along with the serial clause to specify the maximum percentage of failed hosts before Ansible fails the entire play

### Running on a Batch of Hosts at a Time

You can also pass a serial percentage value instead of a fixed number. We can get even more sophisticated. For example, you might want to run the play on one host first, to verify that it works as expected, and then run it on a larger number of hosts in subsequent runs. Since version 2.2, Ansible has let users specify a list of serials (number or percentage) to achieve this behavior. Ansible will restrict the number of hosts on each run to the next available serial item unless the end of the list has been reached or there are no hosts left. This means that the last serial will be kept and applied to each batch run as long as there are hosts left in the play.

### Running Strategies

The strategy clause on a play level gives you additional control over how Ansible behaves per task for all hosts.

- Linear
- Free

Another strategy available in Ansible is the free strategy. In contrast to linear, Ansible will not wait for results of the task to execute on all hosts. Instead, if a host completes one task, Ansible will execute the next task on that host. As a result, some hosts will already be configured, while others are still in the middle of the play.

### Handlers in Pre- and Post-Tasks

When we covered handlers, you learned that they are usually executed after all tasks once, and only when they get notified. But keep in mind there are not only tasks but pre_tasks and post_tasks. Each task section in a playbook is handled separately; any handler notified in pre_tasks, tasks, or post_tasks is executed at the end of each section. As a result, it is possible to execute one handler several times in one play

### Flush Handlers

You may be wondering why we wrote that handlers usually execute after all tasks. We say usually because this is the default. However, Ansible lets us control the execution point of the handlers with the help of a special clause called meta.

### Handlers Listen

The listen clause defines what we’ll call an event, on which one or more handlers can listen. This decouples the task notification key from the handler’s name. To notify more handlers of the same event, we just let them listen; they will also get notified.


## Callback Plug-ins

Ansible supports a feature called callback plug-ins that can perform custom actions in response to Ansible events, such as a play starting or a task completing on a host. You can use a callback plug-in to do things such as send a Slack message or write an entry to a remote logging server. In fact, the output you see in your terminal when you execute an Ansible playbook is implemented as a callback plug-in. Ansible supports three kinds of callback plug-ins:

- Stdout plug-ins
- Notification plug-ins
- Aggregate plug-ins

Stdout plug-ins control the format of the output displayed to the terminal. Ansible’s implementation makes no distinction between notification and aggregate plug-ins, which can perform a variety of actions.


## Custom Modules

Sometimes it’s simpler to use the script module than to write a full-blown Ansible module.

### How Ansible Invokes Modules

Before we implement the module, let’s go over how Ansible invokes them:

1. Generate a standalone Python script with the arguments (Python modules only)
2. Copy the module to the host
3. Create an arguments file on the host (non-Python modules only) 
4. Invoke the module on the host, passing the arguments file as an argument 
5. Parse the standard output of the module

### Invoke the Module

What Ansible actually does is a bit more complicated; it wraps the module in a secure shell command line to prepare the locale and to cleanup afterward.

### Output Variables That Ansible Expects

- <u>changed</u>: All Ansible modules should return a changed variable. The changed variable is a boolean that tells whether the module execution caused the host to change state. 
- <u>failed</u>: If the module fails to complete, it should return "failed": true. Ansible will treat this task execution as a failure and will not run any further tasks against the host that  failed unless the task has an ignore_errors or failed_when clause.
- <u>msg</u>: Use the msg variable to add a descriptive message that describes the reason that a module failed.

### Specifying an Alternative Location for Bash

Note that our module assumes that Bash is located at /bin/bash. However, not all systems will have the Bash executable in that location. You can tell Ansible to look elsewhere for the Bash interpreter by setting the ansible_bash_interpreter variable on hosts that install it elsewhere. Ansible determines which interpreter to use by looking for the shebang (#!) and then looking at the base name of the first element.


## Making Ansible Go Even Faster

### SSH Multiplexing and ControlPersist

Because the SSH protocol runs on top of the TCP protocol, when you make a connection to a remote machine with SSH, you need to make a new TCP connection. When Ansible runs a playbook it makes many SSH connections, to do things such as copy over files and run modules. Each time Ansible makes a new SSH connection to a host, it has to pay this negotiation penalty. OpenSSH supports an optimization called SSH multiplexing, also referred to as ControlPersist, which allows multiple SSH sessions to the same host to share the same TCP connection. This means that the TCP connection negotiation happens only the first time, thus eliminating the negotiation penalty.

### Enabling Pipelining

Pipelining is off by default because it can require some configuration on your remote hosts, but we like to enable it because it is a big speed-up you can implement in Ansible. To enable it, change your ansible.cfg file

### Fact Caching

Facts about your servers contain all kinds of variables that can be useful in your playbook. These facts are gathered at the beginning of a playbook, but this gathering takes time, so it is a candidate for tuning. One option is to create a local cache with this data; another option is not to gather the facts.

### Parallelism

For each task, Ansible will connect to the hosts in parallel to execute the tasks. But Ansible doesn’t necessarily connect to all of the hosts in parallel. Instead, the level of parallelism is controlled by a parameter, which defaults to 5. You can change this default parameter.

### Concurrent Tasks with Async

Ansible introduced support for asynchronous actions with the async clause to work around the problem of connection timeouts. If the execution time for a task exceeds that timeout, Ansible will lose its connection to the host and report an error. Marking a long-running task with the async clause eliminates the risk of a connection timeout.


## Ansible Automation Platform (AWX)

### What Ansible Automation Platform Solves

Ansible Automation Platform is not just a web user interface on top of Ansible: it extends Ansible’s functionality with access control, projects, inventory management, and the ability to run jobs by job templates. Let’s take a closer look at each of these in turn: 

- Access Control

In large corporations, Ansible Automation Platform helps manage automation by delegating control. You can create an organization for each department, and a local system administrator can set up teams with roles and add employees to them, giving each person as much control of the managed hosts and devices as they need to do their job.

- Projects

A project in Ansible Automation Platform terminology is nothing more than a bucket holding logically related playbooks and roles.

- Inventory Management

Ansible Automation Platform allows you to manage inventories as dedicated resources, including managing access control. Within these inventories, you can add default variables and manually add groups and hosts. You can even temporarily disable hosts by clicking a button, so they will be excluded from any job run.

- Run Jobs by Job Templates

Job templates connect projects with inventories. They define how users are allowed to execute a playbook from a project to specific targets from a selected inventory. Refinements can be applied on a playbook level, such as additional parameters and tags. Further, you can specify in what mode the playbook will run.

- RESTful API

The Ansible  Automation Controller exposes a Representational State Transfer (REST) API that lets you integrate with existing build-and-deploy pipelines or continuous deployment systems.
