# SQL

![order](./attachments/sql/order.png)

## Examples

### Exercice 1683: Invalid Tweets

CHAR_LENGHT != LENGHT
LENGHT measures the number of bytes the character takes and char_lenght returns the actual lenght of the character. For example `CHAR_LENGHT('€') = 1` and `LENGHT('€') = 3`

### Exercice 1581: Customer Who Visited but Did Not Make Any Transactions

Write a solution to find the IDs of the users who visited without making any transactions and the number of times they made these types of visits.

Input: 

```sql
Visits
+----------+-------------+
| visit_id | customer_id |
+----------+-------------+
| 1        | 23          |
| 2        | 9           |
| 4        | 30          |
| 5        | 54          |
| 6        | 96          |
| 7        | 54          |
| 8        | 54          |
+----------+-------------+
Transactions
+----------------+----------+--------+
| transaction_id | visit_id | amount |
+----------------+----------+--------+
| 2              | 5        | 310    |
| 3              | 5        | 300    |
| 9              | 5        | 200    |
| 12             | 1        | 910    |
| 13             | 2        | 970    |
+----------------+----------+--------+
```

Output: 

```sql

+-------------+----------------+
| customer_id | count_no_trans |
+-------------+----------------+
| 54          | 2              |
| 30          | 1              |
| 96          | 1              |
+-------------+----------------+
```

Answer:

```sql
select customer_id, count(visit_id) as count_no_trans from Visits 
where visit_id not in (select visit_id from Transactions)
group by customer_id;
```

```sql
select v.customer_id, count(v.customer_id) as count_no_trans from visits v 
left join transactions t on v.visit_id = t.visit_id where t.visit_id is null 
group by v.customer_id
```

### Exercice 197: Rising Temperature
Write a solution to find all dates' id with higher temperatures compared to its previous dates (yesterday).

Input: 
```sql
Weather table:
+----+------------+-------------+
| id | recordDate | temperature |
+----+------------+-------------+
| 1  | 2015-01-01 | 10          |
| 2  | 2015-01-02 | 25          |
| 3  | 2015-01-03 | 20          |
| 4  | 2015-01-04 | 30          |
+----+------------+-------------+
Output: 
+----+
| id |
+----+
| 2  |
| 4  |
+----+
```
Answer: (2nd solution is better)
```sql
select w1.id from weather w1, weather w2 where datediff(w1.recordDate, w2.recordDate) = 1 and w1.temperature > w2.temperature

SELECT 
    w1.id
FROM 
    Weather w1
JOIN 
    Weather w2
ON 
    w1.recordDate = DATE_ADD(w2.recordDate, INTERVAL 1 DAY)
WHERE 
    w1.temperature > w2.temperature;
```

self join: Here same results
```sql
select * from weather w1 join weather w2 
select * from weather w1 cross join weather w2 
select * from weather w1, weather w2
```

### Exercice 1661: Average Time of Process per Machine

There is a factory website that has several machines each running the same number of processes. Write a solution to find the average time each machine takes to complete a process.

```sql
Input: 
Activity table:
+------------+------------+---------------+-----------+
| machine_id | process_id | activity_type | timestamp |
+------------+------------+---------------+-----------+
| 0          | 0          | start         | 0.712     |
| 0          | 0          | end           | 1.520     |
| 0          | 1          | start         | 3.140     |
| 0          | 1          | end           | 4.120     |
| 1          | 0          | start         | 0.550     |
| 1          | 0          | end           | 1.550     |
| 1          | 1          | start         | 0.430     |
| 1          | 1          | end           | 1.420     |
| 2          | 0          | start         | 4.100     |
| 2          | 0          | end           | 4.512     |
| 2          | 1          | start         | 2.500     |
| 2          | 1          | end           | 5.000     |
+------------+------------+---------------+-----------+
Output: 
+------------+-----------------+
| machine_id | processing_time |
+------------+-----------------+
| 0          | 0.894           |
| 1          | 0.995           |
| 2          | 1.456           |
+------------+-----------------+
```

Answer :
```sql
select a1.machine_id, ROUND(AVG(a2.timestamp - a1.timestamp), 3) as processing_time  from (select * from activity where activity_type = 'start') a1 inner join (select * from activity WHERE activity_type = 'end') a2
ON a1.machine_id = a2.machine_id AND a1.process_id = a2.process_id GROUP BY a1.machine_id
```

### Exercice 1280: Students and Examinations

Replace null by a value, can use ISNULL(column_name, value) or COALESCE(column_name, value)

### Exercice 1934: Confirmation Rate

The confirmation rate of a user is the number of 'confirmed' messages divided by the total number of requested confirmation messages. The confirmation rate of a user that did not request any confirmation messages is 0. Round the confirmation rate to two decimal places. Write a solution to find the confirmation rate of each user.

```sql
Input: 
Signups table:
+---------+---------------------+
| user_id | time_stamp          |
+---------+---------------------+
| 3       | 2020-03-21 10:16:13 |
| 7       | 2020-01-04 13:57:59 |
| 2       | 2020-07-29 23:09:44 |
| 6       | 2020-12-09 10:39:37 |
+---------+---------------------+
Confirmations table:
+---------+---------------------+-----------+
| user_id | time_stamp          | action    |
+---------+---------------------+-----------+
| 3       | 2021-01-06 03:30:46 | timeout   |
| 3       | 2021-07-14 14:00:00 | timeout   |
| 7       | 2021-06-12 11:57:29 | confirmed |
| 7       | 2021-06-13 12:58:28 | confirmed |
| 7       | 2021-06-14 13:59:27 | confirmed |
| 2       | 2021-01-22 00:00:00 | confirmed |
| 2       | 2021-02-28 23:59:59 | timeout   |
+---------+---------------------+-----------+
Output: 
+---------+-------------------+
| user_id | confirmation_rate |
+---------+-------------------+
| 6       | 0.00              |
| 3       | 0.00              |
| 7       | 1.00              |
| 2       | 0.50              |
+---------+-------------------+
```

Answer:

```
with 
t1 as (
    select user_id, action, count(1) as action_count from confirmations group by user_id, action
),
t2 as (
    select user_id, count(1) as user_count from confirmations group by user_id
),
t3 as (
    select t2.user_id, round(t1.action_count/t2.user_count, 2) as confirmation_rate  from t2 left join t1 on t2.user_id = t1.user_id where t1.action = 'confirmed'
)

select s.user_id, ifnull(t3.confirmation_rate, 0) as confirmation_rate from signups s left join t3 on s.user_id = t3.user_id 

select s.user_id, IFNULL(ROUND(sum(action = 'confirmed')/count(*),2), 0.00)  as confirmation_rate from Signups s
left join Confirmations c
on c.user_id = s.user_id
group by s.user_id
```

### Exercice 1251: Average Selling Price

usecase ON column_name BETWEEN column_start AND column_end

### Exercice 1193: Monthly Transactions I

```sql
Input: 
Transactions table:
+------+---------+----------+--------+------------+
| id   | country | state    | amount | trans_date |
+------+---------+----------+--------+------------+
| 121  | US      | approved | 1000   | 2018-12-18 |
| 122  | US      | declined | 2000   | 2018-12-19 |
| 123  | US      | approved | 2000   | 2019-01-01 |
| 124  | DE      | approved | 2000   | 2019-01-07 |
+------+---------+----------+--------+------------+
Output: 
+----------+---------+-------------+----------------+--------------------+-----------------------+
| month    | country | trans_count | approved_count | trans_total_amount | approved_total_amount |
+----------+---------+-------------+----------------+--------------------+-----------------------+
| 2018-12  | US      | 2           | 1              | 3000               | 1000                  |
| 2019-01  | US      | 1           | 1              | 2000               | 2000                  |
| 2019-01  | DE      | 1           | 1              | 2000               | 2000                  |
+----------+---------+-------------+----------------+--------------------+-----------------------+
```

Answer

```sql
select 
    date_format(trans_date, '%Y-%m') as month, 
    country, 
    count(*) as trans_count, 
    sum(case when state = 'approved' then 1 else 0 end ) as approved_count, 
    sum(amount) as trans_total_amount, 
    sum(case when state = "approved" then amount else 0 end) as approved_total_amount 
from transactions 
group by month, country
```

### Exercice 1174: Immediate Food Delivery II

#### Problem
Write a solution to find the percentage of immediate orders in the first orders of all customers, rounded to 2 decimal places.

```sql
Input: 
Delivery table:
+-------------+-------------+------------+-----------------------------+
| delivery_id | customer_id | order_date | customer_pref_delivery_date |
+-------------+-------------+------------+-----------------------------+
| 1           | 1           | 2019-08-01 | 2019-08-02                  |
| 2           | 2           | 2019-08-02 | 2019-08-02                  |
| 3           | 1           | 2019-08-11 | 2019-08-12                  |
| 4           | 3           | 2019-08-24 | 2019-08-24                  |
| 5           | 3           | 2019-08-21 | 2019-08-22                  |
| 6           | 2           | 2019-08-11 | 2019-08-13                  |
| 7           | 4           | 2019-08-09 | 2019-08-09                  |
+-------------+-------------+------------+-----------------------------+
Output: 
+----------------------+
| immediate_percentage |
+----------------------+
| 50.00                |
+----------------------+
```

#### Answer

```sql
SELECT ROUND(
    SUM(IF(d.order_date = d.customer_pref_delivery_date, 1, 0)) * 100 
    / COUNT(d.customer_id), 2
) AS immediate_percentage
FROM Delivery d
JOIN (
    SELECT customer_id, MIN(order_date) AS first_order_date
    FROM Delivery 
    GROUP BY customer_id
) first_orders ON d.customer_id = first_orders.customer_id 
AND d.order_date = first_orders.first_order_date;
```

```sql
select round(sum(if(order_date = customer_pref_delivery_date,1,0))*100/ count(customer_id),2) as immediate_percentage
from Delivery
where (customer_id, order_date) in (
    select customer_id, min(order_date) from Delivery group by customer_id
)
```

```sql
with first_order as (
    select *, row_number() over (partition by customer_id order by order_date) as rn from delivery
)
select round(sum(case when (customer_pref_delivery_date-order_date)=0 then 1 else 0 end)/count(*)*100, 2) as immediate_percentage from first_order where rn = 1
```

#### Explain

✅ Avantages :

Utilise ROW_NUMBER(), qui est généralement efficace avec un bon moteur d'indexation.
Facile à comprendre et à maintenir.

❌ Inconvénients :

Peut être coûteux en performance car MySQL crée d'abord une table temporaire (WITH first_order AS (...)).
Si la table delivery est volumineuse, l’utilisation d’une fonction analytique (window function) peut ralentir la requête.

✅ Avantages :

Pas de fonction analytique → Meilleure performance sur MySQL, surtout en l'absence d'index.
MIN(order_date) est facilement optimisé avec un index sur customer_id, order_date.
Évite une table temporaire contrairement à ROW_NUMBER().

❌ Inconvénients :

Sous-requête corrélée → Peut être moins performant si customer_id n'est pas indexé.
MySQL pourrait mal optimiser l'utilisation du IN (...).

✅ Avantages :

Évite les fonctions analytiques et les tables temporaires.
Plus performant que IN (...) car MySQL gère mieux les JOIN.
Profite pleinement des index (customer_id, order_date).

🚀 Conclusion : 

👉 Si customer_id, order_date sont indexés → La requête avec JOIN est la meilleure.

👉 Si aucun index n'est présent → La requête avec ROW_NUMBER() est plus stable, mais moins rapide.

📊 Comparaison des complexités

Requête|Complexité approximative|Explication
---|---|---
ROW_NUMBER()|O(n log n)|Trie nécessaire pour PARTITION BY
IN (...)|O(n²) (pire cas) / O(n log n) (si optimisé)|Peut nécessiter une recherche pour chaque ligne
JOIN|O(n log n)|MySQL optimise bien les JOIN avec index

🚀 Conclusion : Quelle requête est la plus efficace ?

✅ Meilleur choix → La requête avec JOIN (car elle reste O(n log n), ce qui est optimal pour ce type de problème).

❌ La requête avec IN (...) peut être très lente (O(n²)), sauf si optimisée correctement.

⚠️ La requête avec ROW_NUMBER() est correcte, mais les fonctions analytiques sont plus coûteuses sur MySQL.

### Exercice 550: Game Play Analysis IV

#### Problem

You need to count the number of players that logged in for at least two consecutive days starting from their first login date, then divide that number by the total number of players.

```sql
Input: 
Activity table:
+-----------+-----------+------------+--------------+
| player_id | device_id | event_date | games_played |
+-----------+-----------+------------+--------------+
| 1         | 2         | 2016-03-01 | 5            |
| 1         | 2         | 2016-03-02 | 6            |
| 2         | 3         | 2017-06-25 | 1            |
| 3         | 1         | 2016-03-02 | 0            |
| 3         | 4         | 2018-07-03 | 5            |
+-----------+-----------+------------+--------------+
Output: 
+-----------+
| fraction  |
+-----------+
| 0.33      |
+-----------+
```

#### Answer 
```sql
WITH first_login AS (
    -- Find each player's first login date
    SELECT player_id, MIN(event_date) AS first_login_date
    FROM activity
    GROUP BY player_id
)

SELECT 
    ROUND(
        COUNT(DISTINCT a.player_id) / (SELECT COUNT(DISTINCT player_id) FROM activity),
        2
    ) AS fraction
FROM first_login f
JOIN activity a 
ON f.player_id = a.player_id 
AND DATEDIFF(a.event_date, f.first_login_date) = 1;
```

### Exercice 

#### Problem



```sql

```

#### Answer 
```sql

```